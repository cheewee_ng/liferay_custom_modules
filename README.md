Best practises in developing Software Applications.
1)	Source Code versioning
Structure of the Project in Subversions
tags – contain all production releases (for each release to Production, developer shall create the tag)
branches – contain variations of the project (e.g. for POCs).
trunks – contain main development of the project.

2)	Source Code Formatter and Coding Convention 
This is to facilitate the manual code review by Senior Developer and track the changes to the source codes.
Reference link: http://www.oracle.com/technetwork/java/javase/documentation/codeconvtoc-136057.html
Sample Java File with comments: 


For formatter, you may use default JBoss Developer Studio source code formatter.
Under Source – Format, you may click Ctrl –Shift – F.
3)	Modularization to produce reusable components & documentations e.g. JavaDoc and guide to use
 Developer is required to identify and put the reusable codes into common package and have proper documentation so the codes can be easily reused.
High Level Package: com.cxrus.common.module
Proposed Package 
1.	web ui:
    web.ntuc.nlh.modulename
2. Service:
    svc.ntuc.nlh.modulename.api
    svc.ntuc.nlh.modulename.service
3. Service tag
    nlh_module

4)	Backend Input Validation
Besides performing frontend input validation e.g. using JavaScript, developer should also perform backend input validation. The Rational is frontend input validation can be easily bypassed using tool e.g. webscarab.
5)	Application Packaging, Deployment and Configuration Guidelines
This should minimally contain the following items:
1)	Source code location (subversions info, id and password) including configuration files, build script, database sql scripts and etc.
2)	How to build the application for Development, Trial or Production deployment e.g. ANT build.xml command.
3)	Development, Trial and Production Servers (Web, Application, Database, LDAP) and all URL information e.g. www.gv.com and User ID and password to access the application in various environment (Development, Trial and Production).
4)	Application Directory Structure and its purposes.
5)	Application Configurations and their description in configuration files or backend database.

6)	Functional Testing shall be performed by Developers
Developers shall perform unit and functional testing to verify that the codes work correctly. 
7)	Static Code Scan (CheckMarx) shall be performed at early stage, not after completion of the entire development
Developers shall perform static code scan using CheckMarx to fix any defects in the code, so they will learn and will not create the same defects for subsequent codes. 
For checkmarx setup, please refer to the relevant document in \\cx-jupiter\Project-Global\Prod Team\Tech Sharing\Checkmarx
8)	Manual Code Review shall be performed by Senior/Lead Developers at early stage, not after completion of the entire development
Senior developers shall perform manual code review by walking through the source code done by the developer. This is to identify any potential bugs in the code. Any findings shall be documented and served as knowledge sharing to all other developers.
9)	Senior/Lead Developers shall involve in verification of the team’s works.

10)	User Acceptance Testing (UAT) shall be verified by Testing Team prior to releasing to Customer for UAT.

11)	Application Penetration Testing shall be performed by Testing Team using tool e.g. HP Web Inspect or Burp Suite.

12)	Load Testing shall be performed by Testing Team.

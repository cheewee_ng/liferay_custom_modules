//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 1000;  //time in ms, 5 second for example
var $input = $('#txsearch');

//on keyup, start the countdown
$input.on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

//on keydown, clear the countdown
$input.on('keydown', function () {
    clearTimeout(typingTimer);
});

//user is "finished typing," do something
function doneTyping () {
    $('#resultsearch .row').html('');
    $('#resultsearch .text-center').html('');

    $.ajax({
        url: ajaxurl,
        type:"POST",
        dataType:'json',
        data: {
            action:'ajax_search',
            keyword: $('#txsearch').val(),
        },
        success: function(response){
            var html = '';

            html += '<div class="col-sm-6">';
            $.each(response['data'], function( index, value ) {
                if(index === 'courses'){
                    html += '<ul><h4>Courses</h4>';
                    $.each(value, function( course_id, course_details ) {
                        html += '<li><a href="'+course_details['url']+'">'+course_details['title']+'</a></li>';
                    });
                    html += '</ul>';
                }
                if(index === 'others'){
                    html += '<ul><h4>Pages and Others</h4>';
                    $.each(value, function( other_id, other_details ) {
                        html += '<li><a href="'+other_details['url']+'">'+other_details['title']+'</a></li>';
                    });
                    html += '</ul>';
                }
            });
            html += '</div>';

            html += '<div class="col-sm-6">';
            $.each(response['data'], function( index, value ) {
                if(index === 'blogs'){
                    html += '<h4>Blogs</h4>';
                    $.each(value, function( blog_id, blog_details ) {
                        html += '<div class="grid-6 clearfix"><figure><a href="'+blog_details['url']+'"><img src="'+blog_details['thumbnail']+'" alt="" /></a></figure>';
                        html += '<div class="descripts">' +
                            '<p><a href="'+blog_details['url']+'">'+blog_details['title']+'</a></p>' +
                            '<span class="status">'+blog_details['cats']+'</span> <span class="date">'+blog_details['date']+'</span>' +
                            '</div></div>';
                    });
                }
            });
            html += '</div>';

            $('#resultsearch .row').html(html);
            $('#resultsearch .text-center').html('<a href="'+response['data']['search_link']+'">See all results for “'+$('#txsearch').val()+'”</a>');
        },
        error: function(data){
            $('#resultsearch .row').html(
                '<div class="col-sm-12">Failed to get search results, please try again later!</div>'

            );

        }

    });
}

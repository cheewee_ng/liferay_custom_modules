<#assign
	show_footer = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-footer"))
	show_header = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header"))
	show_header_search = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header-search"))
	wrap_widget_page_content = getterUtil.getBoolean(themeDisplay.getThemeSetting("wrap-widget-page-content"))
/>

<#if wrap_widget_page_content && ((layout.isTypeContent() && themeDisplay.isStateMaximized()) || (layout.getType() == "portlet"))>
	<#assign portal_content_css_class = "container" />
<#else>
	<#assign portal_content_css_class = "" />
</#if>
<!--Start-->
<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#--  This is Code for embed web content  -->
<#macro embedJournalArticle journalArticleTitle portletInstanceId>
    <#if validator.isNotNull(serviceLocator)>
        <#if validator.isNotNull(journalArticleLocalService)>
            <#if validator.isNotNull(journalArticleTitle) && !validator.isBlank(journalArticleTitle)>
                <#assign journalArticleUrlTitle=journalArticleTitle?lower_case?replace(" ", "-")?replace("_", "-")/>
                <#if validator.isNotNull(journalArticleUrlTitle) && !validator.isBlank(journalArticleUrlTitle)>
                    <#assign journalArticle=journalArticleLocalService.fetchArticleByUrlTitle(group_id,journalArticleUrlTitle)/> 
                </#if>
            </#if>
        </#if>  
    </#if>  
  <#if validator.isNotNull(journalArticle)> 
        <#assign journalArticlePreferencesMap = 
                    {
                        "portletSetupPortletDecoratorId": "barebone", 
                        "groupId": getterUtil.getString(group_id),
                        "articleId":getterUtil.getString(journalArticle.getArticleId())
                    } 
                    
                />
        <#assign journalArticlePreferences = freeMarkerPortletPreferences.getPreferences(journalArticlePreferencesMap) />
        <@liferay_portlet["runtime"]
                defaultPreferences= journalArticlePreferences
                instanceId=portletInstanceId
                portletProviderAction=portletProviderAction.VIEW
                portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet" 
                />
    </#if>
</#macro>
<#--  End code to embed web content  -->

<#assign navTransparent = getterUtil.getBoolean(theme_settings["nav-transparent"])>
<#assign navBlog = getterUtil.getBoolean(theme_settings["nav-blog"])>
<#assign wrap_widget_page_content = getterUtil.getBoolean(theme_settings["wrap-widget-page-content"])>



<#assign usrRoles  = user.getRoles() >

<#assign showControlMenu  = false >
<#assign userRoleName = "DEFAULT" >
<#assign role_name = []>
<#list usrRoles as uRole>
    <#if uRole.getName() == "Administrator" || uRole.getName() == "Admin" || uRole.getName() == "User">
  <#assign showControlMenu = true />
 <#assign userRoleName = uRole.getName() />
  <#break>
 </#if>
  <#assign userRoleName = uRole.getName() />
</#list>


<#assign base_url = "/web/guest">
<#assign logout_url = htmlUtil.escape(theme_display.getURLSignOut())>
<#assign base_portal = "/c/portal/login">
<#assign base_login = "/web/guest/login">
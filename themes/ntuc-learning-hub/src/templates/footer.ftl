<@embedJournalArticle journalArticleTitle="Footer" portletInstanceId="footer" />  
<#--  <footer class="footer-container">
        <div class="container">
            <div class="row sp-row-2">
                <div class="bcol col-lg-3 col-md-12">
                    <div class="logo">
                        <a href="index.html">
                            <img src="${images_folder}/ntuc-learning-hub-logo-2.png" alt="NTUC-LHUB" />
                        </a>
                    </div>

                    <p><strong>“Transforming People”</strong> is the essential ideal that drives our brand and encapsulates what we offer to our customers and partners. In providing training services and solutions, we see ourselves enabling change for every individual with whom we work with.</p>
<p>NTUC LearningHub Pte Ltd. CPE/UEN No: 200409359E<br />
26 Nov 2018 to 25 Nov 2022</p>
                </div>

                <div class="bcol col-lg-3 col-md-4 sp-991-1">
                    <h4>Useful Links</h4>
                                            <ul class="links">
                                                            <li><a href="https://ntuc-lhub-wordpress-static-prod.s3.ap-southeast-1.amazonaws.com/wp-content/uploads/2020/11/23112145/Registration-Form-For-Individual.docx" >Course Registration Form for Individual</a></li>
                                                            <li><a href="https://ntuc-lhub-wordpress-static-prod.s3.ap-southeast-1.amazonaws.com/wp-content/uploads/2021/01/21174059/Registration-Form-For-Company.xlsx" target="_blank">Course Registration Form for Company</a></li>
                                                            <li><a href="https://ntuc-lhub-wordpress-static-prod.s3.ap-southeast-1.amazonaws.com/wp-content/uploads/2020/05/Company-Code-Request-Form.doc" >Company Code Request Form</a></li>
                                                            <li><a href="https://ntuc-lhub-wordpress-static-prod.s3.ap-southeast-1.amazonaws.com/wp-content/uploads/2021/01/26143314/LHUB-Prospectus-2021-Web.pdf" >Training Prospectus</a></li>
                                                            <li><a href="#" target="_blank">Personal Data and Privacy Statement</a></li>
                                                            <li><a href="${base_login}" target="_blank">Liferay Login</a></li>
                                                            <li><a href="${base_portal}" target="_blank">Okta Login</a></li>
                                                    </ul>
                                    </div>

                <div class="bcol col-lg-3 col-md-4 sp-991-1">
                    <h4>Popular Funding Schemes</h4>
                                            <ul class="links">
                                                            <li ><a href="#" >SkillsFuture Credit</a></li>
                                                            <li ><a href="#" >Skills Development Fund (SDF) for Individuals</a></li>
                                                            <li ><a href="#" >SkillsFuture Mid-Career Enhanced Subsidy</a></li>
                                                            <li ><a href="#" >Enhanced Training Support Package</a></li>
                                                            <li ><a href="#" >Skills Development Fund (SDF) for Companies</a></li>
                                                    </ul>
                                    </div>

                <div class="bcol col-lg-3 col-md-4 sp-991-1">
                    <h4>Get In Touch</h4>
                                            <ul class="socials">
                                                            <li><a href="https://www.facebook.com/ntuclearninghub" target="_blank"><i class="fab fa-facebook-f"></i>Facebook</a></li>
                                                            <li><a href="https://twitter.com/ntuclearninghub" target="_blank"><i class="fab fa-twitter"></i>Twitter</a></li>
                                                            <li><a href="https://www.youtube.com/channel/UCIbcq6DrwkpAEOSxMxpWd1w" target="_blank"><i class="fab fa-youtube"></i>Youtube</a></li>
                                                            <li><a href="https://www.instagram.com/ntuclearninghub/" target="_blank"><i class="fab fa-instagram"></i>Instagram</a></li>
                                                            <li><a href="https://www.linkedin.com/school/ntuc-learninghub/" target="_blank"><i class="fab fa-linkedin"></i>LinkedIn</a></li>
                                                    </ul>
                    
                    <h4> Customer General Enquiries</h4>
                                            <p class="fas fa-phone"><a href="tel:63365482">6336 5482 (Weekdays, 8.30am - 5.30pm)</a></p>
                                            <p class="fas fa-envelope"><a href="enquiry-form/index.html">Enquiry Form</a></p>
                                            <p class="fas fa-map-marker-alt"><a href="contact-us-2/contact-us/index.html">Contact Us</a></p>
                                    </div>
            </div>
        </div>
    </footer>  -->
<a href="#toppage" class="smoothscroll gotop fas fa-chevron-up" style="opacity: 1;">Go Top</a>
<#--  
<footer id="footer" class="CX-footer">
    <div class="CX-top-footer">
        <div class="container-fluid CX-wrapper">
            <div class="row">
                <div class="col-md-3">
                    <div class="box-nav-footer">
                        <h2 class="title-nav">Link Cepat</h2>
                        <ul>
                            <li><a href="#">Solusi Perlindungan AXA Indonesia</a></li>
                            <li><a href="#">AXA Shop</a>
                            <li><a href="#">Hubungi Kami</a>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-nav-footer">
                        <h2 class="title-nav">AXA Indonesia</h2>
                        <ul>
                            <li><a href="#">Tentang AXA Indonesia</a></li>
                            <li><a href="#">Kebijakan Privasi</a>
                            <li><a href="#">Kebijakan Cookie</a>
                            <li><a href="#">Media</a>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-nav-footer">
                        <h2 class="title-nav">Kantor Pusat</h2>
                        <ul>
                            <li>
                                <a href="javascript:void(0)">
                                    AXA Tower Ground Floor Jl. Prof. Dr. Satrio Kav. 18 Kuningan City Jakarta 12940, Indonesia
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    Contact Centre :<br/>
                                    AXA Financial Indonesia : 1500 940 Jam Operasional: Senin-Jumat pukul 08.00 WIB – 17.00 WIB
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    Mandiri AXA General Insurance: 1500 733 Jam Operasional: Senin-Jumat pukul 08.00 WIB – 17.00 WIB
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    Kantor Pusat : +62 21 3005 8000
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-nav-footer">
                        <h2 class="title-nav">Follow AXA</h2>
                        <ul class="social-media">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram-square"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        </ul>
                    </div>
                    <div class="box-nav-footer">
                        <h2 class="title-nav">Direktori</h2>
                        <p class="desc-nav">Cari rumah sakit dan workshop rekanan asuransi AXA terdekat di kota Anda untuk memudahkan Anda mendapat dukungan</p>
                        <form class="form-inline" method="" action="">
                            <div class="form-group">
                                <select class="form-control">
                                    <option value="0">Pilih Kota</option>
                                    <option value="1">Jakarta</option>
                                    <option value="2">Bogor</option>
                                    <option value="3">Bandung</option>
                                </select>
                            </div>
                            <input type="submit" class="btn btn-primary btn-small" value="Cari" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="CX-footer-copyright">
        <div class="container-fluid CX-wrapper">
            <p><a href="#">Disclaimer & Ownership.</a> ©2021 Hak Cipta Dilindungi.</p>
            <p>AXA Financial Indonesia dan Mandiri AXA General Insurance merupakan perusahaan asuransi yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan</p>
        </div>
    </div>
</footer>  -->



                  
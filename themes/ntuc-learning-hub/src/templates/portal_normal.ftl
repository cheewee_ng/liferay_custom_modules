<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WNKRHD');</script>
	<!-- End Google Tag Manager -->
	
	<title>${html_title}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />
</head>
<#if navTransparent == true>
  <#assign nav_cls = " "/>
<#else>
  <#assign nav_cls = "no-banner"/>
</#if>
<#if navBlog == true>
  <#assign nav_blog = "show-nav-blog"/>
<#else>
  <#assign nav_blog = " "/>
</#if>
<body class="${css_class} ${nav_cls}">
 <!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WNKRHD"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<#--  Service Panel  -->
	<#if is_signed_in>
		<div class="styleswitcher">
			<input id="settings-btn" class="settings-btn" type="checkbox">
			<label for="settings-btn" class="settings-box-element"><i class="fa-2x fas fa-cogs"></i></label>
			<div class="buttons-wrapper settings-box-element">
				<h4 class="text-uppercase title-panel">Service Panel</h4>
				<div class="item-panel">
					<span class="label">Show Control Panel</span>
					<label class="rocker rocker-small">
						<input type="checkbox" name="show-control-panel" class="check-control-panel">
						<span class="switch-left">On</span>
						<span class="switch-right">Off</span>
					</label>
				</div>
				<div class="item-bottom">
					<#--  <p class="label">Deploy</p>
					<a href="javascript:;" target="_self" class="lfr-icon-item taglib-icon btn cx-btn btn-primary btn-sm" id="_com_liferay_marketplace_app_manager_web_portlet_MarketplaceAppManagerPortlet_kldx______menu__upload" onclick="_com_liferay_marketplace_app_manager_web_portlet_MarketplaceAppManagerPortlet_uploadUrlLink();" role="menuitem" tabindex="0"> <span class="taglib-text-icon">Upload</span> </a>
					<div class="col-sm-12 mt-3">  -->
            			<a data-senna-off="true" class="lfr-icon-item taglib-icon btn cx-btn btn-primary btn-sm" href="${logout_url}">Logout</a>
					</div>
				</div>
				
			</div>
		</div>	
	</#if>
	<#--  End Service Panel  -->
<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<div class="d-flex flex-column min-vh-100">
	<@liferay.control_menu />

		<#include "${full_templates_path}/header.ftl" />

	<div class="d-flex flex-column flex-fill">
		<#--  <#if show_header>
			<header id="banner">
				<div class="navbar navbar-classic navbar-top py-3">
					<div class="container user-personal-bar">
						<div class="align-items-center autofit-row">
							<a class="${logo_css_class} align-items-center d-md-inline-flex d-sm-none d-none logo-md" href="${site_default_url}" title="<@liferay.language_format arguments="" key="go-to-x" />">
								<img alt="${logo_description}" class="mr-2" height="56" src="${site_logo}" />

								<#if show_site_name>
									<h2 class="font-weight-bold h2 mb-0 text-dark" role="heading" aria-level="1">${site_name}</h2>
								</#if>
							</a>

							<#assign preferences = freeMarkerPortletPreferences.getPreferences({"portletSetupPortletDecoratorId": "barebone", "destination": "/search"}) />

							<div class="autofit-col autofit-col-expand">
								<#if show_header_search>
									<div class="justify-content-md-end mr-4 navbar-form" role="search">
										<@liferay.search_bar default_preferences="${preferences}" />
									</div>
								</#if>
							</div>

							<div class="autofit-col">
								<@liferay.user_personal_bar />
							</div>
						</div>
					</div>
				</div>

				<div class="navbar navbar-classic navbar-expand-md navbar-light pb-3">
					<div class="container">
						<a class="${logo_css_class} align-items-center d-inline-flex d-md-none logo-xs" href="${site_default_url}" rel="nofollow">
							<img alt="${logo_description}" class="mr-2" height="56" src="${site_logo}" />

							<#if show_site_name>
								<h2 class="font-weight-bold h2 mb-0 text-dark">${site_name}</h2>
							</#if>
						</a>

						<#include "${full_templates_path}/navigation.ftl" />
					</div>
				</div>
			</header>
		</#if>  -->
<div id="CX-utama-ntuc" class="CX-utama-ntuc">
			<div class="box-utama" id="toppage">
		<section class="${portal_content_css_class} flex-fill" id="content">
			<h2 class="sr-only" role="heading" aria-level="1">${the_title}</h2>

			<#if selectable>
				<@liferay_util["include"] page=content_includehh />
			<#else>
				${portletDisplay.recycle()}

				${portletDisplay.setTitle(the_title)}

				<@liferay_theme["wrap-portlet"] page="portlet.ftl">
					<@liferay_util["include"] page=content_include />
				</@>
			</#if>
		</section>
</div>
</div>

		<#include "${full_templates_path}/footer.ftl" />
		<#--  <#if show_footer>
			<footer id="footer" role="contentinfo">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center text-md-left">
							<@liferay.language key="powered-by" />

							<a class="text-white" href="http://www.liferay.com" rel="external">Liferay</a>
						</div>
					</div>
				</div>
			</footer>
		</#if>  -->
	</div>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />


<#--  
<script type="text/javascript" src="${javascript_folder}/jquery-3.5.1.min.js"></script>  -->
<script type="text/javascript" src="${javascript_folder}/jquery.cookie.min.js"></script>

<script type="text/javascript" src="${javascript_folder}/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${javascript_folder}/datatable/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="${javascript_folder}/datatable/dataTables.bootstrap4.min.js"></script>


<!--WP-->
<script type='text/javascript' src='${javascript_folder}/wp/pluginccff.js' id='plugin-js'></script>
<script type='text/javascript' src='${javascript_folder}/wp/mainccff.js' id='main-js'></script>
<script type='text/javascript' src='${javascript_folder}/wp/pagination.minccff.js' id='pagination-js'></script>
<script type='text/javascript' src='${javascript_folder}/wp/jssocials.minccff.js' id='jssocials-js'></script>
<script type='text/javascript' src='${javascript_folder}/wp/holderccff.js' id='holder-js'></script>
<script type='text/javascript' src='${javascript_folder}/wp/ajax_searchccff.js' id='ajax_search-js'></script>
<script type='text/javascript' src='${javascript_folder}/wp/customccff.js' id='custom-js'></script>
<script type='text/javascript' src='${javascript_folder}/wp/navigation4a7d.js' id='ntuc-learning-navigation-js'></script>
<script type='text/javascript' src='${javascript_folder}/wp/skip-link-focus-fix4a7d.js' id='ntuc-learning-skip-link-focus-fix-js'></script>
<script type='text/javascript' src='${javascript_folder}/custom.js'></script>
<!--End WP-->
<script>
	$(".Courses .colsub-1").addClass("colsub-2");
</script>
<#--  <script>
$('#something').click(function() {
    location.reload();
});
</script>  -->
<script>
 $('.slider-for').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   fade: true,
   asNavFor: '.slider-nav',
   dots:true
 });
 </script>
</body>

</html>
<%@ include file="init.jsp"%>
<%@page import="web.ntuc.nlh.subscription.constants.MVCCommandNames"%>

<portlet:actionURL name="<%=MVCCommandNames.ADD_SUBSCRIPTION%>"
	var="addSubscriptionURL">
	<portlet:param name="authToken" value="${authToken}" />
</portlet:actionURL>

<aui:form name="fm" method="post" action="${addSubscriptionURL }">
	<div class="boxInput grid">
		<div class="row">
			<div class="col-sm-6">
				<aui:input type="email" name="email" required="true">
					<aui:validator name="email" errorMessage="wrong-email-pattern" />
				</aui:input>

			</div>
			<div class="col-sm-6">
				<aui:input type="text" name="name" required="true">
				</aui:input>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group input-text-wrapper type-select">
					<aui:select name="contentPreference" label="Content Preference">
						<aui:option value="" selected="true" disabled="true">
							<liferay-ui:message key="select-option" />
						</aui:option>
						<c:forEach items="${parameters}" var="pr">
							<aui:option value="${pr.paramValue}" label="${pr.paramName} "/>
						</c:forEach>
				<%--		<aui:option value="Learner">
							<liferay-ui:message key="learner" />
						</aui:option>
						<aui:option value="Business">
							<liferay-ui:message key="business" />
						</aui:option>
						<aui:option value="Both">
							<liferay-ui:message key="both" />
						</aui:option>
					 --%>
					</aui:select>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="custom-checkbox">
					<aui:input type="checkbox" name="agreement" required="true"/>
				</div>
			</div>
		</div>
		<div class="boxAction">
			<button type="submit" value="Submit"
				class="hs-button">Submit</button>
		</div>
</aui:form>
</div>
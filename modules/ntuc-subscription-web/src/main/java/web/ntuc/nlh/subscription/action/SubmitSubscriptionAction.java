package web.ntuc.nlh.subscription.action;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import svc.ntuc.nlh.subscription.model.Subscription;
import svc.ntuc.nlh.subscription.service.SubscriptionLocalServiceUtil;
import web.ntuc.nlh.subscription.constants.SubscriptionPortletKeys;

@Component(immediate = true, property = {
		"mvc.command.name=" + web.ntuc.nlh.subscription.constants.MVCCommandNames.ADD_SUBSCRIPTION,
		"javax.portlet.name=" + SubscriptionPortletKeys.SUBSCRIPTION }, service = MVCActionCommand.class)
public class SubmitSubscriptionAction extends BaseMVCActionCommand {

	Log log = LogFactoryUtil.getLog(SubmitSubscriptionAction.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		// TODO Auto-generated method stub
		log.info("Submit Form");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		String name = ParamUtil.getString(actionRequest, "name");
		String email = ParamUtil.getString(actionRequest, "email");
		String contentPreference = ParamUtil.getString(actionRequest, "contentPreference");
		boolean agreement = ParamUtil.getBoolean(actionRequest, "agreement");
		try {

			Subscription subs = SubscriptionLocalServiceUtil
					.createSubscription(CounterLocalServiceUtil.increment(Subscription.class.getCanonicalName()));

			subs.setName(name);
			subs.setEmail(email);
			subs.setContentPreference(contentPreference);
			subs.setAgreement(agreement);
			subs.setGroupId(themeDisplay.getScopeGroupId());
			subs.setUserId(themeDisplay.getUserId());
			subs.setCompanyId(themeDisplay.getCompanyGroupId());
			subs.setCreateDate(new Date());
			subs.setUserName(themeDisplay.getUser().getFullName());

			SubscriptionLocalServiceUtil.updateSubscription(subs);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Something Error at " + e.getMessage(), e);
			e.printStackTrace();
			SessionErrors.add(actionRequest, "error-subscription");
		}

		SessionMessages.add(actionRequest, "success-subscription");
		actionRequest.setAttribute("name", name);
		actionResponse.getRenderParameters().setValue("mvcPath", "/thank-you.jsp");
		log.info("Exit Submit Form Subscription");
	}

}

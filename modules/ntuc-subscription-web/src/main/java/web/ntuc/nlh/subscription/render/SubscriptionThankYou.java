package web.ntuc.nlh.subscription.render;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import web.ntuc.nlh.subscription.constants.SubscriptionPortletKeys;

@Component(immediate = true, property = { "mvc.command.name=" + web.ntuc.nlh.subscription.constants.MVCCommandNames.THANK_YOU,
		"javax.portlet.name=" + SubscriptionPortletKeys.SUBSCRIPTION }, service = MVCRenderCommand.class)
public class SubscriptionThankYou implements MVCRenderCommand{

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
		// TODO Auto-generated method stub
		return "/thank-you.jsp";
	}

}

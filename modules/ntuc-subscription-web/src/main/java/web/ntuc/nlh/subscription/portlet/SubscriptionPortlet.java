package web.ntuc.nlh.subscription.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import svc.ntuc.nlh.parameter.model.Parameter;
import svc.ntuc.nlh.parameter.model.ParameterGroup;
import svc.ntuc.nlh.parameter.service.ParameterGroupLocalService;
import svc.ntuc.nlh.parameter.service.ParameterLocalService;
import web.ntuc.nlh.subscription.constants.SubscriptionPortletKeys;

/**
 * @author muhamadpangestu
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=subscription",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Subscription", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + SubscriptionPortletKeys.SUBSCRIPTION, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class SubscriptionPortlet extends MVCPortlet {
	Log log = LogFactoryUtil.getLog(SubscriptionPortlet.class);

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		log.info("Parameter portlet render - start");
		try {
//			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			ParameterGroup parentThemeGroup = _parameterGroupLocalService
					.getByCode(SubscriptionPortletKeys.CONTENT_PREFERENCE, false);
			List<Parameter> parameters = _parameterLocalService
					.getByParameterGroupId(parentThemeGroup.getParameterGroupId(), false);
			renderRequest.setAttribute("parameters", parameters);
		} catch (Exception e) {
			log.error("Failed when render Parameter, error:" + e.getMessage());
		}
		log.info("Parameter portlet render - end");
		super.render(renderRequest, renderResponse);
	}

	@Reference
	protected ParameterLocalService _parameterLocalService;

	@Reference
	protected ParameterGroupLocalService _parameterGroupLocalService;
}
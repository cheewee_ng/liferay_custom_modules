package web.ntuc.nlh.subscription.constants;

/**
 * @author muhamadpangestu
 */
public class SubscriptionPortletKeys {

	public static final String SUBSCRIPTION =
		"NTUC_Subscription_Portlet";
	public static final String CONTENT_PREFERENCE = "content-preference";

}
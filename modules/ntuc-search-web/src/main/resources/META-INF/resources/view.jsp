<%@page import="web.ntuc.nlh.search.constants.MVCCommandNames"%>
<%@ include file="/init.jsp"%>
<portlet:actionURL name="<%=MVCCommandNames.SEARCH_ACTION %>" var="actionURL" >
	<portlet:param name="authToken" value="${authToken}" />
</portlet:actionURL>

<%-- <portlet:resourceURL id="<%=MVCCommandNames.TOPIC_DATA_RESOURCES %>" var="topicResourceURL">
	<portlet:param name="authToken" value="${authToken}" />
</portlet:resourceURL>

<portlet:resourceURL id="<%=MVCCommandNames.THEME_DATA_RESOURCES %>" var="themeResourceURL">
	<portlet:param name="authToken" value="${authToken}" />
</portlet:resourceURL> --%>

<div class="course-search">
	<h2 class="title-1 text-center">
		<liferay-ui:message key="looking.for.course" />
	</h2>

	<aui:form action="${actionURL}" method="post" name="fm" data-senna-off="true">
		<input name="post_type" type="hidden" value="course" />
		<div class="row sp-row-3">
			<div class="col-lg bcol">
				<div class="row sp-row-3">
					<div class="bcol col-lg-12">
						<aui:input label="By Keywords" type="text" name="keywords" showRequiredLabel="false"  placeholder="input.keyword"/>
					</div>

					<%-- <div class="bcol col-lg-4 sp-991-2">
						<div class="form-group input-text-wrapper type-select-search">
							<aui:select name="topics" cssClass="selectpicker" label="By Topic"
								data-live-search="true">
								<aui:option value="">
									<liferay-ui:message key="select.topic" />
								</aui:option>
								
							</aui:select>
						</div>
					</div>

					<div class="bcol col-lg-4 sp-991-2">
						<aui:select name="themes" cssClass="selectpicker" label="By Theme"
							data-live-search="true">
							<aui:option value="">
								<liferay-ui:message key="select.theme" />
							</aui:option>
							<c:forEach items="${themeCategories}" var="theme">
								<aui:option value="${theme.categoryId}" label="${theme.title}" />
							</c:forEach>
						</aui:select>
					</div> --%>
				</div>
			</div>
			<div class="col-lg-auto bcol align-self-end sp-991-2">
				<button class="btn-1 btn-go-up" type="submit">Go</button>
			</div>
		</div>
	</aui:form>
</div>

<script type="text/javascript">
	<%-- function loadOption(url, target) {
		jQuery.ajax({
			url : url,
			type: "POST",
			dataType : "json",
			async : "false",
			cache : "false",
			success : function(data,textStatus,XMLHttpRequest){
				$.each(data, function(index, value){
					$("#<portlet:namespace/>"+target).append('<option value="'+value.categoryId+'">'+value.title+'</option >');
				}); 
			},
			error : function(data,textStatus,XMLHttpRequest){
				console.log("failed");
			}
		});
	}
	$(document).ready(function() {
		loadOption("<%=topicResourceURL.toString()%>", 'topics');
		loadOption("<%=themeResourceURL.toString()%>", 'themes');
	}); --%>
</script>

package web.ntuc.nlh.search.constants;

public class SearchMessagesKey {

	public static String XSS_VALIDATION_NOT_PASS = "xss-validation-not-pass";

	public static String SUCCESS_SAVE_GROUP = "success-save-parameter-group";
	public static String FAILED_SAVE_GROUP = "failed-save-parameter-group";
	public static String PARAMETER_GROUP_EXIST = "error-group-exist";
	public static String SUCCESS_DELETE_GROUP = "success-delete-parameter-group";
	public static String FAILED_DELETE_GROUP = "failed-delete-parameter-group";
	public static String GROUP_HAS_PARAMETER = "parameter-group-has-active-parameter";

	public static String SUCCESS_SAVE_PARAMETER = "success-save-parameter";
	public static String FAILED_SAVE_PARAMETER = "failed-save-parameter";
	public static String SUCCESS_DELETE_PARAMETER = "success-delete-parameter";
	public static String FAILED_DELETE_PARAMETER = "failed-delete-parameter";
	public static String PARAMETER_EXIST = "error-parameter-exist";

}

package web.ntuc.nlh.search.constants;

public class MVCCommandNames {

	public static final String SEARCH_ACTION = "searchAction";
	public static final String SEARCH_RESULT_RENDER = "searchResultRender";
	public static final String SEARCH_ACTION_TARGET = "/search-result";
	public static final String TOPIC_DATA_RESOURCES = "topicDataResources";
	public static final String THEME_DATA_RESOURCES = "themeDataResources";
}

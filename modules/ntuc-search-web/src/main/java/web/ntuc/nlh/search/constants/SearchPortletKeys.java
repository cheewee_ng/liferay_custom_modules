package web.ntuc.nlh.search.constants;

/**
 * @author fandifadillah
 */
public class SearchPortletKeys {

	public static final String SEARCH_PORTLET = "web_ntuc_nlh_search_SearchPortlet";
	public static final String SEARCH_DISPLAY = "Search Course";
	public static final String SEARCH_THEME = "course_theme";
	public static final String ASSET_VOCAB_THEME = "course theme";
	public static final String ASSET_VOCAB_TOPIC = "course topic";

}
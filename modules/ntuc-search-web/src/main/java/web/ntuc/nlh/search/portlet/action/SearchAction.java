package web.ntuc.nlh.search.portlet.action;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletMode;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletURL;

import org.osgi.service.component.annotations.Component;

import web.ntuc.nlh.search.constants.MVCCommandNames;
import web.ntuc.nlh.search.constants.SearchMessagesKey;
import web.ntuc.nlh.search.constants.SearchPortletKeys;

@Component(immediate = true, property = { "mvc.command.name=" + MVCCommandNames.SEARCH_ACTION,
		"javax.portlet.name=" + SearchPortletKeys.SEARCH_PORTLET,
		"com.liferay.portlet.action-url-redirect=true" }, service = MVCActionCommand.class)
public class SearchAction extends BaseMVCActionCommand {
	private static Log log = LogFactoryUtil.getLog(SearchAction.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		log.info("Search action - start");

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
//			User user = themeDisplay.getUser();
			long scopeGroupId = themeDisplay.getScopeGroupId();
//			long companyId = themeDisplay.getCompanyId();

			boolean isAuthorized = ParamUtil.getBoolean(actionRequest, "isAuthorized");
			boolean validCSRF = ParamUtil.getBoolean(actionRequest, "validCSRF");
			boolean xssPass = ParamUtil.getBoolean(actionRequest, "xssPass");

			if (!isAuthorized || !validCSRF) {
				String msg = "isAuthorized : " + isAuthorized + " | validCSRF : " + validCSRF;
				SessionErrors.add(actionRequest, "you-dont-have-permission-or-your-session-is-end");
				throw new Exception(msg);
			}

			if (!xssPass) {
				PortletConfig portletConfig = (PortletConfig) actionRequest
						.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
				String msg = LanguageUtil.format(portletConfig.getResourceBundle(themeDisplay.getLocale()),
						SearchMessagesKey.XSS_VALIDATION_NOT_PASS, xssPass);
				SessionErrors.add(actionRequest, "your-input-not-pass-xss");
				throw new Exception(msg);
			}

//			actionResponse.getRenderParameters().setValue("mvcPath", "/search/search-result.jsp");
			/*actionResponse.getRenderParameters().setValue("mvcRenderCommandName", MVCCommandNames.SEARCH_RESULT_RENDER);
			actionResponse.setWindowState(LiferayWindowState.MAXIMIZED);
			actionResponse.setPortletMode(LiferayPortletMode.VIEW);*/
			
			String keywords = ParamUtil.getString(actionRequest, "keywords");
			String groupFriendlyUrl = GroupLocalServiceUtil.getGroup(scopeGroupId).getFriendlyURL();
			String urlPrefix = "/web"+groupFriendlyUrl;
//			String topics = ParamUtil.getString(actionRequest, "topics");
//			String themes = ParamUtil.getString(actionRequest, "themes");
//			log.info("keywords = "+keywords+" topics = "+topics+" themes = "+themes);
			actionResponse.sendRedirect(urlPrefix+MVCCommandNames.SEARCH_ACTION_TARGET+"?course=1&keyword="+keywords);
		} catch (Exception e) {
			log.error("Error while save parameter group: " + e.getMessage(), e);
//			SessionErrors.add(actionRequest, SearchMessagesKey.FAILED_SAVE_GROUP);
		}
		log.info("Search action - end");

	}

}

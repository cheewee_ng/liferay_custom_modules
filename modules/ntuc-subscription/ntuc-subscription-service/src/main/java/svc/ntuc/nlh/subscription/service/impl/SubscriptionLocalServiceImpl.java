/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package svc.ntuc.nlh.subscription.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import svc.ntuc.nlh.subscription.exception.NoSuchSubscriptionException;
import svc.ntuc.nlh.subscription.model.Subscription;
import svc.ntuc.nlh.subscription.service.base.SubscriptionLocalServiceBaseImpl;

/**
 * The implementation of the subscription local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>svc.ntuc.nlh.subscription.service.SubscriptionLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SubscriptionLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=svc.ntuc.nlh.subscription.model.Subscription",
	service = AopService.class
)
public class SubscriptionLocalServiceImpl
	extends SubscriptionLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use <code>svc.ntuc.nlh.subscription.service.SubscriptionLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>svc.ntuc.nlh.subscription.service.SubscriptionLocalServiceUtil</code>.
	 */
	
	Log log = LogFactoryUtil.getLog(SubscriptionLocalServiceBaseImpl.class);
	
	public List<Subscription> getAllSubscription(){
		log.info("Search All Subscription");
		return subscriptionPersistence.findAll();
	}
	
	public List<Subscription> getSubscriptionByGroupId(long groupId){
		log.info("Get Subscription by Group Id");
		return subscriptionPersistence.findByGroupId(groupId);
	}
	
	public Subscription getSubscriptionByEmail(long groupId, String email) throws NoSuchSubscriptionException {
		return subscriptionPersistence.findByEmail(groupId, email);
	}
	
	public List<Subscription> getSubscriptionByName(String name){
		return subscriptionPersistence.findByName(name);
	}
	
}
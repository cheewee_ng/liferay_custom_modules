create index IX_63F6C59C on NTUC_SUBSCRIPTION_Subscription (groupId, email[$COLUMN_LENGTH:75$]);
create index IX_99867279 on NTUC_SUBSCRIPTION_Subscription (name[$COLUMN_LENGTH:75$]);
create index IX_E5765DAE on NTUC_SUBSCRIPTION_Subscription (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_F2B677B0 on NTUC_SUBSCRIPTION_Subscription (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_5609E925 on SUBSCRIPTION_Subscription (groupId);
create index IX_A801E7D9 on SUBSCRIPTION_Subscription (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_6864909B on SUBSCRIPTION_Subscription (uuid_[$COLUMN_LENGTH:75$], groupId);
create table NTUC_SUBSCRIPTION_Subscription (
	uuid_ VARCHAR(75) null,
	subscriptionId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	email VARCHAR(75) null,
	name VARCHAR(75) null,
	contentPreference VARCHAR(75) null,
	agreement BOOLEAN
);

create table SUBSCRIPTION_Subscription (
	uuid_ VARCHAR(75) null,
	subscriptionId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	email VARCHAR(75) null,
	name VARCHAR(75) null,
	contentPreference VARCHAR(75) null
);
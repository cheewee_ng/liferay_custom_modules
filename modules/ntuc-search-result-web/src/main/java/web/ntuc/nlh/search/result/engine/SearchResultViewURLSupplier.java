package web.ntuc.nlh.search.result.engine;

public abstract interface SearchResultViewURLSupplier {

	public abstract String getSearchResultViewURL();
	
}

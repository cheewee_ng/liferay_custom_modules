package web.ntuc.nlh.search.result.engine;

public abstract interface SearchResultPreferences {

	public abstract boolean isDisplayResultsInDocumentForm();

	public abstract boolean isHighlightEnabled();

	public abstract boolean isViewInContext();

}

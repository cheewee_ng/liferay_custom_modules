package web.ntuc.nlh.search.result.constants;

/**
 * @author fandifadillah
 */
public class SearchResultPortletKeys {

	public static final String SEARCH_RESULT_PORTLET =
		"web_ntuc_nlh_search_result_SearchResultPortlet";
	public static final String SEARCH_RESULT_DISPLAY =
			"Search Result";
	public static final String PARAMETER_KEYWORD = "keyword";
//	public static final String PARAMETER_THEME = "theme";
//	public static final String PARAMETER_TOPIC = "topic";
	public static final String PARAMETER_COURSE = "course";
	public static final String TOPIC_VOCABULARY_NAME = "search topic";
	public static final String PARAMETER_GLOBAL_GROUP_CODE = "globalParam";
	public static final String PARAMETER_GLOBAL_SEARCH_TOPIC_CODE = "searchTopic";
	public static final String PARAMETER_COURSE_STRUCTURE_CODE = "courseStructure";
	public static final String PARAMETER_COURSE_TEMPLATE_CODE = "courseTemplate";
	public static final String PARAMETER_COURSE_ADMIN_GROUP_CODE = "courseAdmin";
	public static final String COURSE_CATEGORY_NAME = "courses";
	public static final String BLOGS_CATEGORY_NAME = "blogs";
	public static final String PAGES_CATEGORY_NAME = "pages";
	public static final String PRESS_CATEGORY_NAME = "press releases";
	public static final String NEWS_CATEGORY_NAME = "news";
	public static final String COURSE_CATEGORY_GROUP_CODE= "courseCategory";
	public static final String COURSE_TOPIC_NAME = "courseTopicName";
	public static final String COURSE_THEME_NAME = "courseThemeName";
}
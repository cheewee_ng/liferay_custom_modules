package web.ntuc.nlh.search.result.dto;

public class FilterDto {
	private Long filterId;
	private String filterName;
	private String category;

	public FilterDto() {
		super();
	}

	public FilterDto(Long filterId, String filterName, String category) {
		super();
		this.filterId = filterId;
		this.filterName = filterName;
		this.category = category;
	}

	public Long getFilterId() {
		return filterId;
	}

	public void setFilterId(Long filterId) {
		this.filterId = filterId;
	}

	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "FilterDto [filterId=" + filterId + ", filterName=" + filterName + ", category=" + category + "]";
	}
}

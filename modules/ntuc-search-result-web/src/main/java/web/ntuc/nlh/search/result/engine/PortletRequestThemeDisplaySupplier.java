package web.ntuc.nlh.search.result.engine;

import com.liferay.portal.kernel.theme.ThemeDisplay;

import javax.portlet.PortletRequest;

public class PortletRequestThemeDisplaySupplier implements ThemeDisplaySupplier {
	
	private final PortletRequest _portletRequest;

	public PortletRequestThemeDisplaySupplier(PortletRequest portletRequest) {
		this._portletRequest = portletRequest;
	}

	public ThemeDisplay getThemeDisplay() {
		return (ThemeDisplay) this._portletRequest.getAttribute("LIFERAY_SHARED_THEME_DISPLAY");
	}
}

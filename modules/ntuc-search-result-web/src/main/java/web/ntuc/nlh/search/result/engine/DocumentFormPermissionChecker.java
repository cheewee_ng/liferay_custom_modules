package web.ntuc.nlh.search.result.engine;

public abstract interface DocumentFormPermissionChecker {

	public abstract boolean hasPermission();
}

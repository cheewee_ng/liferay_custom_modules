package web.ntuc.nlh.search.result.portlet.resource;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.portlet.MimeResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import api.ntuc.nlh.content.util.ContentUtil;
import svc.ntuc.nlh.parameter.model.Parameter;
import svc.ntuc.nlh.parameter.model.ParameterGroup;
import svc.ntuc.nlh.parameter.service.ParameterGroupLocalServiceUtil;
import svc.ntuc.nlh.parameter.service.ParameterLocalServiceUtil;
import web.ntuc.nlh.search.result.constants.MVCCommandNames;
import web.ntuc.nlh.search.result.constants.SearchResultMessageKey;
import web.ntuc.nlh.search.result.constants.SearchResultPortletKeys;
import web.ntuc.nlh.search.result.dto.FilterDto;
import web.ntuc.nlh.search.result.dto.SearchResultDto;
import web.ntuc.nlh.search.result.engine.PortletRequestThemeDisplaySupplier;
import web.ntuc.nlh.search.result.engine.PortletURLFactory;
import web.ntuc.nlh.search.result.engine.PortletURLFactoryImpl;
import web.ntuc.nlh.search.result.engine.SearchPortletSearchResultPreferences;
import web.ntuc.nlh.search.result.engine.SearchResultPreferences;
import web.ntuc.nlh.search.result.engine.SearchResultSummaryDisplayBuilder;
import web.ntuc.nlh.search.result.engine.SearchResultSummaryDisplayContext;
import web.ntuc.nlh.search.result.engine.ThemeDisplaySupplier;

@Component(immediate = true, property = { "mvc.command.name=" + MVCCommandNames.SEARCH_DATA_RESOURCES,
		"javax.portlet.name=" + SearchResultPortletKeys.SEARCH_RESULT_PORTLET }, service = MVCResourceCommand.class)
public class SearchResultDataResource implements MVCResourceCommand {

	private static Log log = LogFactoryUtil.getLog(SearchResultDataResource.class);

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException {
		log.info("search result data resources - start");
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long localGroupId = themeDisplay.getScopeGroupId();

//			get default company id for topic vocabulary
			Company company = _companyLocalService.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
			long globalGroupId = company.getGroup().getGroupId(); //themeDisplay.getScopeGroupId()
			log.info("global id = "+globalGroupId);
			boolean isAuthorized = ParamUtil.getBoolean(resourceRequest, "isAuthorized");
			boolean validCSRF = ParamUtil.getBoolean(resourceRequest, "validCSRF");
			boolean xssPass = ParamUtil.getBoolean(resourceRequest, "xssPass");
			String keyword = ParamUtil.getString(resourceRequest, "keyword", " ");

			long course = ParamUtil.getInteger(resourceRequest, "course", 0);

			if (!isAuthorized || !validCSRF) {
				String msg = "isAuthorized : " + isAuthorized + " | validCSRF : " + validCSRF;
				throw new Exception(msg);
			}

			if (!xssPass) {
				PortletConfig portletConfig = (PortletConfig) resourceRequest
						.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
				String msg = LanguageUtil.format(portletConfig.getResourceBundle(themeDisplay.getLocale()),
						SearchResultMessageKey.XSS_VALIDATION_NOT_PASS, xssPass);
				throw new Exception(msg);
			}
			HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
			
			
			List<SearchResultDto> courses = new ArrayList<SearchResultDto>();
			List<Long> categoryIds = new ArrayList<Long>();
//			if (Validator.isNotNull(theme) && !Validator.isBlank(String.valueOf(theme))) {
//				categoryIds.add(theme);
//			}
//			if (Validator.isNotNull(topic) && !Validator.isBlank(String.valueOf(topic))) {
//				categoryIds.add(topic);
//			}
//			log.info("categoryIds.size() = "+categoryIds.size());
			
			if(course == 1) {
//				log.info("masuk sini");
//				AssetVocabulary topicVocabulary = _assetVocabularyLocalService.fetchGroupVocabulary(scopeGroupId,
//						SearchResultPortletKeys.TOPIC_VOCABULARY_NAME);
				ParameterGroup groupGlobalParam = ParameterGroupLocalServiceUtil.getByCode(SearchResultPortletKeys.PARAMETER_GLOBAL_GROUP_CODE, false);
				long paramGroupId =groupGlobalParam.getGroupId();
				Parameter globalTopicParam = ParameterLocalServiceUtil.getByGroupCode(paramGroupId, groupGlobalParam.getParameterGroupId(), SearchResultPortletKeys.PARAMETER_GLOBAL_SEARCH_TOPIC_CODE, false);
//				log.info("globalTopicParam value = "+globalTopicParam.getParamValue());
				AssetVocabulary topicVocabulary = _assetVocabularyLocalService.fetchGroupVocabulary(globalGroupId,
						globalTopicParam.getParamValue());
//				log.info("topic vocabulary = "+topicVocabulary.getName() );
				
				OrderByComparator<AssetCategory> order = null;
				List<AssetCategory> assetCategories = _assetCategoryLocalService
						.getVocabularyCategories(topicVocabulary.getVocabularyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS,
								order)
						.stream().filter(category -> category.getName().equals(SearchResultPortletKeys.COURSE_CATEGORY_NAME)).collect(Collectors.toList());
				for (AssetCategory asset : assetCategories) {
					categoryIds.add(asset.getCategoryId());

				}
			}
			
			log.info("categoryIds.size() = "+categoryIds.size());

			
			SearchContext searchContext = SearchContextFactory.getInstance(request);
			searchContext.setKeywords("\""+keyword+"\"");
			searchContext.setAttribute("paginationType", "more");
//			searchContext.setAssetCategoryIds(categoryIdArray);
			searchContext.getQueryConfig().setCollatedSpellCheckResultEnabled(true);
			searchContext.getQueryConfig().setCollatedSpellCheckResultScoresThreshold(200);
			searchContext.getQueryConfig().setQueryIndexingEnabled(false);
			searchContext.getQueryConfig().setQueryIndexingThreshold(50);
			searchContext.getQueryConfig().setQuerySuggestionEnabled(true);
			searchContext.getQueryConfig().setQuerySuggestionScoresThreshold(0);
			searchContext.getQueryConfig().setQuerySuggestionsMax(5);

//			Adding course

			
//			Long[] categoryIdsArray = new Long[categoryIds.size()];
//			categoryIds.toArray(categoryIdsArray);

			List<SearchResultSummaryDisplayContext> resultCourse = this.buildResultSummary(categoryIds, searchContext,
					themeDisplay, request, resourceRequest, resourceResponse);
			
			List<FilterDto> fundedOptionList = new ArrayList<FilterDto>();
			String[] fundedList = { "Funded", "Not Funded" };
			for (int i = 0; i < fundedList.length; i++) {
				FilterDto dto = new FilterDto();
				Long id = new Long(i);
				dto.setFilterId(id);
				dto.setFilterName(fundedList[i]);
				dto.setCategory("funded");
				fundedOptionList.add(dto);
			}
			
			List<AssetCategory> topicList = AssetCategoryLocalServiceUtil.getCategories();
			List<FilterDto> filterTheme = new ArrayList<FilterDto>();
			List<FilterDto> filterTopic = new ArrayList<FilterDto>();
			ParameterGroup courseCategoryParameterGroup = ParameterGroupLocalServiceUtil.getByCode(SearchResultPortletKeys.COURSE_CATEGORY_GROUP_CODE, false);
			Parameter courseTopicParameter = ParameterLocalServiceUtil.getByGroupCode(courseCategoryParameterGroup.getGroupId(), courseCategoryParameterGroup.getParameterGroupId(), SearchResultPortletKeys.COURSE_TOPIC_NAME, false);
			Parameter courseThemeParameter = ParameterLocalServiceUtil.getByGroupCode(courseCategoryParameterGroup.getGroupId(), courseCategoryParameterGroup.getParameterGroupId(), SearchResultPortletKeys.COURSE_THEME_NAME, false);
//			List<AssetVocabulary> assetVocabularies = AssetVocabularyLocalServiceUtil.getGroupVocabularies(localGroupId);
			
			AssetVocabulary themeVocabulary = _assetVocabularyLocalService.getGroupVocabulary(localGroupId, courseThemeParameter.getParamValue());
			AssetVocabulary topicVocabulary = _assetVocabularyLocalService.getGroupVocabulary(localGroupId, courseTopicParameter.getParamValue());
			
			for (SearchResultSummaryDisplayContext res : resultCourse) {
//				for(long a : res.getCategoryIds()) {
//					log.info("category ids = "+a);
//				}
				List<Long> AllTopicIds = new ArrayList<Long>();
				List<Long> AllThemeIds = new ArrayList<Long>();
				Long[] topicCategories = {};
				Long[] themeCategories = {};
//				
				SearchResultDto v = new SearchResultDto();
				long[] resCategoryIds = res.getCategoryIds();
				if(resCategoryIds != null) {
					for(long resCategoryId : resCategoryIds) {
//						assetVocabularies.stream().filter(vocab -> vocab.getVocabularyId() == resCategory).collect(Collectors.toList());
						OrderByComparator<AssetCategory> order = null;
						List<Long> topicIds = _assetCategoryLocalService
								.getVocabularyCategories(topicVocabulary.getVocabularyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS,
										order)
								.stream().filter(category -> category.getCategoryId() == resCategoryId).map(AssetCategory -> AssetCategory.getCategoryId()).collect(Collectors.toList());
						AllTopicIds.addAll(topicIds);
						List<Long> themeIds = _assetCategoryLocalService
								.getVocabularyCategories(themeVocabulary.getVocabularyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS,
										order)
								.stream().filter(category -> category.getCategoryId() == resCategoryId).map(AssetCategory -> AssetCategory.getCategoryId()).collect(Collectors.toList());
						AllThemeIds.addAll(themeIds);
					}
				}
				
				/*for (long a : res.getCategoryIds()) {
//					log.info("category ids = "+a);
					AssetCategory ac = AssetCategoryLocalServiceUtil.getCategory(a);
//					log.info(ac.toString());
//					log.info("Content Group Id : " + ac.getGroupId());
					for (AssetCategory assetCat : topicList) {
						if (assetCat.getCategoryId() == ac.getCategoryId() && ac.getGroupId() == localGroupId) {
							if (!topicId.contains(a) && ac.getVocabularyId() != getCoursesVocabularyId(theme)) {
								topicId.add(ac.getCategoryId());
//								topicCategories = topicId.toArray(topicCategories);
							}

							if (!themeId.contains(a) && ac.getVocabularyId() == getCoursesVocabularyId(theme)) {
								themeId.add(ac.getCategoryId());
//								themeCategories = themeId.toArray(themeCategories);
							}
						}
					}
				}*/
				v.setTopicIds(AllTopicIds.toArray(topicCategories));
				v.setThemeIds(AllThemeIds.toArray(themeCategories));
				
//				v.setStructure("product");
				v.setTitle(res.getHighlightedTitle());
				v.setDesc(res.getContent());
				if (res.getClassName().equals(JournalArticle.class.getName())) {
					JournalArticle journalArticle = (JournalArticle) res.getAssetObject();
					try {
						String xmlContent = journalArticle.getContent();
						String imageUrl = ContentUtil.getImageUrl(xmlContent, 1, "image", themeDisplay);
//						String detailUrl = ContentUtil.getLinkUrl(xmlContent, 1, "URLDetail", themeDisplay);

						v.setUrlImage(imageUrl);
//						v.setUrlMore(detailUrl);
						String status = ContentUtil.getWebContentVal(xmlContent, 1, "status", themeDisplay);
						String price = ContentUtil.getWebContentVal(xmlContent, 1, "price", themeDisplay);
						String funded = ContentUtil.getWebContentVal(xmlContent, 1, "funded", themeDisplay);
						String popular = ContentUtil.getWebContentVal(xmlContent, 1, "popular", themeDisplay);

						v.setStatus(status);
						v.setUrlImage(imageUrl);
						
						if (price.equals("")) {
							v.setPrice(0.0);
						}
						v.setPrice(Double.valueOf(price));
//						else {
//							v.setPrice(price);
//						}
						
						if (funded.equals("true")) {
							v.setFunded("FUNDED");
							v.setStatus("Before Funding");
						}else {
							v.setFunded("NOT FUNDED");
						}

						if (popular.equals("true")) {
							v.setPopular("POPULAR");
						} 
					} catch (Exception e) {
						v.setUrlImage("");
					}
				}

				// add data when viewURL not null
				if (Validator.isNull(v.getUrlMore())) {
					if (res.isAssetRendererURLDownloadVisible()) {
						v.setUrlMore(res.getAssetRendererURLDownload());
					} else {
						if (Validator.isNotNull(res.getViewURL())) {
							v.setUrlMore(res.getViewURL());
						}
					}
				}
				
				for (Long topicCategoryId : v.getTopicIds()) {
					FilterDto dto = new FilterDto();
					AssetCategory temp = _assetCategoryLocalService.getCategory(topicCategoryId);
					dto.setFilterId(temp.getCategoryId());
					dto.setFilterName(temp.getName());
					dto.setCategory("topicIds");
//					log.info("Dto : " + dto.toString());

					if (!filterTopic.contains(dto)) {
						filterTopic.add(dto);
//						log.info("Topic : " + filterTopic.toString());

					}
				}

				for (Long themeCategoryId : v.getThemeIds()) {
					FilterDto dto = new FilterDto();
					AssetCategory temp = _assetCategoryLocalService.getCategory(themeCategoryId);
					dto.setFilterId(temp.getCategoryId());
					dto.setFilterName(temp.getName());
					dto.setCategory("themeIds");
//					log.info("Dto : " + dto.toString());

					if (!filterTheme.contains(dto)) {
						filterTheme.add(dto);
//						log.info("Theme : " + filterTheme.toString());
					}
				}
				
				courses.add(v);
			}

//			if search is not blank then show the "all result view" on the bottom of the page
//			List<SearchResultDto> others = new ArrayList<SearchResultDto>();
			List<SearchResultDto> blogs = new ArrayList<SearchResultDto>();
			List<SearchResultDto> pages = new ArrayList<SearchResultDto>();
			List<SearchResultDto> pressReleases = new ArrayList<SearchResultDto>();
			List<SearchResultDto> news = new ArrayList<SearchResultDto>();

			if (course == 0) {
				log.info("search not blank");
				blogs.addAll(this.getDtoResult(globalGroupId ,localGroupId, SearchResultPortletKeys.BLOGS_CATEGORY_NAME, searchContext, themeDisplay, request, resourceRequest, resourceResponse));
				pages.addAll(this.getDtoResult(globalGroupId ,localGroupId, SearchResultPortletKeys.PAGES_CATEGORY_NAME, searchContext, themeDisplay, request, resourceRequest, resourceResponse));
				pressReleases.addAll(this.getDtoResult(globalGroupId ,localGroupId, SearchResultPortletKeys.PRESS_CATEGORY_NAME, searchContext, themeDisplay, request, resourceRequest, resourceResponse));
				news.addAll(this.getDtoResult(globalGroupId ,localGroupId, SearchResultPortletKeys.NEWS_CATEGORY_NAME, searchContext, themeDisplay, request, resourceRequest, resourceResponse));
			}
			
			List<FilterDto> newTopicList = new ArrayList<FilterDto>();
			List<FilterDto> newThemeList = new ArrayList<FilterDto>();
			
			HashSet<Long> removeDuplicateTopicId = new HashSet<Long>();
			filterTopic.removeIf(e->!removeDuplicateTopicId.add(e.getFilterId()));
			
			HashSet<Long> removeDuplicateThemeId = new HashSet<Long>();
			filterTheme.removeIf(e->!removeDuplicateThemeId.add(e.getFilterId()));
			
			for (Long topicId : removeDuplicateTopicId) {
				FilterDto dto = new FilterDto();
				AssetCategory ac = _assetCategoryLocalService.getCategory(topicId);
				dto.setFilterId(ac.getCategoryId());
				dto.setFilterName(ac.getName());
				dto.setCategory("topicIds");
				newTopicList.add(dto);
			}
			
			for (Long themeId : removeDuplicateThemeId) {
				FilterDto dto = new FilterDto();
				AssetCategory ac = _assetCategoryLocalService.getCategory(themeId);
				dto.setFilterId(ac.getCategoryId());
				dto.setFilterName(ac.getName());
				dto.setCategory("themeIds");
				newThemeList.add(dto);
				
			}
			
			SearchResultDto maxCourse = courses.stream().max(Comparator.comparingDouble(SearchResultDto::getPrice)).orElseThrow(NoSuchElementException::new);
//			courses.stream().forEach(c -> log.info(c.getPrice()));
			resourceResponse.setContentType("application/json");
			PrintWriter out = null;
			out = resourceResponse.getWriter();
			
			JSONObject rowData = JSONFactoryUtil.createJSONObject();
			rowData.put("filterTheme", newThemeList);
			rowData.put("filterTopic", newTopicList);
			rowData.put("maxPrice", maxCourse.getPrice());
			rowData.put("fundedList", fundedOptionList);
			rowData.put("courses", courses);
			rowData.put("pages", pages);
			rowData.put("blogs", blogs);
			rowData.put("pressReleases", pressReleases);
			rowData.put("news", news);
			rowData.put("status", HttpServletResponse.SC_OK);
			out.print(rowData.toString());
			out.flush();
		} catch (Exception e) {
			log.error("Error while searching result data : " + e.getMessage());
			e.printStackTrace();
			return true;
		}
		log.info("search result data resources - end");
		return false;
	}

	@SuppressWarnings("unchecked")
	private List<SearchResultSummaryDisplayContext> buildResultSummary(List<Long> categoryIds,
			SearchContext searchContext, ThemeDisplay themeDisplay, HttpServletRequest request,
			ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {
		Indexer<JournalArticle> indexer = IndexerRegistryUtil.getIndexer(JournalArticle.class);

		BooleanQuery booleanQuery = new BooleanQueryImpl();
//		DDMStructure productStructure = ContentUtil.getStructure(themeDisplay.getScopeGroupId(), structure, false);
//		if(Validator.isNotNull(theme)) {
//			booleanQuery.addExactTerm("assetCategoryIds", theme);
//		}
//		if(Validator.isNotNull(topic)) {
//			booleanQuery.addExactTerm("assetCategoryIds", topic);
//		}

		if (categoryIds.size() > 0) {
			for (Long id : categoryIds) {
				booleanQuery.addExactTerm("assetCategoryIds", id);
				log.info("id = "+id);
			}
		}
		
		ParameterGroup groupCourseAdmin = ParameterGroupLocalServiceUtil.getByCode(SearchResultPortletKeys.PARAMETER_COURSE_ADMIN_GROUP_CODE, false);
		long siteGroupId = groupCourseAdmin.getGroupId();
		Parameter structureParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId, groupCourseAdmin.getParameterGroupId(), SearchResultPortletKeys.PARAMETER_COURSE_STRUCTURE_CODE, false);
		DynamicQuery structureQuery = DDMStructureLocalServiceUtil.dynamicQuery();
		structureQuery.add(PropertyFactoryUtil.forName("name")
				.like("%>" + structureParam.getParamValue() + "<%"));
		structureQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
		List<DDMStructure> ddmStructures = DDMStructureLocalServiceUtil.dynamicQuery(structureQuery, 0, 1);
		
		booleanQuery.addExactTerm("ddmStructureKey", ddmStructures.get(0).getStructureKey());
		BooleanClause<Query> booleanClause = BooleanClauseFactoryUtil.create(booleanQuery,
				BooleanClauseOccur.MUST.getName());
		searchContext.setBooleanClauses(new BooleanClause[] { booleanClause });
//		final MultiValueFacet fieldFacet = new MultiValueFacet(searchContext);
//		fieldFacet.setFieldName("roleId");
//		FacetConfiguration config = fieldFacet.getFacetConfiguration();
//		JSONObject obj = JSONFactoryUtil.createJSONObject();
//		obj.put("maxTerms", 500);
//		config.setDataJSONObject(obj);
//		fieldFacet.setFacetConfiguration(config);
//		searchContext.addFacet(fieldFacet);
		Hits hits = indexer.search(searchContext);

//		final List<String> terms = new ArrayList<>();
//		for (final TermCollector collector : fieldFacet.getFacetCollector().getTermCollectors()) {
//			terms.add(collector.getTerm());
//			log.info("term = "+collector.getTerm());
//		}
//		log.info("Terms size = "+terms.size());
		
//		log.info("hits.getQueryTerms() : " + hits.getQueryTerms().toString());
		log.info("hits.getLength() = "+hits.getLength());
		List<SearchResultSummaryDisplayContext> results = new ArrayList<SearchResultSummaryDisplayContext>();

		List<Document> documents = hits.toList();
		for (Document document : documents) {
//			log.info("assetCategoryIds = "+document.get(Field.ASSET_CATEGORY_IDS));
//			log.info("roleId = "+document.get("roleId"));
//			log.info("title = "+document.get("title_en_US"));
			SearchResultSummaryDisplayBuilder searchResultSummaryDisplayBuilder = new SearchResultSummaryDisplayBuilder();
			searchResultSummaryDisplayBuilder.setAssetEntryLocalService(AssetEntryLocalServiceUtil.getService());
			searchResultSummaryDisplayBuilder.setCurrentURL(request.getRequestURI());
			searchResultSummaryDisplayBuilder.setDocument(document);
			searchResultSummaryDisplayBuilder.setHighlightEnabled(true);
			searchResultSummaryDisplayBuilder.setLanguage(LanguageUtil.getLanguage());
			searchResultSummaryDisplayBuilder.setLocale(request.getLocale());
			MimeResponse mimeResponse = (MimeResponse) resourceResponse;
			PortletURLFactory portletURLFactory = new PortletURLFactoryImpl(resourceRequest, mimeResponse);
			searchResultSummaryDisplayBuilder.setPortletURLFactory(portletURLFactory);
			searchResultSummaryDisplayBuilder.setQueryTerms(hits.getQueryTerms());
			searchResultSummaryDisplayBuilder.setResourceRequest(resourceRequest);
			searchResultSummaryDisplayBuilder.setResourceResponse(resourceResponse);
			searchResultSummaryDisplayBuilder.setRequest(request);
			searchResultSummaryDisplayBuilder.setResourceActions(ResourceActionsUtil.getResourceActions());
			ThemeDisplaySupplier themeDisplaySupplier = new PortletRequestThemeDisplaySupplier(resourceRequest);
			SearchResultPreferences searchResultPreferences = new SearchPortletSearchResultPreferences(
					resourceRequest.getPreferences(), themeDisplaySupplier);
			searchResultSummaryDisplayBuilder.setSearchResultPreferences(searchResultPreferences);
			searchResultSummaryDisplayBuilder.setThemeDisplay(themeDisplay);

			SearchResultSummaryDisplayContext searchResultSummaryDisplayContext = searchResultSummaryDisplayBuilder
					.build();

			results.add(searchResultSummaryDisplayContext);
		}

		return results;
	}

	private List<SearchResultDto> getDtoResult(long globalGroupId, long localGroupId, String categoryName, SearchContext searchContext,
			ThemeDisplay themeDisplay, HttpServletRequest request, ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) {
		try {
			ParameterGroup groupGlobalParam = ParameterGroupLocalServiceUtil.getByCode(SearchResultPortletKeys.PARAMETER_GLOBAL_GROUP_CODE, false);
			Parameter globalTopicParam = ParameterLocalServiceUtil.getByGroupCode(localGroupId, groupGlobalParam.getParameterGroupId(), SearchResultPortletKeys.PARAMETER_GLOBAL_SEARCH_TOPIC_CODE, false);
			AssetVocabulary topicVocabulary = _assetVocabularyLocalService.fetchGroupVocabulary(globalGroupId,
					globalTopicParam.getParamValue());
//			log.info("topic vocab = "+topicVocabulary.getName());
//			AssetVocabulary topicVocabulary = _assetVocabularyLocalService.fetchGroupVocabulary(globalGroupId,
//					SearchResultPortletKeys.TOPIC_VOCABULARY_NAME);
			OrderByComparator<AssetCategory> order = null;
			List<AssetCategory> assetCategories = _assetCategoryLocalService
					.getVocabularyCategories(topicVocabulary.getVocabularyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS,
							order)
					.stream().filter(category -> category.getName().equals(categoryName)).collect(Collectors.toList());
			List<Long> assetCategoryIds = new ArrayList<Long>();
			for (AssetCategory asset : assetCategories) {
				assetCategoryIds.add(asset.getCategoryId());

			}

			List<SearchResultSummaryDisplayContext> resultOther = this.buildResultSummary(assetCategoryIds,
					searchContext, themeDisplay, request, resourceRequest, resourceResponse);
			List<SearchResultDto> dtos = new ArrayList<SearchResultDto>();
			for (SearchResultSummaryDisplayContext res : resultOther) {
				SearchResultDto v = new SearchResultDto();
//			v.setStructure("product");
//			log.info("res.getFieldAssetCategoryIds() = "+res.getFieldAssetCategoryIds());
				v.setTitle(res.getHighlightedTitle());
				v.setDesc(res.getContent());
				if (res.getClassName().equals(JournalArticle.class.getName())) {
					JournalArticle journalArticle = (JournalArticle) res.getAssetObject();
					try {
						String xmlContent = journalArticle.getContent();
						String imageUrl = ContentUtil.getImageUrl(xmlContent, 1, "image", themeDisplay);
//					String detailUrl = ContentUtil.getLinkUrl(xmlContent, 1, "URLDetail", themeDisplay);

						v.setUrlImage(imageUrl);
						
//					v.setUrlMore(detailUrl);
					} catch (Exception e) {
						v.setUrlImage("");
					}
				}

				// add data when viewURL not null
				if (Validator.isNull(v.getUrlMore())) {
					if (res.isAssetRendererURLDownloadVisible()) {
						v.setUrlMore(res.getAssetRendererURLDownload());
					} else {
						if (Validator.isNotNull(res.getViewURL())) {
							v.setUrlMore(res.getViewURL());
						}
					}
				}

				dtos.add(v);
			}
			return dtos;
		} catch (Exception e) {
			log.error("Error while searching result data : " + e.getMessage());
		}
		return null;
	}

	@Reference
	AssetVocabularyLocalService _assetVocabularyLocalService;

	@Reference
	AssetCategoryLocalService _assetCategoryLocalService;

	@Reference
	CompanyLocalService _companyLocalService;

}

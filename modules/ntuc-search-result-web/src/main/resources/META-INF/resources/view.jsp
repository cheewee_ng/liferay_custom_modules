<%@ include file="/init.jsp"%>

<portlet:resourceURL id="<%=MVCCommandNames.SEARCH_DATA_RESOURCES %>" var="searchResourceURL">
	<portlet:param name="authToken" value="${authToken}" />
	<portlet:param name="keyword" value="${keyword}" />
	<portlet:param name="course" value="${dataCourse}" />
	
</portlet:resourceURL>

<div class="main-wrap" style="padding-bottom: 382.734px;">
	<div class="container sp-main-bot sp-main-top">
		<ul class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Home</a></li>
			<li class="breadcrumb-item">Search Results</li>
			<li class="breadcrumb-item active">"${keyword}"</li>
		</ul>
		
		<h3 class="title-1 center-mb">Search Results for "${keyword}"</h3>
		<div class="row filter-wrap-2">
			<div class="col-mdauto">
				<div class="show-control">
					<a href="#" class="control btn-4"><i class="fas fa-filter"></i>
						Filter</a>
					<div class="sub-content">
						<div class="row">
							<div class="col-6">Advance Filters</div>
							<div class="col-6 text-right">
								<a class="clear-btn" data-form-id="filter" href="#"
									onclick="resetFilter(); return false;">Clear All</a>
							</div>
						</div>
						<form id="filter">
							<div class="lb-1 mb-2">Theme</div>
							<div class="row sp-row-1 break-425" id="filter-theme">
								<!-- INSERTED BY JS -->
							</div>
							<div class="lb-1 mb-2">Topic</div>
							<div class="row sp-row-1 break-425" id="filter-topic">
								<!-- INSERTED BY JS -->
							</div>
							<div class="lb-1 mb-2">Funded/Non Funded</div>
							<div class="row sp-row-1 break-425" id="filter-funded">
								<!-- INSERTED BY JS -->
							</div>
							<div class="lb-1 mb-2">Price</div>
							<div class="container">
								<div class="row sp-row-1 break-425">
									<div class="col-xl-6 col-lg-4 col-6 bcol">
										<div id="slider-range"></div>
									</div>
								</div>
								<div class="row sp-row-1 break-425 slider-labels">
									<div class="col-xl-2 caption">
										<strong>Min:</strong> <span id="slider-range-value1"></span>
									</div>
									<div class="col-xl-4 text-right caption">
										<strong>Max:</strong> <span id="slider-range-value2"></span>
									</div>
								</div>
								<div class="row sp-row-1 break-425">
									<div class="col-xl-5 col-lg-4 col-6 bcol">
										<input type="hidden" name="min-value" value=""> <input
											type="hidden" name="max-value" value="">
									</div>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
			<div class="col-md last">
				<div class="select-wrap">
					<div class="dropdown">
						<select class="filter-price" id="price"
							onchange="filterCourses(this); return false;">
							<option value="asc" id="low-to-high">Price (Low to High)</option>
							<option value="desc" id="high-to-low">Price (High to
								Low)</option>
						</select>

					</div>
				</div>
			</div>
		</div>
		<div id="courses-result" class="row sp-row-2 grid-1 break-480">
		<div class="loader"></div>
			<!-- INSERTED BY JS -->
		</div>
	</div>
</div>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/noUi-slider.js"></script>
	
<script type="text/javascript">
	var courses = "";
	var maxPrice = "";
	var minSliderPrice = 0;
	var maxSliderPrice = 0;
	
	$(document).ready(function() {
		loadResult("<%=searchResourceURL.toString()%>").done(initSlider);
	});
	
	 function initSlider(data) {
    	 // Price Slider
    	var maximumPrice = data.maxPrice;
        $('.noUi-handle').on('click', function() {
        	$(this).width(50);
        });
        
        var rangeSlider = document.getElementById('slider-range');
        var moneyFormat = wNumb({
			decimals: 0,
            thousand: ',',
            prefix: '$'
		});
        maxSliderPrice = maximumPrice;
        document.getElementById('slider-range-value1').innerHTML = minSliderPrice;
        document.getElementById('slider-range-value2').innerHTML = maxSliderPrice;
		noUiSlider.create(rangeSlider, {
            start: [0, maximumPrice],
            step: 10,
            range: {
            	'min': [0],
              	'max': [maximumPrice]
            },
            format: moneyFormat,
            connect: true
		});
          
       	// Set visual min and max values and also update value hidden form inputs
       	rangeSlider.noUiSlider.on('change', function(values, handle) {
            minSliderPrice = values[0];
            maxSliderPrice = values[1];
            
       		document.getElementById('slider-range-value1').innerHTML = values[0];
            document.getElementById('slider-range-value2').innerHTML = values[1];
            document.getElementsByName('min-value').value = moneyFormat.from(values[0]);
            document.getElementsByName('max-value').value = moneyFormat.from(values[1]);
       		filterCourses();
       	});
    }
	 
	function loadResult(url) {
		return jQuery.ajax({
			url : url,
			type : "POST",
			dataType : "json",
			async : "false",
			cache : "false",
			beforeSend: function () { // Before we send the request, remove the .hidden class from the spinner and default to inline-block.
	                $('.loader').removeClass('hidden')
	        },
			success : function(data,textStatus,XMLHttpRequest){
				var htmlCourses = '';
				var htmlPages = '';
				var htmlBlogs = '';
				var htmlPressReleases = '';
				var htmlNews = '';
				var htmlFinal = '';
				var htmlThemeList = '';
	            var htmlTopicList = '';
	            var htmlFundedList = '';
				
				if (data.filterTheme.length > 0) {
                    data.filterTheme.forEach(function(filterTheme) {
                        htmlThemeList += generateFilterList(filterTheme);
                    });
                }

                $('#filter-theme').html(htmlThemeList);

                if (data.filterTopic.length > 0) {
                    data.filterTopic.forEach(function(filterTopic) {
                        htmlTopicList += generateFilterList(filterTopic);
                    });
                }

                $('#filter-topic').html(htmlTopicList);

                if (data.fundedList.length > 0) {
                    data.fundedList.forEach(function(fundedList) {
                        htmlFundedList += generateOtherList(fundedList);
                    });
                }

                $('#filter-funded').html(htmlFundedList);
                
				if(data.courses.length > 0){
					data.courses.forEach(function(course){
						htmlCourses += generateCourseResult(course) ;
						
					});	
				}
				$('#courses-result').html(htmlCourses);
				
				
				if(data.pages.length > 0){
					data.pages.forEach(function(x){
						htmlPages += generateOthersResult('pages',x);
					});
				}
				htmlFinal += htmlPages; 
				
				if(data.blogs.length > 0){
					data.blogs.forEach(function(x){
						htmlBlogs += generateOthersResult('blogs',x);
					});
				}
				htmlFinal += htmlBlogs;
				
				if(data.pressReleases.length > 0){
					data.pressReleases.forEach(function(x){
						htmlPressReleases += generateOthersResult('press releases',x);
					});
				}
				htmlFinal += htmlPressReleases;
				
				if(data.news.length > 0){
					data.news.forEach(function(x){
						htmlNews += generateOthersResult('news',x);
					});
				}
				htmlFinal += htmlNews;
				
				$('#all-result').html(htmlFinal);
				courses = data.courses;
			},
			complete: function () { // Set our complete callback, adding the .hidden class and hiding the spinner.
            	$('.loader').hide();
            },
			error : function(data,textStatus,XMLHttpRequest){
				
			}
		});
	}
	
	function generateCourseResult(data) {
		/*var html = '<div class="item ">';
		html += '<div class="inner imgeffect hoverpp">';
		html += '<figure class="imgwrap">';
		html += '<img src="'+data.urlImage+'"/>';
		html += '<img src="Array" alt="" style="height: 267px"/>';
		html += '</figure>';
		html += '<div class="content">';
		html += '<p class="title">'+data.title+'</p>';
		html += '<div class="info">';
		html += '<p class="price-container">';
		html += '<strong class="price">#</strong></p></div></div>';
		html += '<a href="'+data.urlMore+'" class="fxlink">View detail</a></div>';
		html += '</div>'*/
		var html = '<div class="col-xl-3 col-lg-4 col-6 bcol">';
		html += '<div class="inner imgeffect hoverpp">';
		html += '<figure class="imgwrap" style="height: 176.083px;">';
		html += '<div class="bg" style="background-image: url(' + data.urlImage
				+ ');">';
		html += '<img src="'+ data.urlImage +'" style="height: 200px;"/>';
		html += '</div>'
		html += '</figure>';
		html += '<div class="box-content" style="height: 150px;">'
		html += '<div class="box-status">';
		
		if(data.popular == "POPULAR"){
			html += '<span class="popular">' + data.popular + '</span>'			
		}
		
		if(data.funded == "FUNDED"){			
			html += '<span class="funded">' + data.funded + '</span>'
		}
		
		html += '</div>'
		html += '<p class="title mb-1" style="height: 61.3333px;">'
				+ data.title + '</p>';
		html += '<div class="float-right small-text">' + data.status;
		
		if(data.price != "0"){
			html += '<strong class="price">$ ' + data.price + '</strong>';		
		}
		
		html += '</div>';
		html += '</div>';
		var url = ''+data.urlMore;
        html += '<a href="' + url.split('?')[0] + '" class="fxlink">View detail</a>';
		html += '</div>';
		html += '</div>';
		return html;
	}
	
	function generateOthersResult(name, data) {
		var html = '<div class="grid-7">';
		html += '<div class="bcol title">';
		html += '<h3>'+name+'</h3></div>';
		html += '<div class="bcol content">';
		html += '<h4>'+data.title+'</h4>';
		html += '<p>'+data.desc+'</p></div>';
		html += '<div class="bcol last">';
		html += '<a href="'+data.urlMore+'" class="btn-2">Read More</a>';
		html += '</div></div>';
		return html;
	}
	
	 function getFilteredCourses(filters, sort) {
	        
	        var coursesFilter = courses;
	        var htmlCourses = '';
		    var minPriceFilter = '';
		    var maxPriceFilter = '';
		    var fundedFilter = [];
		    var priceFilter = [];	
		    var topicIdsFilter = [];
		    var themeIdsFilter = [];
		    var filterList = {};
		    
		    if(filters != false){
		    	
		        for(var i = 1; i < filters.length; i++){
		        	//
		        	var temp = filters[i].split(".");
					var keyTemp = temp[0];
		            var valueTemp = temp[1];
		            if(keyTemp == "topicIds"){
		              	topicIdsFilter.push(parseInt(valueTemp));
		            }else if(keyTemp == "themeIds"){
		               	themeIdsFilter.push(parseInt(valueTemp));
		            }else if(keyTemp == "funded"){
		               	fundedFilter.push(valueTemp);
		            }else if(keyTemp == "minPrice"){
		               	minPriceFilter = valueTemp;
		               	priceFilter.push(minPriceFilter);
		 				
		            }else if(keyTemp == "maxPrice"){
		               	maxPriceFilter = valueTemp;
		               	//priceFilter.push(maxPriceFilter);
		               	
		            } 
		       	}
		            
		        filterList['topicIds'] = topicIdsFilter;
		        filterList['themeIds'] = themeIdsFilter;
		        filterList['funded'] = fundedFilter;
		        filterList['price'] = priceFilter;
				
		        
		        
		        coursesFilter = coursesFilter.filter(function(course) {
		     		var allCondition = true;
		     		var segmentCondition = false;
		     		Object.keys(filterList).forEach((key,idx) => {

		     			segmentCondition = false;	
		     			if (filterList[key].length > 0){
		     				filterList[key].forEach((arr,idx) => {
		 	     	           
		 	           		   if (Array.isArray(course[key])) {
		 	                        segmentCondition = segmentCondition || course[key].includes(arr);
		 	                   } 
		 	     	           else if(key == 'price'){
		                 		   var coursePrice = course[key];
		                 		   let temp = minPriceFilter <= coursePrice && coursePrice <= maxPriceFilter;
		                 		   segmentCondition = segmentCondition || Boolean(temp); 
		                 	   }
		 	     	           else{
		 	                       segmentCondition = segmentCondition || course[key] == arr;                		   
		 	                   }
		 	        		});	
		     				allCondition = allCondition && segmentCondition;
		     			}
		                
		           	});
		        		return allCondition;
		 		});

		    } else {
		    	courseFilter = courses;
		    }

	        if (coursesFilter.length > 0) {
	            if (sort != false && sort == "asc") {
	                coursesFilter.sort(sortAsc);
	            } else {
	                coursesFilter.sort(sortDesc);
	            }
	            coursesFilter.forEach(function(course) {
	                htmlCourses += generateCourseResult(course);
	            });
	        }

	        $('#courses-result').html(htmlCourses);
	        //

	    }
	 
	 function generateFilterList(data) {
	        var valueTemp = data.category + '.' + data.filterId;
	        
	        var html = '<div class="col-lg-4 col-md-4 col-6 bcol">';
	        html += '<div class="checkbox checktype">';
	        html += '<input type="checkbox" name="' + data.filterName + '" onclick="filterCourses(this)" id="' + data.filterName + '" value="' + valueTemp + '" /> <label for="' + data.filterName + '">' +
	            data.filterName + '</label>';
	        html += '</div>'
	        html += '</div>'
	        html += '</div>'
	        return html;
	    }

	    function generateOtherList(data) {
	        var valueTemp = data.category + '.' + data.filterName.toUpperCase();
	        var html = '<div class="col-lg-4 col-md-4 col-6 bcol">';
	        html += '<div class="checkbox checktype">';
	        html += '<input type="checkbox" name="' + data.filterName + '" onclick="filterCourses(this)" id="' + data.filterName + '" value="' + valueTemp + '" /> <label for="' + data.filterName + '">' +
	            data.filterName + '</label>';
	        html += '</div>'
	        html += '</div>'
	        html += '</div>'
	        return html;
	    }

	    function filterCourses() {
	        var sortIdx = document.getElementById("price").selectedIndex;
	        var sortId = document.getElementById("price").options[sortIdx].value;
	        //		var priceSortValue = priceSort.selectElement.options[priceSort.selectedIndex].value;
	        //
	        var data = [];
	        $(':checkbox:checked').each(function(i) {
	            //
				data[i] = $(this).val();   	 		
	        });
	        
	        if(minSliderPrice != ''){
	        	//var min = minSliderPrice.replace('$', '');
	        	var min = Number(minSliderPrice.replace(/[^0-9-\.]+/g,""));
	        	
	        	data.push('minPrice.' + min);
	        }
	        
	        if(maxSliderPrice != ''){
	        	//var max = maxSliderPrice.replace('$', '');
	        	var max = Number(maxSliderPrice.replace(/[^0-9-\.]+/g,""));
	        	
	        	data.push('maxPrice.' + max);
	        }
	        
	        if (data.length == 1) {
	            getFilteredCourses(false, sortId);
	        } else {
	            getFilteredCourses(data, sortId);
	        }

	    }

	    function resetFilter() {
	        $('input:checkbox').removeAttr('checked');
	        var sortIdx = document.getElementById("price").selectedIndex;
	        var sortId = document.getElementById("price").options[sortIdx].value;
	        loadResult("<%=searchResourceURL.toString()%>");
	    }

	    function sortAsc(a, b) {
	        return a.price - b.price;
	    }

	    function sortDesc(a, b) {
	        return b.price - a.price;
	    }
</script>


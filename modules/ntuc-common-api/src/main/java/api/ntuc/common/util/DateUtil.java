package api.ntuc.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	private static SimpleDateFormat MMddyyyy = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat ddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat dateTime = new SimpleDateFormat("dd MMM yyyy HH:mm");

	public static Date parse(String date, SimpleDateFormat sdf) {
		try {
			return sdf.parse(date);
		} catch (ParseException e1) {
			return null;
		}
	}

	public static String toString(Date date) {
		return toString(date, dateTime);
	}

	public static String toString(Date date, SimpleDateFormat sdf) {
		return sdf.format(date);
	}

	/**
	 * get today date
	 * 
	 * @return calendar
	 */
	public static Calendar getTodayDate() {
		return setDate(new Date());
	}

	/**
	 * 
	 * calc diff day by 2 date
	 * 
	 * @param startDate
	 * @param endDate
	 * @return number of day
	 */
	public static Number getDateDiff(Date startDate, Date endDate) {
		Calendar start = setDate(startDate);
		Calendar end = setEndDate(endDate);
		long diff = end.getTimeInMillis() - start.getTimeInMillis();
		return diff / (24 * 60 * 60 * 1000);
	}

	/**
	 * set date time to all zero
	 * 
	 * @param date
	 * @return
	 */
	public static Calendar setDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	/**
	 * set date time to end date
	 * 
	 * @param date
	 * @return
	 */
	public static Calendar setEndDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar;
	}

	public static SimpleDateFormat getByLocale(Locale locale) {
		if (locale.toString().equalsIgnoreCase("in_ID")) {
			return ddMMyyyy;
		} else {
			return MMddyyyy;
		}
	}

}

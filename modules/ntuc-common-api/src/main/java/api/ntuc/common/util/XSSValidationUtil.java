package api.ntuc.common.util;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class XSSValidationUtil {

	private static Whitelist whitelistRelaxed = Whitelist.relaxed().addAttributes(":all", "class", "style", "align")
			.addAttributes("td", "valign");

	public static boolean isValid(String input) {
		if (input == null || input.isEmpty()) {
			return true;
		}
		return Jsoup.isValid(input, Whitelist.basic());
	}

	public static boolean isValidRelaxed(String input) {
		if (input == null || input.isEmpty()) {
			return true;
		}
		return Jsoup.isValid(input, whitelistRelaxed);
	}

	public static String cleanRelaxed(String input) {
		if (input == null || input.isEmpty()) {
			return input;
		}
		return Jsoup.clean(input, whitelistRelaxed);
	}

}

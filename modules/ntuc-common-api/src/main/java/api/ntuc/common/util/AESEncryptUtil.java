package api.ntuc.common.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * The Class AESEncryptUtil.
 */
public class AESEncryptUtil {
	
	private static Log log = LogFactoryUtil.getLog(AESEncryptUtil.class);

	private static Cipher ecipher;
	private static Cipher dcipher;
	
	private static final String ALGO = "AES";
	
	//key should 16 bytes or 16 characters
	private static final byte[] defaultKeyValue =new byte[]{'@','[','W','E','B','A','R','Q','!','1','2','3','4','5',']','#'};
	
	private static SecretKeySpec defaultKey = new SecretKeySpec(defaultKeyValue, ALGO);
	
	private AESEncryptUtil() {
		 throw new IllegalStateException("Utility class");
	}
	
    /**
     * Encrypt.
     *
     * @param plainText the plain text
     * @return the string
     * @throws Exception the unabled process exception
     */
	public static String encrypt(String str, String key) throws Exception {

		try {
			ecipher = Cipher.getInstance(ALGO);
			
			SecretKeySpec sctKey = Validator.isNotNull(key)?new SecretKeySpec(key.getBytes(), ALGO):defaultKey;
			ecipher.init(Cipher.ENCRYPT_MODE, sctKey);

			byte[] utf8 = str.getBytes("UTF8");

			byte[] enc = ecipher.doFinal(utf8);

			return Base64.getEncoder().encodeToString(enc);
		} catch (Exception e) {
			log.error("Encryption error: "+e.getMessage(),e);
			
			throw new Exception("[Encryption failed] : Unable to complete encryption due to interruption");
		}

	}

    /**
     * Decrypt.
     *
     * @param encrypted the encrypted
     * @return the string
     * @throws Exception the unabled process exception
     */
	public static String decrypt(String str, String key) throws Exception {

		try {
			dcipher = Cipher.getInstance(ALGO);
			
			SecretKeySpec sctKey = Validator.isNotNull(key)?new SecretKeySpec(key.getBytes(), ALGO):defaultKey;
			dcipher.init(Cipher.DECRYPT_MODE, sctKey);

			byte[] dec = Base64.getDecoder().decode(str);

			byte[] utf8 = dcipher.doFinal(dec);

			return new String(utf8, "UTF8");
		} catch (Exception e) {
			log.error("Decryption error: "+e.getMessage(),e);
			
			throw new Exception("[Decryption failed] : Unable to complete decryption due to interruption");

		}

	}
    
}

package api.ntuc.common.util;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.ImageResolutionException;
import com.liferay.portal.kernel.image.ImageBag;
import com.liferay.portal.kernel.image.ImageToolUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.Validator;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

public class CompressImageUtil {

	private static Log log = LogFactoryUtil.getLog(CompressImageUtil.class);
	private static final int maxHeight = 1920;
	private static final int maxWidth = 1920;
	private static final int maxSize = 1000000;
	private static final String TMP_DIR = System.getProperty("java.io.tmpdir") + File.separatorChar + "CP";

	public static boolean convertImage(String inputImagePath, String outputImagePath, String formatName) {

		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		ImageOutputStream ios = null;

		try {
			inputStream = new FileInputStream(inputImagePath);
			outputStream = new FileOutputStream(outputImagePath);

			// reads input image from file
			BufferedImage inputImage = ImageIO.read(inputStream);

			Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(formatName);
			ImageWriter writer = (ImageWriter) writers.next();

			ios = ImageIO.createImageOutputStream(outputStream);
			writer.setOutput(ios);

			ImageWriteParam param = writer.getDefaultWriteParam();
			param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			if (Validator.isNotNull(formatName) && "tiff".equalsIgnoreCase(formatName)) {
				param.setCompressionType("LZW");
				param.setCompressionQuality(0.05f);
			} else {
				// for jpeg image
				param.setCompressionQuality(0.7f);
			}
			writer.write(null, new IIOImage(inputImage, null, null), param);
			writer.dispose();

			return true;
//			if(ios.length() <= maxSize) {
//				return true;
//			}else {
//				return false;
//			}

			// writes to the output image in specified format
//			boolean result = ImageIO.write(inputImage, formatName, outputStream);
//			return result;
		} catch (Exception e) {
			log.error(e);
			return false;
		} finally {
			try {
				if (inputStream != null)
					inputStream.close();
				if (outputStream != null)
					outputStream.close();
//				if (ios != null) ios.close();
			} catch (Exception e) {
				log.error(e);
			}
		}
	}

	//masih dalam pengecekan, error compile gradle di 7.3
//	public static boolean convertPdf(String inputPdfPath, String outputPdfPath, String formatName) {
//
//		FileOutputStream outputStream = null;
//		ImageOutputStream output = null;
//		PDDocument document = null;
//		
//		try {
//			outputStream = new FileOutputStream(outputPdfPath);
//			output = ImageIO.createImageOutputStream(outputStream);
//			
//			ImageWriter writer = ImageIO.getImageWritersByFormatName(formatName).next();
//			writer.setOutput(output);
//			
//			document = PDDocument.load(new File(inputPdfPath));
//			PDFRenderer pdfRenderer = new PDFRenderer(document);
//			int pageCount = document.getNumberOfPages();
//			BufferedImage[] images = new BufferedImage[pageCount];
//			
//			ImageWriteParam params = writer.getDefaultWriteParam();
//			params.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//			params.setCompressionType("Deflate");
////			params.setCompressionType("LZW");
////			params.setCompressionQuality(0.05f);
//			
//			writer.prepareWriteSequence(null);
//			
//			for (int page = 0; page < pageCount; page++) {
////				BufferedImage image = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
//				BufferedImage image = pdfRenderer.renderImage(page);
//				images[page] = image;
//				IIOMetadata metadata = writer.getDefaultImageMetadata(new ImageTypeSpecifier(image), params);
//				writer.writeToSequence(new IIOImage(image, null, metadata), params);
//			}
//			writer.endWriteSequence();
//			writer.dispose();
//			
//			return true;
//		} catch (Exception e) {
//			log.error(e);
//			return false;
//		} finally {
//			try {
//				if (outputStream != null) outputStream.close();
//				if (document != null) document.close();
////				if (output != null) output.close();
//			} catch (Exception e) {
//				log.error(e);
//			}
//		}
//
//		//return false;
//	}

	public static boolean convertAndResizeImage(String inputImagePath, String outputImagePath, String formatName) {

		try {
			log.debug("convertAndResizeImage - inputImagePath: " + inputImagePath + ", outputImagePath: "
					+ outputImagePath);

			File file = new File(inputImagePath);

			ImageBag imageBag = ImageToolUtil.read(file);

			RenderedImage renderedImage = imageBag.getRenderedImage();

			if (renderedImage.getHeight() > maxHeight || renderedImage.getWidth() > maxWidth) {
				log.debug("convertAndResizeImage - resize");

				RenderedImage resize = ImageToolUtil.scale(renderedImage, maxHeight, maxWidth);

				ImageIO.write(resize, imageBag.getType(), file);
			}
			return convertImage(inputImagePath, outputImagePath, formatName);
		} catch (ImageResolutionException e) {
			log.error("convertAndResizeImage - ImageResolutionException: " + e.getMessage(), e);
			return false;
		} catch (Exception e) {
			log.error("convertAndResizeImage - error: " + e.getMessage(), e);
			return false;
		}
	}

	public static File compressImage(File file, String fileType) {

		String ext = GetterUtil.getString(FileUtil.getExtension(file.getName())).toLowerCase();
		log.info("extension :" + ext);

		String filename = fileType + StringPool.PERIOD + ext;
		File folder = new File(TMP_DIR);
		if (!folder.exists())
			folder.mkdirs();
		String tempFileName = TMP_DIR + File.separator + filename;
		File document = new File(tempFileName);

		try {
			log.debug("convertAndResizeImage - inputImagePath: " + file.getPath());

			ImageBag imageBag = ImageToolUtil.read(file);

			RenderedImage renderedImage = imageBag.getRenderedImage();

			if (renderedImage.getHeight() > maxHeight || renderedImage.getWidth() > maxWidth) {
				log.debug("convertAndResizeImage - resize");

				RenderedImage resize = ImageToolUtil.scale(renderedImage, maxHeight, maxWidth);

				ImageIO.write(resize, imageBag.getType(), file);
			}
			convertImage(file.getPath(), document.getPath(), ext);
		} catch (ImageResolutionException e) {
			log.error("convertAndResizeImage - ImageResolutionException: " + e.getMessage(), e);
		} catch (Exception e) {
			log.error("convertAndResizeImage - error: " + e.getMessage(), e);
		}

		return document;
	}

	public static void deleteTempFile(File[] file) {
		try {
			for (File temp : file) {
				if (temp.delete()) {
					log.info("Deleted the file: " + temp.getName());
				} else {
					System.out.println("Failed to delete the file.");
				}
			}
		} catch (Exception e) {
			log.error("Error delete file : " + e.getLocalizedMessage());
		}

	}

}

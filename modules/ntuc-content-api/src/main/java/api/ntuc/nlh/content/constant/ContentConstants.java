package api.ntuc.nlh.content.constant;

public class ContentConstants {

	public static final String JOURNAL_PRIMARYKEY = "contentRef";
	public static final String JOURNAL_UUID = "uuid";
	public static final String BACK_URL_VAR = "backUrl";

	public static final int START_INDEX = 0;
	public static final int END_INDEX = 6;

	public static final String PRODUCT_STRUCTURE = "Product";
	public static final String PRODUCT_TNC_STRUCTURE = "Syarat dan Ketentuan";
	public static final String PRODUCT_ADVANTAGE_STRUCTURE = "Keunggulan utama";
	public static final String NEWS_STRUCTURE = "Card News";
	public static final String PROMO_STRUCTURE = "Section Promo";
	public static final String TATA_CARA = "Tata Cara"; 
	public static final String BO_KEUNGGULAN_PLAN_STRUCTURE= "Keunggulan Plan (Beli Online)";
	public static final String BO_CAKUPAN_PENYAKIT_STRUCTURE= "Cakupan Penyakit (Beli Online)";
	public static final String BO_RISIKO_STRUCTURE= "Risiko (Beli Online)";
	public static final String BO_MANFAAT_PERAWATAN_STRUCTURE= "Manfaat Perawatan RS (Beli Online)";
	public static final String BO_SNK_STRUCTURE= "Syarat dan ketentuan Plan (Beli Online)";
	public static final String BO_BANNER_STRUCTURE= "Banner Static Beli Online";
	public static final String BO_OTHERS_STRUCTURE= "Other Plan Detail ( Beli Online )";
	
}

package web.ntuc.nlh.leadershipexcellence.constants;

/**
 * @author muhamadpangestu
 */
public class NtucLeadershipExcellenceWebPortletKeys {

	public static final String NTUCLEADERSHIPEXCELLENCEWEB =
		"web_ntuc_nlh_leadershipexcellence_NtucLeadershipExcellenceWebPortlet";

}
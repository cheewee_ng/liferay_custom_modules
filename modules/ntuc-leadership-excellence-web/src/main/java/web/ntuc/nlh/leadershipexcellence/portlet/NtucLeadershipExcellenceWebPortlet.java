package web.ntuc.nlh.leadershipexcellence.portlet;

import web.ntuc.nlh.leadershipexcellence.constants.NtucLeadershipExcellenceWebPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author muhamadpangestu
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=NtucLeadershipExcellenceWeb",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + NtucLeadershipExcellenceWebPortletKeys.NTUCLEADERSHIPEXCELLENCEWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class NtucLeadershipExcellenceWebPortlet extends MVCPortlet {
}
<%@ include file="/init.jsp"%>

<div class="row filter-wrap-2">
	<div class="col-md-auto">
		<div class="show-control">
			<a href="#" class="control btn-4"><i class="fas fa-filter"></i>
				Filter</a>
			<div class="sub-content">
				<div class="row">
					<div class="col-6">Advance Filters</div>
					<div class="col-6 text-right">
						<a class="clear-btn" data-form-id="filter"
							href="javascript:void(0)">Clear All</a>
					</div>
				</div>

				<form id="filter">

					<div class="lb-1 mb-2">Topic</div>
					<div class="row sp-row-1 break-425">
						<div class="col-lg-4 col-md-4 col-6 bcol">
							<div class="checkbox checktype">
								<input type="checkbox" name="topics[]" value="john-maxwell"
									id="topic-john-maxwell" /> <label for="topic-john-maxwell">John
									Maxwell</label>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-6 bcol">
							<div class="checkbox checktype">
								<input type="checkbox" name="topics[]"
									value="john-maxwell-signature-series"
									id="topic-john-maxwell-signature-series" /> <label
									for="topic-john-maxwell-signature-series">John Maxwell
									Signature Series</label>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-6 bcol"></div>
						<div class="col-lg-4 col-md-4 col-6 bcol"></div>
					</div>
					<hr />
					<div class="lb-1 mb-2">Price</div>
					<div class="row sp-row-1 break-425">
						<div class="col-lg-4 col-md-4 col-6 bcol">
							<div class="checkbox checktype">
								<input type="checkbox" name="prices[]" value="100-500"
									id="price-100-500" /> <label for="price-100-500">$100 -
									$ 500</label>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-6 bcol">
							<div class="checkbox checktype">
								<input type="checkbox" name="prices[]" value="500-1000-2"
									id="price-500-1000-2" /> <label for="price-500-1000-2">$500
									- $1000</label>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-6 bcol"></div>
						<div class="col-lg-4 col-md-4 col-6 bcol"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md last">
		<!--<div class="checkbox checkfund">
                    <input type="checkbox" id="funded" value="1" />
                    <label for="funded">Funded</label>
                </div>-->
		<!--<div class="select-wrap">
                    <select id="rating" class="selectpicker">
                        <option value="desc">Rating (High to Low)</option>
                        <option value="asc">Rating (Low to High)</option>
                    </select>
                </div>-->
		<div class="select-wrap">
			<select id="price" class="selectpicker">
				<option value="asc">Price (Low to High)</option>
				<option value="desc">Price (High to Low)</option>
			</select>
		</div>
	</div>
</div>
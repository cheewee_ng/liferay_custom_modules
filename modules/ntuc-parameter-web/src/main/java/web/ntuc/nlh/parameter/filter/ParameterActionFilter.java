package web.ntuc.nlh.parameter.filter;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;

import org.osgi.service.component.annotations.Component;

import api.ntuc.common.override.OverrideActionRequestParam;
import api.ntuc.common.util.CSRFValidationUtil;
import api.ntuc.common.util.PermissionUtil;
import api.ntuc.common.util.XSSValidationUtil;
import web.ntuc.nlh.parameter.constants.MVCCommandNames;
import web.ntuc.nlh.parameter.constants.ParameterPortletKeys;
import web.ntuc.nlh.parameter.constants.PermissionConstant;

@Component(immediate = true, property = {
		"javax.portlet.name=" + ParameterPortletKeys.PARAMETER_PORTLET, }, service = PortletFilter.class)
public class ParameterActionFilter implements ActionFilter {

	private Log log = LogFactoryUtil.getLog(ParameterActionFilter.class.getName());

	@Override
	public void init(FilterConfig filterConfig) throws PortletException {

	}

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain)
			throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Map<String, Object> newParam = new HashMap<String, Object>();

		/*
		 * Check Permission for access page
		 */
		boolean isAuthorized = false;
		String actionName = ParamUtil.getString(request, "javax.portlet.action");
		PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();
		try {
			boolean isAdministrator = request.isUserInRole(PermissionConstant.ROLE_ADMINISTRATOR);
			switch (actionName) {
			case MVCCommandNames.UPDATE_GROUP_ACTION:
				boolean addGroup = PermissionUtil.contains(permissionChecker, themeDisplay.getSiteGroupId(),
						PermissionConstant.ROLE_ADD_GROUP, PermissionConstant.PERMISSION_RESOURCES_NAME);
				boolean editGroup = PermissionUtil.contains(permissionChecker, themeDisplay.getSiteGroupId(),
						PermissionConstant.ROLE_EDIT_GROUP, PermissionConstant.PERMISSION_RESOURCES_NAME);
				isAuthorized = isAdministrator || addGroup || editGroup;
				break;
			case MVCCommandNames.DELETE_GROUP_ACTION:
				boolean deleteGroup = PermissionUtil.contains(permissionChecker, themeDisplay.getSiteGroupId(),
						PermissionConstant.ROLE_DELETE_GROUP, PermissionConstant.PERMISSION_RESOURCES_NAME);
				isAuthorized = isAdministrator || deleteGroup;
				break;
			case MVCCommandNames.UPDATE_PARAMETER_ACTION:
				boolean addParameter = PermissionUtil.contains(permissionChecker, themeDisplay.getSiteGroupId(),
						PermissionConstant.ROLE_ADD_PARAMETER, PermissionConstant.PERMISSION_RESOURCES_NAME);
				boolean editParameter = PermissionUtil.contains(permissionChecker, themeDisplay.getSiteGroupId(),
						PermissionConstant.ROLE_EDIT_PARAMETER, PermissionConstant.PERMISSION_RESOURCES_NAME);
				isAuthorized = isAdministrator || addParameter || editParameter;
				break;
			case MVCCommandNames.DELETE_PARAMETER_ACTION:
				boolean deleteParameter = PermissionUtil.contains(permissionChecker, themeDisplay.getSiteGroupId(),
						PermissionConstant.ROLE_DELETE_PARAMETER, PermissionConstant.PERMISSION_RESOURCES_NAME);
				isAuthorized = isAdministrator || deleteParameter;
				break;
			default:
				break;
			}
		} catch (Exception e) {
			log.error("error while setting button access role : " + e.getMessage());
		}
		newParam.put("isAuthorized", isAuthorized);

		/*
		 * CSRF Validation
		 */
		boolean validCSRF = false;
		String authToken = ParamUtil.getString(request, "authToken");
		if (CSRFValidationUtil.isValidRequest(request, authToken)) {
			validCSRF = true;
		}
		newParam.put("validCSRF", validCSRF);

		/*
		 * XSS Validation
		 */
		boolean xssPass = true;
		Set<String> names = request.getActionParameters().getNames();
		for (String name : names) {
			String param = request.getActionParameters().getValue(name);
			if (!XSSValidationUtil.isValid(param)) {
				xssPass = false;
				break;
			}
		}
		newParam.put("xssPass", xssPass);

		OverrideActionRequestParam over = new OverrideActionRequestParam(request, newParam);
		chain.doFilter(over, response);
	}
}

package web.ntuc.nlh.parameter.constants;

public class MVCCommandNames {

	public static final String UPDATE_GROUP_RENDER = "groupUpdateRender";
	public static final String UPDATE_GROUP_ACTION = "groupUpdateAction";
	public static final String GROUP_DATA_RESOURCES = "groupDataResources";
	public static final String DELETE_GROUP_ACTION = "groupDeleteAction";
	public static final String VIEW_GROUP_RENDER = "groupViewRender";
	
	public static final String UPDATE_PARAMETER_RENDER = "parameterUpdateRender";
	public static final String UPDATE_PARAMETER_ACTION = "parameterUpdateAction";
	public static final String PARAMETER_DATA_RESOURCES = "parameterDataResources";
	public static final String DELETE_PARAMETER_ACTION = "parameterDeleteAction";
	public static final String VIEW_PARAMETER_RENDER = "parameterViewRender";

}

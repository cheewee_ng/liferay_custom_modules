package web.ntuc.nlh.parameter.filter;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;
import javax.portlet.filter.RenderFilter;

import org.osgi.service.component.annotations.Component;

import api.ntuc.common.override.OverrideRenderRequestParam;
import api.ntuc.common.util.CSRFValidationUtil;
import api.ntuc.common.util.PermissionUtil;
import api.ntuc.common.util.XSSValidationUtil;
import web.ntuc.nlh.parameter.constants.MVCCommandNames;
import web.ntuc.nlh.parameter.constants.ParameterPortletKeys;
import web.ntuc.nlh.parameter.constants.PermissionConstant;

@Component(immediate = true, property = {
		"javax.portlet.name=" + ParameterPortletKeys.PARAMETER_PORTLET, }, service = PortletFilter.class)
public class ParameterRenderFilter implements RenderFilter {

	private Log log = LogFactoryUtil.getLog(ParameterRenderFilter.class.getName());

	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain)
			throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Map<String, Object> newParam = new HashMap<String, Object>();

		/*
		 * Check Permission for access page
		 */
		boolean isAuthorized = false;
		PermissionChecker permissionChecker = themeDisplay.getPermissionChecker();
		String renderName = ParamUtil.getString(request, "mvcRenderCommandName");
		try {
			boolean isAdmin = permissionChecker.isGroupAdmin(themeDisplay.getScopeGroupId());
			switch (renderName) {
			case MVCCommandNames.UPDATE_GROUP_RENDER:
				boolean isAdd = PermissionUtil.contains(permissionChecker, themeDisplay.getSiteGroupId(),
						PermissionConstant.ROLE_ADD_GROUP, PermissionConstant.PERMISSION_RESOURCES_NAME);
				boolean isEdit = PermissionUtil.contains(permissionChecker, themeDisplay.getScopeGroupId(),
						PermissionConstant.ROLE_EDIT_GROUP, PermissionConstant.PERMISSION_RESOURCES_NAME);
				isAuthorized = isAdmin || isEdit || isAdd;
				break;
			case MVCCommandNames.UPDATE_PARAMETER_RENDER:
				boolean isAddParameter = PermissionUtil.contains(permissionChecker, themeDisplay.getSiteGroupId(),
						PermissionConstant.ROLE_ADD_PARAMETER, PermissionConstant.PERMISSION_RESOURCES_NAME);
				boolean isEditParameter = PermissionUtil.contains(permissionChecker, themeDisplay.getScopeGroupId(),
						PermissionConstant.ROLE_EDIT_PARAMETER, PermissionConstant.PERMISSION_RESOURCES_NAME);
				isAuthorized = isAdmin || isEditParameter || isAddParameter;
				break;
			default:
				// open module paramter for the 1st time no need to validate
				isAuthorized = true;
				break;
			}
		} catch (Exception e) {
			log.error("error while setting button access role," + e.getMessage(), e);
		}

		newParam.put("isAuthorized", isAuthorized);

		/*
		 * CSRF Validation
		 */
		boolean validCSRF = false;
		if (Validator.isNull(renderName)) {
			// open module paramter for the 1st time no need to validate
			validCSRF = true;
		} else {
			String authToken = ParamUtil.getString(request, "authToken");
			if (CSRFValidationUtil.isValidRequest(request, authToken)) {
				validCSRF = true;
			}
		}
		newParam.put("validCSRF", validCSRF);

		/*
		 * XSS Validation
		 */
		boolean xssPass = true;
		Set<String> names = request.getRenderParameters().getNames();
		for (String name : names) {
			String param = request.getRenderParameters().getValue(name);
			if (!XSSValidationUtil.isValid(param)) {
				xssPass = false;
				break;
			}
		}
		newParam.put("xssPass", xssPass);

		OverrideRenderRequestParam over = new OverrideRenderRequestParam(request, newParam);
		chain.doFilter(over, response);
	}

}

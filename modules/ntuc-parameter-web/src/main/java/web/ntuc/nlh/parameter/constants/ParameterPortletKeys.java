package web.ntuc.nlh.parameter.constants;

/**
 * @author fazarnugroho
 */
public class ParameterPortletKeys {

	public static final String PARAMETER_PORTLET = "Parameter_Portlet";
	public static final String PARAMETER_DISPLAY = "Parameter Management";

}
package web.ntuc.nlh.eventfilter.engine;

import com.liferay.portal.kernel.util.GetterUtil;

import javax.portlet.PortletPreferences;

public class SearchPortletSearchResultPreferences implements SearchResultPreferences {

	private Boolean _displayResultsInDocumentForm;
	private final DocumentFormPermissionChecker _documentFormPermissionChecker;
	private Boolean _highlightEnabled;
	private final PortletPreferences _portletPreferences;
	private Boolean _viewInContext;

	public SearchPortletSearchResultPreferences(PortletPreferences portletPreferences,
			ThemeDisplaySupplier themeDisplaySupplier) {
		this._portletPreferences = portletPreferences;

		this._documentFormPermissionChecker = new DocumentFormPermissionCheckerImpl(
				themeDisplaySupplier.getThemeDisplay());
	}

	public boolean isDisplayResultsInDocumentForm() {
		if (this._displayResultsInDocumentForm != null) {
			return this._displayResultsInDocumentForm.booleanValue();
		}
		if (this._documentFormPermissionChecker.hasPermission()) {
			this._displayResultsInDocumentForm = Boolean.valueOf(
					GetterUtil.getBoolean(this._portletPreferences.getValue("displayResultsInDocumentForm", null)));
		} else {
			this._displayResultsInDocumentForm = Boolean.valueOf(false);
		}
		return this._displayResultsInDocumentForm.booleanValue();
	}

	public boolean isHighlightEnabled() {
		if (this._highlightEnabled != null) {
			return this._highlightEnabled.booleanValue();
		}
		this._highlightEnabled = Boolean
				.valueOf(GetterUtil.getBoolean(this._portletPreferences.getValue("highlightEnabled", null), true));

		return this._highlightEnabled.booleanValue();
	}

	public boolean isViewInContext() {
		if (this._viewInContext != null) {
			return this._viewInContext.booleanValue();
		}
		this._viewInContext = Boolean
				.valueOf(GetterUtil.getBoolean(this._portletPreferences.getValue("viewInContext", null), true));

		return this._viewInContext.booleanValue();
	}
}

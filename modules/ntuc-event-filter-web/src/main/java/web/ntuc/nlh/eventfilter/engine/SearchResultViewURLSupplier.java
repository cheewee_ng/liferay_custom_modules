package web.ntuc.nlh.eventfilter.engine;

public abstract interface SearchResultViewURLSupplier {

	public abstract String getSearchResultViewURL();
	
}

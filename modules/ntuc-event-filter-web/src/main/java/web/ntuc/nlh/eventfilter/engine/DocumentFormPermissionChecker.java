package web.ntuc.nlh.eventfilter.engine;

public abstract interface DocumentFormPermissionChecker {

	public abstract boolean hasPermission();
}

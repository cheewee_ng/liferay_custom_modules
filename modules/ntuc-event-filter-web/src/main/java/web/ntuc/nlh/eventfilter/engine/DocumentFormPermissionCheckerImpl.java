package web.ntuc.nlh.eventfilter.engine;

import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.theme.ThemeDisplay;

public class DocumentFormPermissionCheckerImpl implements DocumentFormPermissionChecker {
	
	private final ThemeDisplay _themeDisplay;

	public DocumentFormPermissionCheckerImpl(ThemeDisplay themeDisplay) {
		this._themeDisplay = themeDisplay;
	}

	public boolean hasPermission() {
		PermissionChecker permissionChecker = this._themeDisplay.getPermissionChecker();
		if (permissionChecker.isCompanyAdmin()) {
			return true;
		}
		return false;
	}
}

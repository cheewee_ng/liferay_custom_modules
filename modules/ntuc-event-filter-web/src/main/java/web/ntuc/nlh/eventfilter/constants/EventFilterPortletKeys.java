package web.ntuc.nlh.eventfilter.constants;

/**
 * @author muhamadpangestu
 */
public class EventFilterPortletKeys {

	public static final String EVENTFILTER_PORTLET = "EventFilter_Portlet";
	public static final String ASSET_VOCAB_TOPICS = "topic for search purposes";
	public static final String ASSET_VOCAB_EVENT = "events";

}
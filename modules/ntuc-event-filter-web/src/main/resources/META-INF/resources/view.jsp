<%@page import="web.ntuc.nlh.eventfilter.constants.MVCCommandNames"%>
<%@ include file="init.jsp"%>

<portlet:resourceURL id="<%=MVCCommandNames.LIST_TOPICS_RESOURCE%>"
	var="dataResourceURL">
	<portlet:param name="authToken" value="${authToken}" />
	<portlet:param name="topicId" value="${topicId}" />
	<portlet:param name="eventTypeId" value="${eventTypeId}" />
</portlet:resourceURL>

<section class="cx-events-ntuc">
	<div class="container sp-main-bot sp-main-top">
		<div class="row title-wrap-2 break-1360">
			<div class="col-xl-4 order-xl-12 actions bcol" id="filter">
				<div class="cate">
					<select id="category_filter" name="select_category"
						onchange="filterTopics()" tabindex="-98">
						<option value="0">All Categories</option>
						<c:forEach items="${events}" var="e">
							<option value="${e.categoryId}">${e.name}</option>
						</c:forEach>

					</select>
				</div>
				<div class="calendar-view">
					<div class="checkbox">
						<input type="checkbox" id="calendarview" name="calendarview"
							value="1"> <label for="calendarview">Calendar
							view</label> <input type="hidden" name="past_event" value="1">
					</div>
					<div class="row sp-row-1 yearmonth">
						<div class="col-5 bcol">
							<select name="filter_year" id="search-blog-year"
								onchange="filterTopics()" tabindex="-98">
								<option value="0">Year</option>
								<c:forEach items="${years}" var="year">
									<option value="${year}">${year}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-7 bcol">
							<select name="filter_month" id="search-blog-month"
								onchange="filterTopics()" tabindex="-98">
								<option value="0">All Months</option>
								<option value="1">January</option>
								<option value="2">February</option>
								<option value="3">March</option>
								<option value="4">April</option>
								<option value="5">May</option>
								<option value="6">June</option>
								<option value="7">July</option>
								<option value="8">August</option>
								<option value="9">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
						</div>
					</div>
					<div class="date-wrap" style="display: none;">
						<input name="specifieddate" id="specifieddate"
							onblur="filterByDate()" type="text" value=""
							class="form-control nogetdate"> <i
							class="fas fa-calendar-alt"></i>
					</div>
				</div>
			</div>
			<div class="col-xl-8 sp-1360-1 bcol">
				<div class="sly-wrap nav-tabs-1 event-tabs">
					<div class="multisly" data-slide="1" style="overflow: hidden;">
						<ul class="nav sly-content tabs"
							style="transform: translateZ(0px); width: 344px;">
							<li class="item" id="all-event" style="width: 65px;"><a
								href="/events">All Event</a></li>
							<li class="item" id="upcoming-events" style="width: 132px;">
								<a href="/upcoming-events">Upcoming Events</a>
							</li>
							<li class="item" id="past-events" style="width: 87px;"><a
								href="/past-events">Past Events</a></li>
						</ul>
					</div>
					<div class="scrollbar" style="visibility: hidden;">
						<div class="handle"
							style="transform: translateZ(0px) translateX(0px); width: 830px;">
							<div class="mousearea"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="no-data">
			<p>No records found</p>
		</div>
		<div class="row sp-row-2 break-480 grid-1" id="topic-list">
			<div class="loader"></div>
			<!--  Generate Content by JS  -->
		</div>
	</div>
</section>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script type="text/javascript">
	
	var dataList = "";
	
	$(document).ready(function() {
		initData("<%=dataResourceURL.toString()%>");	
	});

	function initData(url) {
		return jQuery.ajax({
			url : url,
			type : "GET",
			dataType : "json",
			async : false,
			cache : false,
			beforeSend: function (data) { // Before we send the request, remove the .hidden class from the spinner and default to inline-block.
                $('.loader').removeClass('hidden');
				$('.no-data').hide();
            },
			success : function(data, status, XMLHttpRequest) {
				console.log(data.topicList);
				var htmlArticles = '';
				setActive(data.eventType);
				
				if(data.topicList.length > 0){
					data.topicList.sort(sortDate);
					data.topicList.forEach(function(topic){
						htmlArticles += generateTopicList(topic);
					});
				}
				
				$('#topic-list').html(htmlArticles);
				
				dataList = data.topicList;
			},
			complete: function () { // Set our complete callback, adding the .hidden class and hiding the spinner.
            	$('.loader').hide();
            },
			error: function(data, status, XMLHttpRequest) {
                //return null;
            }
		});
	}
	
	function filterTopics(){
		var htmlArticles = '';
		var topicFilter = dataList;
		
		var filterCategory = parseInt($('#category_filter option:selected').val());
		var filterYear = parseInt($('#search-blog-year option:selected').val());
		var filterMonth = parseInt($('#search-blog-month option:selected').val());
		
		topicFilter = topicFilter.filter(topic => {
			var date = new Date(topic.endDate);
			var topicYear = date.getFullYear();
			var topicMonth = date.getMonth() + 1;
			var categoryId = parseInt(topic.categoryId);
			
			if (filterYear != 0 && filterMonth != 0 && filterCategory != 0){
				return filterYear == topicYear && filterMonth == topicMonth && filterCategory == categoryId;
			}else if(filterCategory != 0 && filterYear == 0 && filterMonth == 0){
				return filterCategory == categoryId;
			}else if(filterCategory != 0 && filterYear != 0 && filterMonth == 0){
				return filterCategory == categoryId && filterYear == topicYear;
			}else if(filterYear == 0 && filterMonth == 0 && filterCategory == 0){
				return topicFilter = dataList;
			}
		});
		
		console.log(topicFilter);
		
		if (topicFilter.length > 0) {
			$('.no-data').hide();
			topicFilter.forEach(function(topic) {
				htmlArticles += generateTopicList(topic);
            });
        }else{
        	$('.no-data').show();
        }

        $('#topic-list').html(htmlArticles);
	}
	
	function filterByDate(){
		//alert("ok");
		var topicFilter = dataList;
		var htmlArticles = '';
		
		var filterDate = document.getElementById("specifieddate").value;
		console.log("filter date : " + filterDate);
		var filterCategory = parseInt($('#category_filter option:selected').val());
		
		topicFilter = topicFilter.filter(topic => {
			var topicDate = getFormattedDate(topic.endDate);
			console.log("topic date : " + topicDate);
			var categoryId = parseInt(topic.categoryId);
			
			if (filterDate != "" && filterCategory != 0){
				return filterDate == topicDate && filterCategory == categoryId;
			}else if(filterDate != "" && filterCategory == 0){
				return filterDate == topicDate;
			}else{
				return topicFilter = dataList;
			}
		});
		
		console.log(topicFilter);
		
		if (topicFilter.length > 0) {
			$('.no-data').hide();
			topicFilter.forEach(function(topic) {
				htmlArticles += generateTopicList(topic);
            });
        }else{
        	$('.no-data').show();
        }

        $('#topic-list').html(htmlArticles);
	}
	
	function generateTopicList(data){
		var tempDate = getFormattedDate(data.endDate);
		var html = '<div class="col-xl-3 col-lg-4 col-6 bcol">';
		html += '<div class="inner imgeffect">';
		html += '<figure class="imgwrap">';
		html += '<img src="' + data.urlImage + '" class="attachment-event-thumb size-event-thumb wp-post-image" alt="" loading="lazy">';
		html += '</figure>';
		html += '<div class="content" style="height: 148.889px;">';
		html += '<p class="title" style="height: 80px;">' + data.title + '</p>';
		html += '<div class="row sp-row-1 align-items-end">';
		html += '<div class="bcol col-8">';
		html += '<p class="date">' + tempDate + '</p>';
		html += '</div>';
		html += '<div class="bcol col-4 last">';
		html += '<p class="more">Read more</p>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		var url = ''+data.urlMore;
		html += '<a href="' + url.split('?')[0] + '" class="fxlink">View detail</a>';
		html += '</div>';
		html += '</div>';
		
		return html;
	}
	
	function getFormattedDate(convertDate){
		const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var d = new Date(convertDate);
		var month = months[d.getMonth()];
		var day = '' + d.getDate();
		if(day.length < 2){
			day = '0' + day;
		}
		var year = d.getFullYear();
		
		return day + " " + month + " " + year;
	}
	
	function sortDate(a, b){
		var dateA = new Date(a.endDate);
		var dateB = new Date(b.endDate);
		return dateB - dateA;
	}
	
	function setActive(typeId){
		if(typeId == 1){
			$('#filter').show();
			$('#all-event').addClass('active');
			$('#upcoming-events').removeClass('active');
			$('#past-events').removeClass('active');
		}else if(typeId == 2){
			$('#filter').hide();
			$('#all-event').removeClass('active');
			$('#upcoming-events').addClass('active');
			$('#past-events').removeClass('active');
		}else if(typeId == 3){
			$('#filter').show();
			$('#all-event').removeClass('active');
			$('#upcoming-events').removeClass('active');
			$('#past-events').addClass('active');
		}
	}
	
</script>
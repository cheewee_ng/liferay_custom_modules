package web.ntuc.nlh.socialenquiry.constants;

public class PermissionConstant {

	// resources name permission
	public static final String PERMISSION_RESOURCES_NAME = "parameter.permission.resources";

	// request attribute names
	public static final String ADD_GROUP = "addGroup";
	public static final String EDIT_GROUP = "editGroup";
	public static final String DELETE_GROUP = "deleteGroup";
	public static final String ADD_PARAMETER = "addParameter";
	public static final String EDIT_PARAMETER = "editParameter";
	public static final String DELETE_PARAMETER = "deleteParameter";
	public static final String IS_ADMINISTRATOR = "isAdmin";
	public static final String VIEW_PARAMETER = "viewParameter";

	// role names
	public static final String ROLE_ADD_PARAMETER = "ADD_PARAMETER";
	public static final String ROLE_EDIT_PARAMETER = "EDIT_PARAMETER";
	public static final String ROLE_DELETE_PARAMETER = "DELETE_PARAMETER";
	public static final String ROLE_ADD_GROUP = "ADD_GROUPS";
	public static final String ROLE_EDIT_GROUP = "EDIT_GROUPS";
	public static final String ROLE_DELETE_GROUP = "DELETE_GROUPS";
	public static final String ROLE_ADMINISTRATOR = "Administrator";
	public static final String ROLE_VIEW_PARAMETER = "VIEW_PARAMETER";

}

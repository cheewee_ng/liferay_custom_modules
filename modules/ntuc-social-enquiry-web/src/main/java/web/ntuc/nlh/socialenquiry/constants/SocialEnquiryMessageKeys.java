package web.ntuc.nlh.socialenquiry.constants;

public class SocialEnquiryMessageKeys {
	
	public static String XSS_VALIDATION_NOT_PASS = "xss-validation-not-pass";
	public static String PASS_NOT_MATCH = "password-not-match";
	
}

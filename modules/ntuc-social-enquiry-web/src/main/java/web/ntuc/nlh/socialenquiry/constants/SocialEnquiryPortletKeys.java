package web.ntuc.nlh.socialenquiry.constants;

/**
 * @author muhamadpangestu
 */
public class SocialEnquiryPortletKeys {

	public static final String SOCIALENQUIRY_PORTLET = "NTUC_Social_Enquiry_Portlet";
	public static final String DMS_PASSWORD = "social-enquiry";

}
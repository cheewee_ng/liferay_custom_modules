package web.ntuc.nlh.socialenquiry.resource;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.PrintWriter;
import java.util.List;

import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import api.ntuc.common.util.AESEncryptUtil;
import svc.ntuc.nlh.parameter.model.Parameter;
import svc.ntuc.nlh.parameter.model.ParameterGroup;
import svc.ntuc.nlh.parameter.service.ParameterGroupLocalService;
import svc.ntuc.nlh.parameter.service.ParameterLocalService;
import web.ntuc.nlh.socialenquiry.constants.MVCCommandNames;
import web.ntuc.nlh.socialenquiry.constants.SocialEnquiryMessageKeys;
import web.ntuc.nlh.socialenquiry.constants.SocialEnquiryPortletKeys;

@Component(immediate = true, property = { "mvc.command.name=" + MVCCommandNames.SOCIAL_ENQUIRY_RESOURCE,
		"javax.portlet.name=" + SocialEnquiryPortletKeys.SOCIALENQUIRY_PORTLET }, service = MVCResourceCommand.class)
public class SocialEnquiryResource implements MVCResourceCommand {

	private static Log log = LogFactoryUtil.getLog(SocialEnquiryResource.class);

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException {
		log.info("List Courses Resources - Start");

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long groupId = themeDisplay.getScopeGroupId();
			log.info("groupID = " + groupId);
//		Filter Implementation and prepare resources
			boolean isAuthorized = ParamUtil.getBoolean(resourceRequest, "isAuthorized");
			boolean validCSRF = ParamUtil.getBoolean(resourceRequest, "validCSRF");
			boolean xssPass = ParamUtil.getBoolean(resourceRequest, "xssPass");

			String userPass = ParamUtil.getString(resourceRequest, "password");

			log.info(userPass);
			
			ParameterGroup parentThemeGroup = _parameterGroupLocalService
					.getByCode(SocialEnquiryPortletKeys.DMS_PASSWORD, false);
			// log.info(parentThemeGroup.toString());
			List<Parameter> parameters = _parameterLocalService
					.getByParameterGroupId(parentThemeGroup.getParameterGroupId(), false);
			for (Parameter parameter : parameters) {
				// log.info(parameter.getParamValue());
//				encryptPass = AESEncryptUtil.encrypt(parameter.getParamValue(), null);
//				password = parameter.getParamValue();
			}

			if (!isAuthorized || !validCSRF) {
				String msg = "isAuthorized : " + isAuthorized + " | validCSRF : " + validCSRF;
				throw new Exception(msg);
			}

			if (!xssPass) {
				PortletConfig portletConfig = (PortletConfig) resourceRequest
						.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
				String msg = LanguageUtil.format(portletConfig.getResourceBundle(themeDisplay.getLocale()),
						SocialEnquiryMessageKeys.XSS_VALIDATION_NOT_PASS, xssPass);
				throw new Exception(msg);
			}

			HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
			
			resourceResponse.setContentType("application/json");
			PrintWriter out = null;
			out = resourceResponse.getWriter();

			JSONObject rowData = JSONFactoryUtil.createJSONObject();
			rowData.put("status", HttpServletResponse.SC_OK);
			out.print(rowData.toString());
			out.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while searching result data : " + e.getMessage());
			e.printStackTrace();
		}
		log.info("search result data resources - end");
		return false;
	}

	@Reference
	protected ParameterLocalService _parameterLocalService;

	@Reference
	protected ParameterGroupLocalService _parameterGroupLocalService;
}

package web.ntuc.nlh.socialenquiry.action;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import svc.ntuc.nlh.parameter.model.Parameter;
import svc.ntuc.nlh.parameter.model.ParameterGroup;
import svc.ntuc.nlh.parameter.service.ParameterGroupLocalService;
import svc.ntuc.nlh.parameter.service.ParameterLocalService;
import web.ntuc.nlh.socialenquiry.constants.MVCCommandNames;
import web.ntuc.nlh.socialenquiry.constants.SocialEnquiryMessageKeys;
import web.ntuc.nlh.socialenquiry.constants.SocialEnquiryPortletKeys;
import web.ntuc.nlh.socialenquiry.util.CryptoJsAESUtil;

@Component(immediate = true, property = { "mvc.command.name=" + MVCCommandNames.SUBMIT_FORM,
		"javax.portlet.name=" + SocialEnquiryPortletKeys.SOCIALENQUIRY_PORTLET,
		"com.liferay.portlet.action-url-redirect=true" }, service = MVCActionCommand.class)
public class SubmitFormAction extends BaseMVCActionCommand {

	Log log = LogFactoryUtil.getLog(SubmitFormAction.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		// TODO Auto-generated method stub
		log.info("Submit Form Action - Start");
		
		try {
			
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			boolean isAuthorized = ParamUtil.getBoolean(actionRequest, "isAuthorized");
			boolean validCSRF = ParamUtil.getBoolean(actionRequest, "validCSRF");
			boolean xssPass = ParamUtil.getBoolean(actionRequest, "xssPass");

			if (!isAuthorized || !validCSRF) {
				String msg = "isAuthorized : " + isAuthorized + " | validCSRF : " + validCSRF;
				SessionErrors.add(actionRequest, "you-dont-have-permission-or-your-session-is-end");
				throw new Exception(msg);
			}

			if (!xssPass) {	
				PortletConfig portletConfig = (PortletConfig) actionRequest
						.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
				String msg = LanguageUtil.format(portletConfig.getResourceBundle(themeDisplay.getLocale()),
						SocialEnquiryMessageKeys.XSS_VALIDATION_NOT_PASS, xssPass);
				SessionErrors.add(actionRequest, "your-input-not-pass-xss");
				throw new Exception(msg);
			}
			
			String authToken = ParamUtil.getString(actionRequest, "logsesTkn");
			authToken = new String(Base64.decode(authToken));
			
			String password = ParamUtil.getString(actionRequest, "password");
//			log.info("before : " + password);

			
			String decodePass = new String(Base64.decode(password));
			password = CryptoJsAESUtil.decryptData(decodePass, authToken);		
//			log.info("after : " + password);
			
			if (password.equals(getPassword())) {
				log.info("Validation Password Passed");
				actionResponse.getRenderParameters().setValue("mvcPath", "/form.jsp");
			} else {
				log.info("Failed to validate password");
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Something error at " + e.getMessage(), e);
			e.printStackTrace();
		}
		log.info("Submit Form Action - Exit");
	}

	private String getPassword() {
		log.info("Getting Encrypted Password - Start");
		String password = "";
		try {
			ParameterGroup parentThemeGroup = _parameterGroupLocalService
					.getByCode(SocialEnquiryPortletKeys.DMS_PASSWORD, false);
			//log.info(parentThemeGroup.toString());
			List<Parameter> parameters = _parameterLocalService
					.getByParameterGroupId(parentThemeGroup.getParameterGroupId(), false);
			for (Parameter parameter : parameters) {
				//log.info(parameter.getParamValue());
				password = parameter.getParamValue();
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Failed when getting password, error : " + e.getMessage());
		}
		log.info("Getting Encrypted Password - End");
		return password;
	}

	@Reference
	protected ParameterLocalService _parameterLocalService;

	@Reference
	protected ParameterGroupLocalService _parameterGroupLocalService;

}

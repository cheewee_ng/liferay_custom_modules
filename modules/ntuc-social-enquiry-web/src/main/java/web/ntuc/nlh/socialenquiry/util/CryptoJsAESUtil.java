package web.ntuc.nlh.socialenquiry.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.SecureRandom;
import com.liferay.portal.kernel.util.Base64;

import java.nio.charset.StandardCharsets;
import java.security.DigestException;
import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptoJsAESUtil {

	private static final Log _log = LogFactoryUtil.getLog(CryptoJsAESUtil.class);

	public static String encryptData(String text, String secretKey) {
		String data = null;
		try {
			// generate random salt
			SecureRandom secureRandom = new SecureRandom();
			byte[] saltDataEncryption = new byte[8];
			secureRandom.nextBytes(saltDataEncryption);
			// changed MessageDigest from MD5 to SHA-256
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			// MessageDigest md5 = MessageDigest.getInstance("MD5");
			final byte[][] keyAndIVEncryption = GenerateKeyAndIV(32, 16, 1, saltDataEncryption,
					secretKey.getBytes(StandardCharsets.UTF_8), md5);
			SecretKeySpec keyEncryption = new SecretKeySpec(keyAndIVEncryption[0], "AES");
			IvParameterSpec ivEncryption = new IvParameterSpec(keyAndIVEncryption[1]);

			// encryption
			Cipher aesCBCEncryption = Cipher.getInstance("AES/CBC/PKCS5Padding");
			aesCBCEncryption.init(Cipher.ENCRYPT_MODE, keyEncryption, ivEncryption);
			byte[] cipherTextEncryption = aesCBCEncryption.doFinal(text.getBytes(StandardCharsets.UTF_8));
			// concate 8 zero bytes + salt + cipherTextEncryption
			int arrayLength = 8 + saltDataEncryption.length + cipherTextEncryption.length;
			byte[] cipherTextEncryptionComplete = new byte[arrayLength];
			System.arraycopy(saltDataEncryption, 0, cipherTextEncryptionComplete, 8, saltDataEncryption.length);
			System.arraycopy(cipherTextEncryption, 0, cipherTextEncryptionComplete, 16, cipherTextEncryption.length);
			String cipherTextBase64 = Base64.encode(cipherTextEncryptionComplete);
			// now we are using the new cipherTextBase64 as input for the decryption
			data = cipherTextBase64;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return data;
	}

	public static String decryptData(String encryptedData, String secretKey) {
		String data = null;
		try {
			byte[] cipherData = Base64.decode(encryptedData);
			byte[] saltData = Arrays.copyOfRange(cipherData, 8, 16);

			MessageDigest md5 = MessageDigest.getInstance("MD5");
			final byte[][] keyAndIV = GenerateKeyAndIV(32, 16, 1, saltData, secretKey.getBytes("UTF-8"), md5);
			SecretKeySpec key = new SecretKeySpec(keyAndIV[0], "AES");
			IvParameterSpec iv = new IvParameterSpec(keyAndIV[1]);

			byte[] encrypted = Arrays.copyOfRange(cipherData, 16, cipherData.length);
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, key, iv);

			final String decryptedData = new String(cipher.doFinal(encrypted));
			data = decryptedData;
		} catch (Exception e) {
			_log.error("Error while decrypt data: " + e.getMessage());
		}
		return data;
	}

	/**
	 * Generates a key and an initialization vector (IV) with the given salt and
	 * password.
	 * <p>
	 * This method is equivalent to OpenSSL's EVP_BytesToKey function (see
	 * https://github.com/openssl/openssl/blob/master/crypto/evp/evp_key.c). By
	 * default, OpenSSL uses a single iteration, MD5 as the algorithm and UTF-8
	 * encoded password data.
	 * </p>
	 * 
	 * @param keyLength  the length of the generated key (in bytes)
	 * @param ivLength   the length of the generated IV (in bytes)
	 * @param iterations the number of digestion rounds
	 * @param salt       the salt data (8 bytes of data or <code>null</code>)
	 * @param password   the password data (optional)
	 * @param md         the message digest algorithm to use
	 * @return an two-element array with the generated key and IV
	 */
	public static byte[][] GenerateKeyAndIV(int keyLength, int ivLength, int iterations, byte[] salt, byte[] password,
			MessageDigest md) {

		int digestLength = md.getDigestLength();
		int requiredLength = (keyLength + ivLength + digestLength - 1) / digestLength * digestLength;
		byte[] generatedData = new byte[requiredLength];
		int generatedLength = 0;

		try {
			md.reset();

			// Repeat process until sufficient data has been generated
			while (generatedLength < keyLength + ivLength) {

				// Digest data (last digest if available, password data, salt if available)
				if (generatedLength > 0)
					md.update(generatedData, generatedLength - digestLength, digestLength);
				md.update(password);
				if (salt != null)
					md.update(salt, 0, 8);
				md.digest(generatedData, generatedLength, digestLength);

				// additional rounds
				for (int i = 1; i < iterations; i++) {
					md.update(generatedData, generatedLength, digestLength);
					md.digest(generatedData, generatedLength, digestLength);
				}

				generatedLength += digestLength;
			}

			// Copy key and IV into separate byte arrays
			byte[][] result = new byte[2][];
			result[0] = Arrays.copyOfRange(generatedData, 0, keyLength);
			if (ivLength > 0)
				result[1] = Arrays.copyOfRange(generatedData, keyLength, keyLength + ivLength);

			return result;

		} catch (DigestException e) {
			throw new RuntimeException(e);

		} finally {
			// Clean out temporary data
			Arrays.fill(generatedData, (byte) 0);
		}
	}

}

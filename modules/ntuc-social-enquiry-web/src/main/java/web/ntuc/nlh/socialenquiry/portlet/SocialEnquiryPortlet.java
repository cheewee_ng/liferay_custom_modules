package web.ntuc.nlh.socialenquiry.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import api.ntuc.common.util.AESEncryptUtil;
import api.ntuc.common.util.CSRFValidationUtil;
import svc.ntuc.nlh.parameter.model.Parameter;
import svc.ntuc.nlh.parameter.model.ParameterGroup;
import svc.ntuc.nlh.parameter.service.ParameterGroupLocalService;
import svc.ntuc.nlh.parameter.service.ParameterLocalService;
import web.ntuc.nlh.socialenquiry.constants.SocialEnquiryPortletKeys;
import web.ntuc.nlh.socialenquiry.util.CryptoJsAESUtil;

/**
 * @author muhamadpangestu
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=ntuc-module",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.single-page-application=false", "javax.portlet.display-name=SocialEnquiry",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + SocialEnquiryPortletKeys.SOCIALENQUIRY_PORTLET,
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.version=3.0",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class SocialEnquiryPortlet extends MVCPortlet {
	Log log = LogFactoryUtil.getLog(SocialEnquiryPortlet.class);

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		log.info("Social Enquiry portlet render - start");
		try {
			
			String password = null;
			String encryptPass;
			String decryptPass;

			String authToken = CSRFValidationUtil.authToken(renderRequest);
			String byteAuthToken = Base64.encode(authToken.getBytes());

			ParameterGroup parentThemeGroup = _parameterGroupLocalService
					.getByCode(SocialEnquiryPortletKeys.DMS_PASSWORD, false);
			// log.info(parentThemeGroup.toString());
			List<Parameter> parameters = _parameterLocalService
					.getByParameterGroupId(parentThemeGroup.getParameterGroupId(), false);
			for (Parameter parameter : parameters) {
				password = parameter.getParamValue();
			}
			
			encryptPass = CryptoJsAESUtil.encryptData(password, byteAuthToken);
			decryptPass = CryptoJsAESUtil.decryptData(encryptPass, byteAuthToken);
//			log.info("Encrypt : " + encryptPass);
//			log.info("Decrypt : " + decryptPass);
			
			renderRequest.setAttribute("dbPass", password);
			renderRequest.setAttribute("encryptPass", encryptPass);
			renderRequest.setAttribute("authToken", authToken);
			
		} catch (Exception e) {
			log.error("Failed when render Enquiry, error:" + e.getMessage());
		}
		log.info("Social Enquiry portlet render - end");
		super.render(renderRequest, renderResponse);
	}

	@Reference
	protected ParameterLocalService _parameterLocalService;

	@Reference
	protected ParameterGroupLocalService _parameterGroupLocalService;

}
<%@page import="api.ntuc.common.util.CSRFValidationUtil"%>
<%@page import="com.liferay.portal.kernel.util.Base64"%>
<%@page import="web.ntuc.nlh.socialenquiry.constants.MVCCommandNames"%>
<%@ include file="init.jsp"%>

<portlet:actionURL name="<%=MVCCommandNames.SUBMIT_FORM%>"
	var="submitFormURL">
	<portlet:param name="authToken" value="${authToken}" />
</portlet:actionURL>

<%
	Boolean status = false;

	String formName = "EnquiryForm";
	String password = "";

	String logsesTkn = CSRFValidationUtil.authToken(renderRequest);
	logsesTkn = Base64.encode(logsesTkn.getBytes());
%>

<div class="passster-form" id="passster-form">
	<aui:form name="<%= formName %>" method="post"
		action="${submitFormURL}" onSubmit="event.preventDefault();">

		<h4>PROTECTED AREA</h4>
		<p>This content is password-protected. Please verify with a
			password to unlock the content.</p>
		<aui:input name="logsesTkn" type="hidden" value="<%=logsesTkn%>" />
		<aui:input name="password" id="passster_password"
			class="passster-password" type="password" label=""
			value="<%=password%>" placeholder="Enter your password.." />
		<aui:button name="submit" type="submit" cssClass="passster-submit"
			onclick='<%=renderResponse.getNamespace() + "submitCheck();"%>'
			value="Submit" />
		<div id="passter-error">
			<span class="passster-error">Something went wrong. Please try
				again.</span>
		</div>
	</aui:form>
</div>

<script type="text/javascript"
	src="<%=renderRequest.getContextPath()%>/js/aes.js"></script>

<script type="text/javascript">	

function <portlet:namespace />submitCheck(){
	var logVal = atob($('#<portlet:namespace/>logsesTkn').val());
	var origVal = $('#<portlet:namespace/>passster_password').val();
	var encVal = CryptoJS.AES.encrypt(origVal, logVal);
	//alert('Original Encypt : ' + encVal);
	var base64Val = btoa(encVal);
	//alert('Backend Encrypt : ${encryptPass}');
	$('#<portlet:namespace/>passster_password').val(base64Val);

	if(origVal == "${dbPass}"){		
		document.<portlet:namespace/><%=formName%>.submit();
	}else{
		document.getElementById("passter-error").style.display = "block";
	}
}


</script>
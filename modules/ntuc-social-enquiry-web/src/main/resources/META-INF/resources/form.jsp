<%@ include file="/init.jsp"%>

<script type="text/javascript" src="https://js.hsforms.net/forms/v2.js"></script>

<div class="container sp-main-bot sp-main-top" id="hubspot-form">
	<p class="title-1 text-center">
		<span style="font-size: 14pt;"> <em>NOTE: Only use this
				form for enquiries to the Call Centre/Social Media DMs/Direct Email.
				<strong>Make sure to use this page in INCOGNITO MODE</strong>.
		</em>
		</span>
	</p>
	<ul class="nav-tabs-2 breaktabs-991 sp-bot-2 tabs"
		style="max-width: 900px; margin: 0 auto;">
		<li class="navtab active" data-tab="#tab-2" data-nav=".navtab"
			data-content=".tabcontent">Individual</li>
		<li class="navtab" data-tab="#tab-3" data-nav=".navtab"
			data-content=".tabcontent">Corporate</li>
	</ul>
	<div id="tab-2" class="tabcontent current">
		<script type="text/javascript">
			$(document).ready(function() {
				hbspt.forms.create({
					portalId : "6596891",
					formId : "0767ec97-78d9-4506-b866-159f50611ed0"
				});
			});
		</script>

	</div>
	<div id="tab-3" class="tabcontent">
		<script type="text/javascript">
			$(document).ready(function() {
				hbspt.forms.create({
					portalId : "6596891",
					formId : "c2206e13-4ed3-4b84-abcc-92586bfff950"
				});
			});
		</script>
	</div>
</div>
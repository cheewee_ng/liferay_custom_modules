package web.ntuc.nlh.course.admin.util;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetLinkConstants;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetCategoryServiceUtil;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetLinkLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.journal.constants.JournalArticleConstants;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.Conjunction;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ClassName;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import api.ntuc.nlh.content.util.ContentUtil;
import svc.ntuc.nlh.course.admin.model.Course;
import svc.ntuc.nlh.course.admin.service.CourseLocalServiceUtil;
import svc.ntuc.nlh.parameter.model.Parameter;
import svc.ntuc.nlh.parameter.model.ParameterGroup;
import svc.ntuc.nlh.parameter.service.ParameterGroupLocalServiceUtil;
import svc.ntuc.nlh.parameter.service.ParameterLocalServiceUtil;
import web.ntuc.nlh.course.admin.constants.CourseAdminWebPortletKeys;

public class GenerateCourseCMS {
	private static Log log = LogFactoryUtil.getLog(GenerateCourseCMS.class);

//	public void convertToCMS(String courseTitle, String venue, String description, double courseFee, String courseType, int availability, String batchId, boolean fundedCourseFlag, String courseCode, double courseDuration, Date startDate, Date endDate) {
//		
//	}

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public static String generatedXml(String courseTitle, String venue, String description, double courseFee,
			String courseType, int availability, String batchId, boolean fundedCourseFlag, String courseCode,
			double courseDuration, Date startDate, Date endDate) {
		String formatedStartDate = Validator.isNull(startDate) ? "" : sdf.format(startDate);
		String formatedEndDate = Validator.isNull(endDate) ? "" : sdf.format(endDate);
		String locale = LocaleUtil.getDefault().toString();
		String xml = "<?xml version=\"1.0\"?>\r\n" + "\r\n" + "<root available-locales=\"" + locale
				+ "\" default-locale=\"" + locale + "\">\r\n"
				+ "	<dynamic-element name=\"courseTitle\" type=\"text\" index-type=\"keyword\" instance-id=\"hrmelrxe\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + courseTitle
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"batchID\" type=\"text\" index-type=\"keyword\" instance-id=\"axjlodoc\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + batchId
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"courseCode\" type=\"text\" index-type=\"keyword\" instance-id=\"hijoukbu\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + courseCode
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"startDate\" type=\"ddm-date\" index-type=\"keyword\" instance-id=\"efjxtaua\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + formatedStartDate
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"endDate\" type=\"ddm-date\" index-type=\"keyword\" instance-id=\"cwioofuf\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + formatedEndDate
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"venue\" type=\"text\" index-type=\"keyword\" instance-id=\"elxvojiy\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + venue + "]]></dynamic-content>\r\n"
				+ "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"image\" type=\"image\" index-type=\"text\" instance-id=\"smarribq\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"courseSchedule\" type=\"text\" index-type=\"keyword\" instance-id=\"anyhklji\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[Course Schedule]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"courseDescription\" type=\"text\" index-type=\"keyword\" instance-id=\"bgrszoyl\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + description
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"CourseDuration\" type=\"text\" index-type=\"keyword\" instance-id=\"llmxzcye\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + courseDuration
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"modeofAssesment\" type=\"text\" index-type=\"keyword\" instance-id=\"uvgbyeoa\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"whoShouldAttend\" type=\"text_box\" index-type=\"text\" instance-id=\"iagmtvij\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"noStudentEnrolled\" type=\"text\" index-type=\"keyword\" instance-id=\"khchgxvu\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + availability
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"courseOverview\" type=\"text\" index-type=\"keyword\" instance-id=\"fhwcdsat\">\r\n"
				+ "		<dynamic-element name=\"DetailCourseOverview\" instance-id=\"vceymloc\" type=\"text_area\" index-type=\"text\">\r\n"
				+ "			<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "		</dynamic-element>\r\n" + "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[Course Overview]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"courseObjectives\" type=\"text\" index-type=\"keyword\" instance-id=\"dciwjijx\">\r\n"
				+ "		<dynamic-element name=\"detailCourseObjectives\" instance-id=\"utkfmban\" type=\"text_area\" index-type=\"text\">\r\n"
				+ "			<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "		</dynamic-element>\r\n" + "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[Course Objectives]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"prerequisites\" type=\"text\" index-type=\"keyword\" instance-id=\"aggiupbm\">\r\n"
				+ "		<dynamic-element name=\"detailPrerequisites\" instance-id=\"dctewfrp\" type=\"text_area\" index-type=\"text\">\r\n"
				+ "			<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "		</dynamic-element>\r\n" + "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[Pre-requisites]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"courseOutline\" type=\"text\" index-type=\"keyword\" instance-id=\"wgpokbde\">\r\n"
				+ "		<dynamic-element name=\"detailCourseOutline\" instance-id=\"smancurr\" type=\"text_area\" index-type=\"text\">\r\n"
				+ "			<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "		</dynamic-element>\r\n" + "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[Course Outline]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"certificateObtainedAndConferredBy\" type=\"text\" index-type=\"keyword\" instance-id=\"tclwvldx\">\r\n"
				+ "		<dynamic-element name=\"detailCertificateObtainedAndConferredBy\" instance-id=\"oclnqzdq\" type=\"text_area\" index-type=\"text\">\r\n"
				+ "			<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "		</dynamic-element>\r\n" + "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[Certificate Obtained and Conferred by]]></dynamic-content>\r\n"
				+ "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"whatsInItForMe\" type=\"text\" index-type=\"keyword\" instance-id=\"uqoiilnr\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[What's In It for Me]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"detailWhatsInItforMe\" type=\"text_area\" index-type=\"text\" instance-id=\"hiwrsflz\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"additionalDetails\" type=\"text\" index-type=\"keyword\" instance-id=\"znatiyva\">\r\n"
				+ "		<dynamic-element name=\"detailAdditionalDetails\" instance-id=\"pbroubyj\" type=\"text_area\" index-type=\"text\">\r\n"
				+ "			<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "		</dynamic-element>\r\n" + "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[Additional Details]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"popular\" type=\"boolean\" index-type=\"keyword\" instance-id=\"rilblaws\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"price\" type=\"ddm-number\" index-type=\"keyword\" instance-id=\"aaksbaex\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + courseFee
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"funded\" type=\"boolean\" index-type=\"keyword\" instance-id=\"vbuzsjyp\">\r\n"
				+ "		<dynamic-content language-id=\"" + locale + "\"><![CDATA[" + fundedCourseFlag
				+ "]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n"
				+ "	<dynamic-element name=\"Textx1q0\" type=\"text\" index-type=\"\" instance-id=\"krenopnr\">\r\n"
				+ "		<dynamic-element name=\"Text2a02\" instance-id=\"tymbokxw\" type=\"text\" index-type=\"keyword\">\r\n"
				+ "			<dynamic-content language-id=\"" + locale + "\"><![CDATA[]]></dynamic-content>\r\n"
				+ "		</dynamic-element>\r\n" + "		<dynamic-content language-id=\"" + locale
				+ "\"><![CDATA[ENQUIRE NOW]]></dynamic-content>\r\n" + "	</dynamic-element>\r\n" + "</root>";
		return xml;
	}

	public static void generate() {
		try {
			Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
			long groupId = GroupLocalServiceUtil
					.getGroup(company.getCompanyId(), CourseAdminWebPortletKeys.GUEST_GROUP_NAME).getGroupId();
			ParameterGroup groupGlobalParam = ParameterGroupLocalServiceUtil
					.getByCode(CourseAdminWebPortletKeys.PARAMETER_GLOBAL_GROUP_CODE, false);
			Parameter globalEmailParam = ParameterLocalServiceUtil.getByGroupCode(groupId,
					groupGlobalParam.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_GLOBAL_EMAIL_CODE,
					false);
			String email = globalEmailParam.getParamValue();
			long userId = UserLocalServiceUtil.getUserIdByEmailAddress(company.getCompanyId(), email);
//			List<Course> allCourses = CourseLocalServiceUtil.getCourses(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
//			log.info("allCourses.size() = "+allCourses.size());
			List<Course> allCourses = CourseLocalServiceUtil.getAllActiveAndUpdatedCourse(false, true);
			List<String> courseCodes = allCourses.stream().map(Course -> Course.getCourseCode()).distinct()
					.collect(Collectors.toList());

			for (String courseCode : courseCodes) {
				List<Course> courses = CourseLocalServiceUtil.getCourseByCourseCode(courseCode, false);
				Course course = courses.get(0);
				ParameterGroup groupCourseAdmin = ParameterGroupLocalServiceUtil
						.getByCode(CourseAdminWebPortletKeys.PARAMETER_COURSE_ADMIN_GROUP_CODE, false);
				long siteGroupId = groupCourseAdmin.getGroupId();

				Parameter structureParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
						groupCourseAdmin.getParameterGroupId(),
						CourseAdminWebPortletKeys.PARAMETER_COURSE_STRUCTURE_CODE, false);
				DynamicQuery structureQuery = DDMStructureLocalServiceUtil.dynamicQuery();
				structureQuery
						.add(PropertyFactoryUtil.forName("name").like("%>" + structureParam.getParamValue() + "<%"));
				structureQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
				List<DDMStructure> ddmStructures = DDMStructureLocalServiceUtil.dynamicQuery(structureQuery, 0, 1);

				Parameter templateParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
						groupCourseAdmin.getParameterGroupId(),
						CourseAdminWebPortletKeys.PARAMETER_COURSE_TEMPLATE_CODE, false);
				DynamicQuery templateQuery = DDMTemplateLocalServiceUtil.dynamicQuery();
				templateQuery
						.add(PropertyFactoryUtil.forName("name").like("%>" + templateParam.getParamValue() + "<%"));
				templateQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
				List<DDMTemplate> ddmTemplates = DDMTemplateLocalServiceUtil.dynamicQuery(templateQuery);

				Parameter folderParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
						groupCourseAdmin.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_COURSE_FOLDER_CODE,
						false);
				DynamicQuery folderQuery = JournalFolderLocalServiceUtil.dynamicQuery();
				folderQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
				folderQuery.add(PropertyFactoryUtil.forName("name").eq(folderParam.getParamValue()));
				List<JournalFolder> folders = JournalFolderLocalServiceUtil.dynamicQuery(folderQuery);
				long folderId = folders.get(0).getFolderId();

				Map<Locale, String> titleMap = new HashMap<Locale, String>();
				Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
				Map<String, byte[]> articleUrlMap = new HashMap<String, byte[]>();

				titleMap.put(LocaleUtil.getDefault(), course.getCourseTitle());
				descriptionMap.put(LocaleUtil.getDefault(), course.getDescription());
				articleUrlMap.put(LocaleUtil.getDefault().toString(),
						course.getCourseTitle().replace(" ", "-").getBytes());
				Parameter globalTopicParam = ParameterLocalServiceUtil.getByGroupCode(groupId,
						groupGlobalParam.getParameterGroupId(),
						CourseAdminWebPortletKeys.PARAMETER_GLOBAL_SEARCH_TOPIC_CODE, false);
				AssetVocabulary topicVocabulary = AssetVocabularyLocalServiceUtil
						.fetchGroupVocabulary(company.getGroup().getGroupId(), globalTopicParam.getParamValue());
				OrderByComparator<AssetCategory> order = null;
				List<AssetCategory> assetCategories = AssetCategoryLocalServiceUtil.getVocabularyCategories(
						topicVocabulary.getVocabularyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, order);
				List<AssetCategory> courseCategory = assetCategories.stream()
						.filter(category -> category.getName().equals(CourseAdminWebPortletKeys.COURSE_CATEGORY_NAME))
						.collect(Collectors.toList());
				long[] assetCategoryIds = new long[courseCategory.size()];
				for (int i = 0; i < courseCategory.size(); i++) {
					assetCategoryIds[i] = courseCategory.get(i).getCategoryId();
				}

				ServiceContext serviceContext = new ServiceContext();
				serviceContext.setAddGroupPermissions(true);
				serviceContext.setAddGuestPermissions(true);
				serviceContext.setScopeGroupId(siteGroupId);
				serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);

				DynamicQuery subfolderQuery = JournalFolderLocalServiceUtil.dynamicQuery();
				subfolderQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
				subfolderQuery.add(PropertyFactoryUtil.forName("name").eq(course.getCourseType()));
				subfolderQuery.add(PropertyFactoryUtil.forName("parentFolderId").eq(folderId));
				List<JournalFolder> subFolders = JournalFolderLocalServiceUtil.dynamicQuery(subfolderQuery);
				long courseFolderId = 0;
				if (subFolders.size() > 0) {
					courseFolderId = subFolders.get(0).getFolderId();
				} else {
					courseFolderId = JournalFolderLocalServiceUtil
							.addFolder(userId, siteGroupId, folderId, course.getCourseType(), null, serviceContext)
							.getFolderId();
				}

				DynamicQuery articleQuery = JournalArticleLocalServiceUtil.dynamicQuery();
				articleQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
				articleQuery.add(PropertyFactoryUtil.forName("content").like("%[" + course.getCourseCode() + "]%"));
				articleQuery.add(PropertyFactoryUtil.forName("status").ne(WorkflowConstants.STATUS_IN_TRASH));
				List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.dynamicQuery(articleQuery);
				List<JournalArticle> filteredArticles = journalArticles.stream()
						.filter(x -> x.getTemplateId().equals(ddmTemplates.get(0).getTemplateKey())
								&& x.getStructureId().equals(ddmStructures.get(0).getStructureKey()))
						.collect(Collectors.toList());
				JournalArticle resultJournalArticle = null;
				if (filteredArticles.size() > 0) {
					try {
						JournalArticle article = filteredArticles.get(0);
						double version = JournalArticleLocalServiceUtil.getLatestVersion(siteGroupId,
								article.getArticleId());
						JournalArticle latestArticle = JournalArticleLocalServiceUtil
								.getLatestArticle(article.getResourcePrimKey());
						String languageId = LocaleUtil.getDefault().getLanguage();

						String formatedStartDate = Validator.isNull(course.getStartDate()) ? ""
								: sdf.format(course.getStartDate());
						String formatedEndDate = Validator.isNull(course.getEndDate()) ? ""
								: sdf.format(course.getEndDate());

						String xmlContent = latestArticle.getContentByLocale(languageId);
						Map<String, String> existingFieldMap = new HashMap<String, String>();
						existingFieldMap.put("courseTitle", course.getCourseTitle());
						existingFieldMap.put("venue", course.getVenue());
						existingFieldMap.put("courseDescription", course.getDescription());
						existingFieldMap.put("price", course.getCourseFee().toString());
						existingFieldMap.put("batchID", course.getBatchId());
						existingFieldMap.put("courseCode", course.getCourseCode());
						existingFieldMap.put("CourseDuration", course.getCourseDuration().toString());
						existingFieldMap.put("noStudentEnrolled", String.valueOf(course.getAvailability()));
						existingFieldMap.put("funded", String.valueOf(course.getFundedCourseFlag()));
						existingFieldMap.put("popular", String.valueOf(course.getPopular()));
						existingFieldMap.put("startDate", formatedStartDate);
						existingFieldMap.put("endDate", formatedEndDate);

						String editedXmlContent = ContentUtil.getAndReplaceWebContentVal(xmlContent, 1,
								existingFieldMap, languageId);
						int status = 0;
//						long finalFolderId = 0;
						
						if(latestArticle.getStatus() == WorkflowConstants.STATUS_EXPIRED) {
							status = WorkflowConstants.STATUS_APPROVED;
//							finalFolderId = courseFolderId;
//							JournalArticleLocalServiceUtil.moveArticle(siteGroupId, latestArticle.getArticleId(), finalFolderId,
//									serviceContext);
						} else {
							status = latestArticle.getStatus();
//							finalFolderId = latestArticle.getFolderId();
						}
						
						List<AssetCategory> assets = AssetCategoryServiceUtil.getCategories(JournalArticle.class.getName(), latestArticle.getResourcePrimKey());
						log.info("asset size = "+assets.size());
						long[] courseCategoryIds = new long[assets.size()];
						for (int i = 0; i < assets.size(); i++) {
							log.info("name = "+assets.get(i).getName());
							courseCategoryIds[i] = assets.get(i).getCategoryId();
						}
						serviceContext.setAssetCategoryIds(courseCategoryIds);
						
						updatePreviousJournal(userId, latestArticle.getFriendlyURLsXML(), siteGroupId,
								latestArticle.getArticleId(), status ,serviceContext);
						resultJournalArticle = JournalArticleLocalServiceUtil.updateArticle(userId, siteGroupId, 
								latestArticle.getFolderId(), latestArticle.getArticleId(), version, titleMap, descriptionMap, 
								editedXmlContent, ddmStructures.get(0).getStructureKey(), 
								ddmTemplates.get(0).getTemplateKey(), latestArticle.getLayoutUuid(), 
								0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, 0, 0, 0, 
								true, true, false, "", null, null, 
								latestArticle.getUrlTitle(), serviceContext);
						
//						resultJournalArticle = JournalArticleLocalServiceUtil.updateArticle(userId, siteGroupId,
//								finalFolderId, latestArticle.getArticleId(), version, titleMap,
//								descriptionMap, editedXmlContent, latestArticle.getLayoutUuid(), serviceContext);

					} catch (Exception e) {
						e.printStackTrace();
					}
					log.info("updating course code = " + course.getCourseCode() + " completed successfully");
				} else {
					serviceContext.setAssetCategoryIds(assetCategoryIds);

					String xml = GenerateCourseCMS.generatedXml(course.getCourseTitle(), course.getVenue(),
							course.getDescription(), course.getCourseFee(), course.getCourseType(),
							course.getAvailability(), course.getBatchId(), course.getFundedCourseFlag(),
							course.getCourseCode(), course.getCourseDuration(), course.getStartDate(),
							course.getEndDate());
					Layout layout = LayoutLocalServiceUtil.getLayoutByFriendlyURL(siteGroupId, false,
							"/detail-courses-number-2");

					long nullLong = 0;
					String urlTitle = course.getCourseTitle().replace(" ", "-");

					resultJournalArticle = JournalArticleLocalServiceUtil.addArticle(userId, siteGroupId,
							courseFolderId, nullLong, nullLong, "", true, JournalArticleConstants.VERSION_DEFAULT, titleMap, descriptionMap, xml,
							ddmStructures.get(0).getStructureKey(), ddmTemplates.get(0).getTemplateKey(),
							layout.getUuid(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, 0, 0, 0, true, true, false, "",
							null, null, urlTitle, serviceContext);
					
					log.info("adding course code = " + course.getCourseCode() + " completed successfully");
				}
				Set<String> relatedArticleIds = getPopularArticles(resultJournalArticle.getResourcePrimKey(),
						siteGroupId, ddmStructures.get(0).getStructureKey(), ddmTemplates.get(0).getTemplateKey());
				setRelatedAsset(siteGroupId, userId, resultJournalArticle.getArticleId(), relatedArticleIds);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void updatePreviousJournal(long userId, String url, long groupId, String articleId,
			int workflowStatus, ServiceContext serviceContext) {

		List<Double> journalArticlesVersionsToUpdate = new ArrayList<Double>();

		DynamicQuery dq = JournalArticleLocalServiceUtil.dynamicQuery()
				.setProjection(ProjectionFactoryUtil.projectionList().add(ProjectionFactoryUtil.property("id"))
						.add(ProjectionFactoryUtil.property("version")).add(ProjectionFactoryUtil.property("status")))
				.add(PropertyFactoryUtil.forName("groupId").eq(groupId))
				.add(PropertyFactoryUtil.forName("articleId").eq(articleId)).addOrder(OrderFactoryUtil.asc("version"));
		List<Object[]> result = JournalArticleLocalServiceUtil.dynamicQuery(dq);

		for (int i = 0; i < result.size(); i++) {
			double version = (double) result.get(i)[1];
			int status = (int) result.get(i)[2];

			if ((status == WorkflowConstants.STATUS_APPROVED)) {
				journalArticlesVersionsToUpdate.add(version);
			}
		}

		for (double v : journalArticlesVersionsToUpdate) {
			try {
				JournalArticleLocalServiceUtil.updateStatus(userId, groupId, articleId, v,
						workflowStatus, url, new HashMap<String, Serializable>(), serviceContext);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
	}

	private static Set<String> getPopularArticles(long resourcePrimaryKey, long groupId, String ddmStructureKey,
			String ddmTemplateKey) {
		Set<String> relatedArticleIds = new HashSet<String>();
		List<Course> popularCourse = CourseLocalServiceUtil.getAllActiveAndPopularCourse(false, true);

		List<AssetCategory> existingJournalCategories = AssetCategoryLocalServiceUtil
				.getCategories("com.liferay.journal.model.JournalArticle", resourcePrimaryKey);
		AssetVocabulary topicVocabulary = AssetVocabularyLocalServiceUtil.fetchGroupVocabulary(groupId,
				CourseAdminWebPortletKeys.COURSE_TOPIC_VOCAB_NAME);
		List<AssetCategory> existingTopicCategories = existingJournalCategories.stream()
				.filter(x -> x.getVocabularyId() == topicVocabulary.getVocabularyId()).collect(Collectors.toList());
		Set<Long> existingTopicCategoryIds = existingTopicCategories.stream().map(x -> x.getCategoryId())
				.collect(Collectors.toSet());
		for (Course course : popularCourse) {
			DynamicQuery articleQuery = JournalArticleLocalServiceUtil.dynamicQuery();
			articleQuery.add(PropertyFactoryUtil.forName("groupId").eq(groupId));
			articleQuery.add(PropertyFactoryUtil.forName("content").like("%[" + course.getCourseCode() + "]%"));
			articleQuery.add(PropertyFactoryUtil.forName("status").ne(WorkflowConstants.STATUS_IN_TRASH));
			List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.dynamicQuery(articleQuery);
			List<JournalArticle> filteredArticles = journalArticles.stream()
					.filter(x -> x.getTemplateId().equals(ddmTemplateKey) && x.getStructureId().equals(ddmStructureKey))
					.collect(Collectors.toList());

			if (filteredArticles.size() > 0) {
				List<AssetCategory> popularJournalCategories = AssetCategoryLocalServiceUtil.getCategories(
						"com.liferay.journal.model.JournalArticle", filteredArticles.get(0).getResourcePrimKey());
				Set<Long> popularTopicCategoryIds = popularJournalCategories.stream()
						.filter(x -> x.getVocabularyId() == topicVocabulary.getVocabularyId())
						.map(x -> x.getCategoryId()).collect(Collectors.toSet());
				for (long topicId : popularTopicCategoryIds) {
					if (existingTopicCategoryIds.contains(topicId)) {
						relatedArticleIds.add(filteredArticles.get(0).getArticleId());
					}
				}
			}
		}
		return relatedArticleIds;
	}

	private static void setRelatedAsset(long groupId, long userId, String currentArticleId, Set<String> targetArticleIds) {
		try {
			JournalArticle currentArticle = JournalArticleLocalServiceUtil.getArticle(groupId, currentArticleId);
			AssetEntry currentAssetEntry = AssetEntryLocalServiceUtil
					.getEntry("com.liferay.journal.model.JournalArticle", currentArticle.getResourcePrimKey());

			Set<Long> targetEntryIds = new HashSet<Long>();

			for (String targetArticleId : targetArticleIds) {
				JournalArticle targetArticle = JournalArticleLocalServiceUtil.getArticle(groupId, targetArticleId);
				AssetEntry targetAssetEntry = AssetEntryLocalServiceUtil
						.getEntry("com.liferay.journal.model.JournalArticle", targetArticle.getResourcePrimKey());
				targetEntryIds.add(targetAssetEntry.getEntryId());
			}

			AssetLinkLocalServiceUtil.deleteLinks(currentAssetEntry.getEntryId());

			for (long targetEntryId : targetEntryIds) {
				if (targetEntryId != currentAssetEntry.getEntryId()) {
					AssetLinkLocalServiceUtil.addLink(userId, targetEntryId, currentAssetEntry.getEntryId(),
							AssetLinkConstants.TYPE_RELATED, 0);
				}
			}
		} catch (PortalException e) {
			e.printStackTrace();
		}

	}

	public static void moveApprovedCourse() {
		try {
			Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
			long groupId = GroupLocalServiceUtil
					.getGroup(company.getCompanyId(), CourseAdminWebPortletKeys.GUEST_GROUP_NAME).getGroupId();
			ParameterGroup groupGlobalParam = ParameterGroupLocalServiceUtil
					.getByCode(CourseAdminWebPortletKeys.PARAMETER_GLOBAL_GROUP_CODE, false);
			Parameter globalEmailParam = ParameterLocalServiceUtil.getByGroupCode(groupId,
					groupGlobalParam.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_GLOBAL_EMAIL_CODE,
					false);
			String email = globalEmailParam.getParamValue();
			long userId = UserLocalServiceUtil.getUserIdByEmailAddress(company.getCompanyId(), email);
			ParameterGroup groupCourseAdmin = ParameterGroupLocalServiceUtil
					.getByCode(CourseAdminWebPortletKeys.PARAMETER_COURSE_ADMIN_GROUP_CODE, false);
			long siteGroupId = groupCourseAdmin.getGroupId();

			Parameter folderParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
					groupCourseAdmin.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_COURSE_FOLDER_CODE,
					false);
			
			Parameter structureParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
					groupCourseAdmin.getParameterGroupId(),
					CourseAdminWebPortletKeys.PARAMETER_COURSE_STRUCTURE_CODE, false);
			DynamicQuery structureQuery = DDMStructureLocalServiceUtil.dynamicQuery();
			structureQuery
					.add(PropertyFactoryUtil.forName("name").like("%>" + structureParam.getParamValue() + "<%"));
			structureQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			List<DDMStructure> ddmStructures = DDMStructureLocalServiceUtil.dynamicQuery(structureQuery, 0, 1);

			Parameter templateParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
					groupCourseAdmin.getParameterGroupId(),
					CourseAdminWebPortletKeys.PARAMETER_COURSE_TEMPLATE_CODE, false);
			DynamicQuery templateQuery = DDMTemplateLocalServiceUtil.dynamicQuery();
			templateQuery
					.add(PropertyFactoryUtil.forName("name").like("%>" + templateParam.getParamValue() + "<%"));
			templateQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			List<DDMTemplate> ddmTemplates = DDMTemplateLocalServiceUtil.dynamicQuery(templateQuery);
			
			DynamicQuery folderQuery = JournalFolderLocalServiceUtil.dynamicQuery();
			folderQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			folderQuery.add(PropertyFactoryUtil.forName("name").eq(folderParam.getParamValue()));
			List<JournalFolder> generatedCourseFolders = JournalFolderLocalServiceUtil.dynamicQuery(folderQuery);

			List<JournalFolder> generatedChildFolders = JournalFolderLocalServiceUtil.getFolders(siteGroupId,
					generatedCourseFolders.get(0).getFolderId());

			List<JournalArticle> allJournalArticles = new ArrayList<JournalArticle>();
			for (JournalFolder journalFolder : generatedChildFolders) {
				List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.getArticles(siteGroupId,
						journalFolder.getFolderId());
				List<JournalArticle> filteredJournals = journalArticles.stream()
						.filter(distinctByKey(JournalArticle::getArticleId)).collect(Collectors.toList());
				allJournalArticles.addAll(filteredJournals);
			}

			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setAddGroupPermissions(true);
			serviceContext.setAddGuestPermissions(true);
			serviceContext.setScopeGroupId(siteGroupId);
			serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);

			List<JournalArticle> latestJournalArticles = new ArrayList<JournalArticle>();
			for (JournalArticle latestJournal : allJournalArticles) {
				JournalArticle latestArticle = JournalArticleLocalServiceUtil.getLatestArticle(siteGroupId,
						latestJournal.getArticleId());
				latestJournalArticles.add(latestArticle);
			}
			List<JournalArticle> activeJournalArticles = latestJournalArticles.stream()
					.filter(x -> x.getStatus() == WorkflowConstants.STATUS_APPROVED).collect(Collectors.toList());

			for (JournalArticle article : activeJournalArticles) {
				Set<String> relatedArticleIds = getPopularArticles(article.getResourcePrimKey(),
						siteGroupId, ddmStructures.get(0).getStructureKey(), ddmTemplates.get(0).getTemplateKey());
				setRelatedAsset(siteGroupId, userId, article.getArticleId(), relatedArticleIds);
				
				long targetFolderId = getArticleFolderByThemeName(article, siteGroupId, userId);
				JournalArticleLocalServiceUtil.moveArticle(siteGroupId, article.getArticleId(), targetFolderId,
						serviceContext);
			}
		} catch (Exception e) {

		}

	}

	private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}

	private static long getArticleFolderByThemeName(JournalArticle article, long groupId, long userId) {
		List<AssetCategory> journalCategories = AssetCategoryLocalServiceUtil
				.getCategories("com.liferay.journal.model.JournalArticle", article.getResourcePrimKey());
		AssetVocabulary themeVocabulary = AssetVocabularyLocalServiceUtil.fetchGroupVocabulary(groupId,
				CourseAdminWebPortletKeys.COURSE_THEME_VOCAB_NAME);
		List<AssetCategory> courseThemeCategories = journalCategories.stream()
				.filter(x -> x.getVocabularyId() == themeVocabulary.getVocabularyId()).collect(Collectors.toList());
		AssetCategory courseThemeCategory = courseThemeCategories.get(0);

		DynamicQuery folderQuery = JournalFolderLocalServiceUtil.dynamicQuery();
		folderQuery.add(PropertyFactoryUtil.forName("groupId").eq(groupId));
		folderQuery.add(PropertyFactoryUtil.forName("name").eq(CourseAdminWebPortletKeys.COURSE_CATEGORY_NAME));
		List<JournalFolder> coursesFolders = JournalFolderLocalServiceUtil.dynamicQuery(folderQuery);
		long parentFolderId = coursesFolders.get(0).getFolderId();

		DynamicQuery subfolderQuery = JournalFolderLocalServiceUtil.dynamicQuery();
		subfolderQuery.add(PropertyFactoryUtil.forName("groupId").eq(groupId));
		subfolderQuery.add(PropertyFactoryUtil.forName("name").eq(courseThemeCategory.getName()));
		subfolderQuery.add(PropertyFactoryUtil.forName("parentFolderId").eq(parentFolderId));
		List<JournalFolder> subFolders = JournalFolderLocalServiceUtil.dynamicQuery(subfolderQuery);

		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);
		serviceContext.setScopeGroupId(groupId);
		serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);

		long themeFolderId = 0;
		if (subFolders.size() > 0) {
			themeFolderId = subFolders.get(0).getFolderId();
		} else {
			try {
				themeFolderId = JournalFolderLocalServiceUtil
						.addFolder(userId, groupId, parentFolderId, courseThemeCategory.getName(), null, serviceContext)
						.getFolderId();
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}

		return themeFolderId;
	}
	
	public static void deleteNonTMSCourse() {
		try {
			Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
			long groupId = GroupLocalServiceUtil
					.getGroup(company.getCompanyId(), CourseAdminWebPortletKeys.GUEST_GROUP_NAME).getGroupId();
			ParameterGroup groupGlobalParam = ParameterGroupLocalServiceUtil
					.getByCode(CourseAdminWebPortletKeys.PARAMETER_GLOBAL_GROUP_CODE, false);
			Parameter globalEmailParam = ParameterLocalServiceUtil.getByGroupCode(groupId,
					groupGlobalParam.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_GLOBAL_EMAIL_CODE,
					false);
			String email = globalEmailParam.getParamValue();
			long userId = UserLocalServiceUtil.getUserIdByEmailAddress(company.getCompanyId(), email);
			
			ParameterGroup groupCourseAdmin = ParameterGroupLocalServiceUtil
					.getByCode(CourseAdminWebPortletKeys.PARAMETER_COURSE_ADMIN_GROUP_CODE, false);
			long siteGroupId = groupCourseAdmin.getGroupId();
			
			Parameter structureParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
					groupCourseAdmin.getParameterGroupId(),
					CourseAdminWebPortletKeys.PARAMETER_COURSE_STRUCTURE_CODE, false);
			DynamicQuery structureQuery = DDMStructureLocalServiceUtil.dynamicQuery();
			structureQuery
					.add(PropertyFactoryUtil.forName("name").like("%>" + structureParam.getParamValue() + "<%"));
			structureQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			List<DDMStructure> ddmStructures = DDMStructureLocalServiceUtil.dynamicQuery(structureQuery, 0, 1);

			Parameter templateParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
					groupCourseAdmin.getParameterGroupId(),
					CourseAdminWebPortletKeys.PARAMETER_COURSE_TEMPLATE_CODE, false);
			DynamicQuery templateQuery = DDMTemplateLocalServiceUtil.dynamicQuery();
			templateQuery
					.add(PropertyFactoryUtil.forName("name").like("%>" + templateParam.getParamValue() + "<%"));
			templateQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			List<DDMTemplate> ddmTemplates = DDMTemplateLocalServiceUtil.dynamicQuery(templateQuery);
			
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setAddGroupPermissions(true);
			serviceContext.setAddGuestPermissions(true);
			serviceContext.setScopeGroupId(siteGroupId);
			
			List<Course> courses = CourseLocalServiceUtil.getAllActiveCourse();
			
			DynamicQuery articleQuery = JournalArticleLocalServiceUtil.dynamicQuery();
			articleQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
//			articleQuery.add(PropertyFactoryUtil.forName("content").like("%[" + course.getCourseCode() + "]%"));
			articleQuery.add(PropertyFactoryUtil.forName("status").ne(WorkflowConstants.STATUS_IN_TRASH));
//			Conjunction conjunction = RestrictionsFactoryUtil.conjunction();
			for(Course c : courses) {
				articleQuery.add(RestrictionsFactoryUtil.not(PropertyFactoryUtil.forName("content").like("%[" + c.getCourseCode() + "]%")));
			}
			List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.dynamicQuery(articleQuery);
			List<JournalArticle> filteredArticles = journalArticles.stream().filter(x -> x.getTemplateId().equals(ddmTemplates.get(0).getTemplateKey()) && x.getStructureId().equals(ddmStructures.get(0).getStructureKey())).collect(Collectors.toList());
			for(JournalArticle article : filteredArticles) {
				updatePreviousJournal(userId, article.getFriendlyURLsXML(), siteGroupId,
						article.getArticleId(), WorkflowConstants.STATUS_EXPIRED ,serviceContext);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}

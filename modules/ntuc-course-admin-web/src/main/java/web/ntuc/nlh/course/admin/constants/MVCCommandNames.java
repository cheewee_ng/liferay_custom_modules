package web.ntuc.nlh.course.admin.constants;

public class MVCCommandNames {
	public static final String COURSE_DATA_RESOURCES = "courseDataResources";
	public static final String ADD_COURSE_API_ACTION = "addCourseFromApiAction";
	public static final String GENERATE_COURSE_ARTICLE_ACTION = "generateCourseArticleAction";
	public static final String COURSE_VIEW_RENDER= "courseViewRender";
}

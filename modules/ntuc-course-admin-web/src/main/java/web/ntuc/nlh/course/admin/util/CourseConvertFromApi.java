package web.ntuc.nlh.course.admin.util;

import com.liferay.portal.kernel.dao.orm.Conjunction;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.Http;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import api.ntuc.common.util.ComparationUtil;
import api.ntuc.common.util.DateUtil;
import api.ntuc.common.util.HttpApiUtil;
import svc.ntuc.nlh.course.admin.exception.NoSuchCourseException;
import svc.ntuc.nlh.course.admin.model.Course;
import svc.ntuc.nlh.course.admin.service.CourseLocalServiceUtil;
import svc.ntuc.nlh.parameter.model.Parameter;
import svc.ntuc.nlh.parameter.model.ParameterGroup;
import svc.ntuc.nlh.parameter.service.ParameterGroupLocalServiceUtil;
import svc.ntuc.nlh.parameter.service.ParameterLocalServiceUtil;
import web.ntuc.nlh.course.admin.constants.CourseAdminWebPortletKeys;

public class CourseConvertFromApi {
	
	private static Log log = LogFactoryUtil.getLog(CourseConvertFromApi.class);
	static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	public static void converter() {
		try {
			Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
			long groupId = GroupLocalServiceUtil.getGroup(company.getCompanyId(), CourseAdminWebPortletKeys.GUEST_GROUP_NAME).getGroupId();
			
//			log.info("guest group id = "+groupId );
//			GET CLIENT ID & CLIENT SECRET FROM PARAMETER
			ParameterGroup parameterAuthGroup = ParameterGroupLocalServiceUtil
					.getByCode(CourseAdminWebPortletKeys.PARAMETER_AUTH_GROUP_CODE, false);
//			log.info(parameterAuthGroup);
			long siteGroupId = parameterAuthGroup.getGroupId();
//			log.info("sitegroupid = "+siteGroupId);
			Parameter parameterClientId = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
					parameterAuthGroup.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_CLIENT_ID_CODE,
					false);
			Parameter parameterClientSecret = ParameterLocalServiceUtil.getByGroupCode(siteGroupId,
					parameterAuthGroup.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_CLIENT_SECRET_CODE,
					false);

//			GET API URL FROM PARAMETER
			ParameterGroup parameterApiGroup = ParameterGroupLocalServiceUtil.getByCode(CourseAdminWebPortletKeys.PARAMETER_URL_GROUP_CODE, false);	
			Parameter parameterApiGetCourse = ParameterLocalServiceUtil.getByGroupCode(siteGroupId, parameterApiGroup.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_GET_COURSE_CODE, false);
			Parameter parameterApiGetPopularCourse = ParameterLocalServiceUtil.getByGroupCode(siteGroupId, parameterApiGroup.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_GET_POPULAR_COURSE_CODE, false);
			
//			GET RESPONSE FROM API
			Object courseResponse = HttpApiUtil.request(parameterApiGetCourse.getParamValue(), Http.Method.GET.name(), "", parameterClientId.getParamValue(), parameterClientSecret.getParamValue());
			Object popularCourseResponse = HttpApiUtil.request(parameterApiGetPopularCourse.getParamValue(), Http.Method.GET.name(), "", parameterClientId.getParamValue(), parameterClientSecret.getParamValue());
			JSONArray courseJsonArray = JSONFactoryUtil.createJSONArray(courseResponse.toString());
			JSONArray popularCourseJsonArray = JSONFactoryUtil.createJSONArray(popularCourseResponse.toString());
//			log.info("jsonArray = "+jsonArray);
			Set<String> popularCourses = new HashSet<String>();
			
			for(Object obj : popularCourseJsonArray) {
				JSONObject json = JSONFactoryUtil.createJSONObject(obj.toString());
				popularCourses.add(json.getString("coursecode"));
			}
			
			for (Object object : courseJsonArray) {
				JSONObject obj = JSONFactoryUtil.createJSONObject(object.toString());
//				log.info(obj.get("course_code"));
//				CourseLocalServiceUtil
				insertApiIntoDb(siteGroupId, obj, popularCourses);
			}
//			this.sdf.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
//			String todayString = this.sdf.format(new Date());
//			Date todayDate = this.sdf.
			DynamicQuery dynamicQuery = CourseLocalServiceUtil.dynamicQuery();
			Conjunction conjunctionQuery = RestrictionsFactoryUtil.conjunction();
			conjunctionQuery.add(RestrictionsFactoryUtil.eq("deleted", false));
			conjunctionQuery.add(RestrictionsFactoryUtil.le("startDate",new Date()));
//			conjunctionQuery.add(RestrictionsFactoryUtil.not(RestrictionsFactoryUtil.in(propertyName, values)))
			dynamicQuery.add(conjunctionQuery);
			List<Course> expiredCourses = CourseLocalServiceUtil.dynamicQuery(dynamicQuery);
			for(Course c : expiredCourses) {
				if(c.getDeleted() == false) {
					CourseLocalServiceUtil.updateCourse(c.getCourseId(), c.getEndDate(), c.getVenue(), c.getAllowOnlinePayment(), c.getCourseTitle(), c.getAllowWebRegistration(), c.getDescription(), c.getAvailability(), c.getBatchId(), c.getWebExpiry(), c.getFundedCourseFlag(), c.getCourseCode(), c.getCourseDuration(), c.getStartDate(), c.getCourseFee(), c.getCourseType(), true, false,false);
				}
			}
			
//			List<Course> updatedCourses = CourseLocalServiceUtil.getCourseByCourseCodeAndStartDate(courseCode, startDate);
//			log.info(responseObj);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static void insertApiIntoDb(long groupId, JSONObject jsonCourse,Set<String> popularCourses) {
		try {
//			log.info("groupid = "+groupId);
			Date endDate = DateUtil.parse(jsonCourse.getString("end_date"), sdf);
			String venue = jsonCourse.getString("venue");
			boolean allowOnlinePayment = jsonCourse.getString("allow_online_payment").equalsIgnoreCase("yes") ? true : false ;
			String courseTitle = jsonCourse.getString("course_title");
			boolean allowWebRegistration = jsonCourse.getString("allow_web_registration").equalsIgnoreCase("yes") ? true : false;
			String description = jsonCourse.getString("description");
			int availability = jsonCourse.getInt("availability");
			String batchId = jsonCourse.getString("batchid");
			Date webExpiry = DateUtil.parse(jsonCourse.getString("web_expiry"), sdf);
			boolean fundedCourseFlag = jsonCourse.getString("funded_course_flag").equalsIgnoreCase("yes") ? true : false;
			String courseCode = jsonCourse.getString("course_code");
			double courseDuration = jsonCourse.getDouble("course_duration");
			Date startDate = DateUtil.parse(jsonCourse.getString("start_date"), sdf);
			double courseFee = jsonCourse.getDouble("course_fee", 0);
			String courseType = jsonCourse.getString("course_type");
			boolean updated = false;
			boolean popular = false;
			
			if(popularCourses.contains(courseCode)) {
				popular = true;
			}
//			if(startDate.after(this.sdf.parse(todayString))) {
				try {
					Course existingCourse = CourseLocalServiceUtil.getCourseByCourseCodeAndBatchId(courseCode, batchId);
//					log.info("existing course id = "+existingCourse.getCourseId());
					if(!compareToExistingCourse(existingCourse, endDate, venue, allowOnlinePayment, courseTitle, allowWebRegistration, description, availability, batchId, webExpiry, fundedCourseFlag, courseCode, courseDuration, startDate, courseFee, courseType,popular)) {
						updated = true;
					}
					CourseLocalServiceUtil.updateCourse(existingCourse.getCourseId(), endDate, venue, allowOnlinePayment, courseTitle, allowWebRegistration, description, availability, batchId, webExpiry, fundedCourseFlag, courseCode, courseDuration, startDate, courseFee, courseType, false, updated, popular);
//					log.info("updated course id = "+existingCourse.getCourseId());
				} catch (NoSuchCourseException e) {
					Course course = CourseLocalServiceUtil.addCourse(groupId, endDate, venue, allowOnlinePayment, courseTitle, allowWebRegistration, description, availability, batchId, webExpiry, fundedCourseFlag, courseCode, courseDuration, startDate, courseFee, courseType, false, true, popular);
//					log.info("add course id = "+course.getCourseId());
				}	
//			} else {
//				log.info("expired course);
//				try {
//					Course deletedCourse = CourseLocalServiceUtil.getCourseByCourseCodeAndBatchId(courseCode, batchId);
					
//				} catch (NoSuchCourseException e) {

//				}
//			}
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}
	
	private static boolean compareToExistingCourse(Course existingCourse, Date endDate, String venue,
			boolean allowOnlinePayment, String courseTitle, boolean allowWebRegistration, String description,
			int availability, String batchId, Date webExpiry, boolean fundedCourseFlag, String courseCode,
			double courseDuration, Date startDate, double courseFee, String courseType, boolean popular) {
		boolean isSame = false;
//		log.info("course code = "+courseCode);
//		log.info("popular course = "+existingCourse.getPopular());
//		log.info("popular input = "+popular);
//		log.info(existingCourse.getPopular() == popular);
//		log.info("compare popular = "+ComparationUtil.compare(existingCourse.getPopular(), popular));
		isSame = ComparationUtil.compare(existingCourse.getEndDate(),endDate) 
				&& ComparationUtil.compare(existingCourse.getVenue(), venue)  
				&& existingCourse.getAllowOnlinePayment() == allowOnlinePayment
				&& ComparationUtil.compare(existingCourse.getCourseTitle(),courseTitle)
				&& existingCourse.getAllowWebRegistration() ==  allowWebRegistration
				&& ComparationUtil.compare(existingCourse.getDescription(),description)
				&& existingCourse.getAvailability() == availability
				&& ComparationUtil.compare(existingCourse.getBatchId(),batchId)
				&& ComparationUtil.compare(existingCourse.getWebExpiry(),webExpiry)
				&& existingCourse.getFundedCourseFlag() == fundedCourseFlag
				&& ComparationUtil.compare(existingCourse.getCourseCode(),courseCode)
				&& existingCourse.getCourseDuration() == courseDuration
				&& ComparationUtil.compare(existingCourse.getStartDate(),startDate)
				&& existingCourse.getCourseFee() == courseFee
				&& ComparationUtil.compare(existingCourse.getCourseType(),courseType)
				&& existingCourse.getPopular() == popular;
//		log.info("is same = "+isSame);
		return isSame;
	}
	
//	@Reference
//	protected ParameterLocalService _parameterLocalService;
//
//	@Reference
//	protected ParameterGroupLocalService _parameterGroupLocalService;
}

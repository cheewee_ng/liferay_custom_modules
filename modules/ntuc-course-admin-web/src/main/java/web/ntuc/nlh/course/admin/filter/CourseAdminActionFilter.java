package web.ntuc.nlh.course.admin.filter;

import com.liferay.portal.kernel.util.ParamUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;

import org.osgi.service.component.annotations.Component;

import api.ntuc.common.override.OverrideActionRequestParam;
import api.ntuc.common.util.CSRFValidationUtil;
import api.ntuc.common.util.XSSValidationUtil;
import web.ntuc.nlh.course.admin.constants.CourseAdminWebPortletKeys;

@Component(immediate = true, property = {
		"javax.portlet.name=" + CourseAdminWebPortletKeys.COURSE_ADMIN_PORTLET }, service = PortletFilter.class)
public class CourseAdminActionFilter implements ActionFilter {
	@Override
	public void init(FilterConfig filterConfig) throws PortletException {

	}

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain)
			throws IOException, PortletException {

//		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Map<String, Object> newParam = new HashMap<String, Object>();

		/*
		 * Check Permission for access page
		 */
		boolean isAuthorized = true;
		
		newParam.put("isAuthorized", isAuthorized);

		/*
		 * CSRF Validation
		 */
		boolean validCSRF = false;
		String authToken = ParamUtil.getString(request, "authToken");
		if (CSRFValidationUtil.isValidRequest(request, authToken)) {
			validCSRF = true;
		}
		newParam.put("validCSRF", validCSRF);

		/*
		 * XSS Validation
		 */
		boolean xssPass = true;
		Set<String> names = request.getActionParameters().getNames();
		for (String name : names) {
			String param = request.getActionParameters().getValue(name);
			if (!XSSValidationUtil.isValid(param)) {
				xssPass = false;
				break;
			}
		}
		newParam.put("xssPass", xssPass);

		OverrideActionRequestParam over = new OverrideActionRequestParam(request, newParam);
		chain.doFilter(over, response);
	}
}

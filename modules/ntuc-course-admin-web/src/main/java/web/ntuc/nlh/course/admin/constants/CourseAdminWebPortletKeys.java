package web.ntuc.nlh.course.admin.constants;

import com.liferay.portal.kernel.util.PropsUtil;

/**
 * @author fandifadillah
 */
public class CourseAdminWebPortletKeys {
	
	public static final String COURSE_ADMIN_PORTLET = "web_ntuc_nlh_course_admin_CourseAdminWebPortletKeys";
	public static final String COURSE_ADMIN_DISPLAY = "Course Admin";
	public static final String PARAMETER_AUTH_GROUP_CODE = "httpAuth";
	public static final String PARAMETER_URL_GROUP_CODE = "apiUrl";
	public static final String PARAMETER_CLIENT_ID_CODE = "clientId";
	public static final String PARAMETER_CLIENT_SECRET_CODE = "clientSecret";
	public static final String PARAMETER_GET_COURSE_CODE = "getCourseByCode";
	public static final String PARAMETER_GET_POPULAR_COURSE_CODE = "getPopularCourse";
	public static final String PARAMETER_GLOBAL_GROUP_CODE = "globalParam";
	public static final String PARAMETER_COURSE_ADMIN_GROUP_CODE = "courseAdmin";
	public static final String PARAMETER_GLOBAL_EMAIL_CODE = "userEmail";
	public static final String PARAMETER_GLOBAL_SEARCH_TOPIC_CODE = "searchTopic";
//	public static final String PARAMETER_GLOBAL_GROUP_NAME_CODE = "groupName";
	public static final String PARAMETER_COURSE_STRUCTURE_CODE = "courseStructure";
	public static final String PARAMETER_COURSE_TEMPLATE_CODE = "courseTemplate";
	public static final String PARAMETER_COURSE_FOLDER_CODE = "courseFolder";
	public static final String PARAMETER_TARGET_SITE_CODE = "courseSite";
	public static final String COURSE_CATEGORY_NAME = "courses";
	public static final String COURSE_THEME_VOCAB_NAME = "course theme";
	public static final String COURSE_TOPIC_VOCAB_NAME = "course topic";
	
//	public static final String COURSE_DDM_STRUCTURE = PropsUtil.get("ntuc.course.structure");
//	public static final String COURSE_DDM_TEMPLATE = PropsUtil.get("ntuc.course.template");
//	public static final String COURSE_JOURNAL_FOLDER = "generated courses";
	public static final String GUEST_GROUP_NAME = "Guest";//PropsUtil.get("ntuc.site.group.name");
//	public static final String GROUP_NAME = "Guest";//PropsUtil.get("ntuc.site.group.name");
//	public static final String USER_EMAIL = PropsUtil.get("ntuc.default.user.email");
}
package web.ntuc.nlh.course.admin.action;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetLink;
import com.liferay.asset.kernel.model.AssetLinkConstants;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetLinkLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleResource;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalArticleResourceLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.CompanyLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;

import org.osgi.service.component.annotations.Component;

import api.ntuc.common.util.ComparationUtil;
import api.ntuc.common.util.DateUtil;
import api.ntuc.nlh.content.util.ContentUtil;
import svc.ntuc.nlh.course.admin.exception.CourseValidationException;
import svc.ntuc.nlh.course.admin.model.Course;
import svc.ntuc.nlh.course.admin.service.CourseLocalServiceUtil;
import svc.ntuc.nlh.parameter.exception.ParameterValidationException;
import svc.ntuc.nlh.parameter.model.Parameter;
import svc.ntuc.nlh.parameter.model.ParameterGroup;
import svc.ntuc.nlh.parameter.service.ParameterGroupLocalServiceUtil;
import svc.ntuc.nlh.parameter.service.ParameterLocalServiceUtil;
import web.ntuc.nlh.course.admin.constants.CourseAdminMessagesKey;
import web.ntuc.nlh.course.admin.constants.CourseAdminWebPortletKeys;
import web.ntuc.nlh.course.admin.constants.MVCCommandNames;
import web.ntuc.nlh.course.admin.util.GenerateCourseCMS;

@Component(immediate = true, property = { "mvc.command.name=" + MVCCommandNames.GENERATE_COURSE_ARTICLE_ACTION,
		"javax.portlet.name=" + CourseAdminWebPortletKeys.COURSE_ADMIN_PORTLET }, service = MVCActionCommand.class)
public class GenerateCourseArticleAction extends BaseMVCActionCommand {
	private static Log log = LogFactoryUtil.getLog(GenerateCourseArticleAction.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		log.info("Generate course action - start");
		
		/*SimpleDateFormat testSdf = new SimpleDateFormat("dd/MM/yyyy");
		String date1Str = "-";
		String date2Str = "23/11/2021";
		String date3Str = "24/11/2021";
		String date4Str = "24/11/2021";
		Date date1 = DateUtil.parse(date1Str, testSdf);
		Date date2 = DateUtil.parse(date2Str, testSdf);
		Date date3 = DateUtil.parse(date3Str, testSdf);
		Date date4 = DateUtil.parse(date4Str, testSdf);
		
		log.info(ComparationUtil.compare(date1, date2));
		log.info(ComparationUtil.compare(date1, date3));
		log.info(ComparationUtil.compare(date3, date4));*/
		
//		JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(109145);
//		log.info(journalArticle.get);
		try {
			Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long groupId = themeDisplay.getScopeGroupId();
			long globalGroupId = GroupLocalServiceUtil.getGroup(company.getCompanyId(), CourseAdminWebPortletKeys.GUEST_GROUP_NAME)
					.getGroupId();
			
			ParameterGroup groupGlobalParam = ParameterGroupLocalServiceUtil.getByCode(CourseAdminWebPortletKeys.PARAMETER_GLOBAL_GROUP_CODE, false);
			Parameter globalEmailParam = ParameterLocalServiceUtil.getByGroupCode(globalGroupId, groupGlobalParam.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_GLOBAL_EMAIL_CODE, false);
			String email = globalEmailParam.getParamValue();
			
			long userId = UserLocalServiceUtil.getUserIdByEmailAddress(company.getCompanyId(), email);
			
			boolean isAuthorized = ParamUtil.getBoolean(actionRequest, "isAuthorized");
			boolean validCSRF = ParamUtil.getBoolean(actionRequest, "validCSRF");
			boolean xssPass = ParamUtil.getBoolean(actionRequest, "xssPass");
			String courseCode = ParamUtil.getString(actionRequest, "courseCode");

			if (!isAuthorized || !validCSRF) {
				String msg = "isAuthorized : " + isAuthorized + " | validCSRF : " + validCSRF;
				SessionErrors.add(actionRequest, "you-dont-have-permission-or-your-session-is-end");
				throw new CourseValidationException(msg);
			}

			if (!xssPass) {
				PortletConfig portletConfig = (PortletConfig) actionRequest
						.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
				String msg = LanguageUtil.format(portletConfig.getResourceBundle(themeDisplay.getLocale()),
						CourseAdminMessagesKey.XSS_VALIDATION_NOT_PASS, xssPass);
				SessionErrors.add(actionRequest, "your-input-not-pass-xss");
				throw new ParameterValidationException(msg);
			}
			
			List<Course> courses = CourseLocalServiceUtil.getCourseByCourseCode(courseCode, false);
			Course course = courses.get(0);
			ParameterGroup groupCourseAdmin = ParameterGroupLocalServiceUtil.getByCode(CourseAdminWebPortletKeys.PARAMETER_COURSE_ADMIN_GROUP_CODE, false);
			long siteGroupId = groupCourseAdmin.getGroupId();
			log.info("course code = "+courseCode);
			
			Parameter structureParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId, groupCourseAdmin.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_COURSE_STRUCTURE_CODE, false);
			DynamicQuery structureQuery = DDMStructureLocalServiceUtil.dynamicQuery();
			structureQuery.add(PropertyFactoryUtil.forName("name")
					.like("%>" + structureParam.getParamValue() + "<%"));
			structureQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			List<DDMStructure> ddmStructures = DDMStructureLocalServiceUtil.dynamicQuery(structureQuery, 0, 1);
			log.info("ddmStructures.size() = " + ddmStructures.size());

			Parameter templateParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId, groupCourseAdmin.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_COURSE_TEMPLATE_CODE, false);
			DynamicQuery templateQuery = DDMTemplateLocalServiceUtil.dynamicQuery();
			templateQuery.add(PropertyFactoryUtil.forName("name")
					.like("%>" + templateParam.getParamValue() + "<%"));
			templateQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			List<DDMTemplate> ddmTemplates = DDMTemplateLocalServiceUtil.dynamicQuery(templateQuery);
			log.info("ddmTemplates.size() = " + ddmTemplates.size());

//			DLFolder folder = DLFolderLocalServiceUtil.getFolder(groupId, 0, "courses");
			Parameter folderParam = ParameterLocalServiceUtil.getByGroupCode(siteGroupId, groupCourseAdmin.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_COURSE_FOLDER_CODE, false);
			DynamicQuery folderQuery = JournalFolderLocalServiceUtil.dynamicQuery();
			folderQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			folderQuery.add(PropertyFactoryUtil.forName("name").eq(folderParam.getParamValue()));
			List<JournalFolder> folders = JournalFolderLocalServiceUtil.dynamicQuery(folderQuery);
			log.info("folders.size() = " + folders.size());
			long folderId = folders.get(0).getFolderId();
			
//			long dummyFolderId = 132740;
//			JournalFolder targetedfolder = JournalFolderLocalServiceUtil.getFolder(dummyFolderId);

			Map<Locale, String> titleMap = new HashMap<Locale, String>();
			Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
			Map<String, byte[]> articleUrlMap = new HashMap<String, byte[]>();

			titleMap.put(LocaleUtil.getDefault(), course.getCourseTitle());
			descriptionMap.put(LocaleUtil.getDefault(), course.getDescription());
			articleUrlMap.put(LocaleUtil.getDefault().toString(), course.getCourseTitle().replace(" ", "-").getBytes());
			log.info("articleUrlMap = "+articleUrlMap);
			Parameter globalTopicParam = ParameterLocalServiceUtil.getByGroupCode(globalGroupId, groupGlobalParam.getParameterGroupId(), CourseAdminWebPortletKeys.PARAMETER_GLOBAL_SEARCH_TOPIC_CODE, false);
			AssetVocabulary topicVocabulary = AssetVocabularyLocalServiceUtil.fetchGroupVocabulary(company.getGroup().getGroupId(),
					globalTopicParam.getParamValue());
			OrderByComparator<AssetCategory> order = null;
			List<AssetCategory> assetCategories = AssetCategoryLocalServiceUtil
					.getVocabularyCategories(topicVocabulary.getVocabularyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS,
							order);
			List<AssetCategory> courseCategory = assetCategories.stream().filter(category -> category.getName().equals(CourseAdminWebPortletKeys.COURSE_CATEGORY_NAME)).collect(Collectors.toList());
			long[] assetCategoryIds = new long[courseCategory.size()];
			for (int i=0; i<courseCategory.size(); i++) {
//				assetCategoryIds.add(asset.getCategoryId());
				assetCategoryIds[i] = courseCategory.get(i).getCategoryId();
			}
			
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setAddGroupPermissions(true);
			serviceContext.setAddGuestPermissions(true);
			serviceContext.setScopeGroupId(siteGroupId);
			
//			serviceContext.set
			serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
			
			
			DynamicQuery subfolderQuery = JournalFolderLocalServiceUtil.dynamicQuery();
			subfolderQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			subfolderQuery.add(PropertyFactoryUtil.forName("name").eq(course.getCourseType()));
			subfolderQuery.add(PropertyFactoryUtil.forName("parentFolderId").eq(folderId));
			List<JournalFolder> subFolders = JournalFolderLocalServiceUtil.dynamicQuery(subfolderQuery);
			log.info("subFolders size = "+subFolders.size());
			long courseFolderId = 0; 
			if(subFolders.size() > 0 ) {
				courseFolderId = subFolders.get(0).getFolderId();
			} else {
				courseFolderId = JournalFolderLocalServiceUtil.addFolder(userId, siteGroupId, folderId, course.getCourseType(), null, serviceContext).getFolderId();
			}
			
			DynamicQuery articleQuery = JournalArticleLocalServiceUtil.dynamicQuery();
			articleQuery.add(PropertyFactoryUtil.forName("groupId").eq(siteGroupId));
			articleQuery.add(PropertyFactoryUtil.forName("content").like("%[" + course.getCourseCode() + "]%"));
			articleQuery.add(PropertyFactoryUtil.forName("status").ne(WorkflowConstants.STATUS_IN_TRASH));
			List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.dynamicQuery(articleQuery);
			List<JournalArticle> filteredArticles = journalArticles.stream().filter(x -> x.getTemplateId().equals(ddmTemplates.get(0).getTemplateKey()) && x.getStructureId().equals(ddmStructures.get(0).getStructureKey())).collect(Collectors.toList());
			JournalArticle resultJournalArticle = null;
			if (filteredArticles.size() > 0) {
				try {
//					for (JournalArticle article : filteredArticles) {
						JournalArticle article = filteredArticles.get(0);
						log.info("article id = "+article.getArticleId());
						double version = JournalArticleLocalServiceUtil.getLatestVersion(siteGroupId, article.getArticleId());
						JournalArticle latestArticle = JournalArticleLocalServiceUtil.getLatestArticle(article.getResourcePrimKey());
						String languageId = LocaleUtil.getDefault().getLanguage();					
						
						String formatedStartDate = Validator.isNull(course.getStartDate()) ? "" : this.sdf.format(course.getStartDate());
						String formatedEndDate = Validator.isNull(course.getEndDate()) ? "" : this.sdf.format(course.getEndDate());
						
						String xmlContent = latestArticle.getContentByLocale(languageId);
						Map<String, String> existingFieldMap = new HashMap<String, String>();
						existingFieldMap.put("courseTitle", course.getCourseTitle());
						existingFieldMap.put("venue", course.getVenue());
						existingFieldMap.put("courseDescription", course.getDescription());
						existingFieldMap.put("price", course.getCourseFee().toString());
						existingFieldMap.put("batchID", course.getBatchId());
						existingFieldMap.put("courseCode", course.getCourseCode());
						existingFieldMap.put("CourseDuration", course.getCourseDuration().toString());
						existingFieldMap.put("noStudentEnrolled", String.valueOf(course.getAvailability()));
						existingFieldMap.put("funded", String.valueOf(course.getFundedCourseFlag()));
						existingFieldMap.put("popular", String.valueOf(course.getPopular()));
						existingFieldMap.put("startDate", formatedStartDate);
						existingFieldMap.put("endDate", formatedEndDate);
						
						String editedXmlContent = ContentUtil.getAndReplaceWebContentVal(xmlContent, 1, existingFieldMap, languageId);
						
						this.updatePreviousJournal(userId, latestArticle.getFriendlyURLsXML(), siteGroupId, latestArticle.getArticleId(), WorkflowConstants.STATUS_PENDING, serviceContext);
						resultJournalArticle = JournalArticleLocalServiceUtil.updateArticle(userId, siteGroupId, latestArticle.getFolderId(), latestArticle.getArticleId(), 
								version, titleMap, descriptionMap, editedXmlContent, latestArticle.getLayoutUuid(), serviceContext);
						
				}catch (Exception e) {
					e.printStackTrace();
				}
				log.info("updating course code = "+course.getCourseCode()+" completed successfully");
			} else {
				serviceContext.setAssetCategoryIds(assetCategoryIds);
				
				String xml = GenerateCourseCMS.generatedXml(course.getCourseTitle(), course.getVenue(),
						course.getDescription(), course.getCourseFee(), course.getCourseType(), course.getAvailability(),
						course.getBatchId(), course.getFundedCourseFlag(), course.getCourseCode(),
						course.getCourseDuration(), course.getStartDate(), course.getEndDate());
				Layout layout = LayoutLocalServiceUtil.getLayoutByFriendlyURL(siteGroupId, false, "/detail-courses-number-2");
				log.info("layout uuid = "+layout.getUuid()+" url = "+layout.getFriendlyURL());
				
				long nullLong = 0;
				double nullDouble = 0;
				
				resultJournalArticle = JournalArticleLocalServiceUtil.addArticle(userId, siteGroupId, courseFolderId, 
						nullLong, nullLong, "", true, nullDouble, titleMap, 
						descriptionMap, xml, ddmStructures.get(0).getStructureKey(), 
						ddmTemplates.get(0).getTemplateKey(),layout.getUuid(), 
						0, 0, 0, 0, 
						0, 0, 0,
						0, 0, 0, 
						true, 0, 0, 0, 
						0, 0, true, true, 
						false, "", null, null, "course/"+course.getCourseTitle().replace(" ", "-"), serviceContext);
				log.info("adding course code = "+course.getCourseCode()+" completed successfully");
			}
//			136830
			Set<String> relatedArticleIds = getPopularArticles(filteredArticles.get(0).getResourcePrimKey(),siteGroupId, ddmStructures.get(0).getStructureKey(), ddmTemplates.get(0).getTemplateKey());
//			relatedArticleIds.add("67092");
//			relatedArticleIds.add("136830");
//			relatedArticleIds.add("132689");
			this.setRelatedAsset(siteGroupId, userId, resultJournalArticle.getArticleId(), relatedArticleIds);
		} catch (Exception e) {
			log.error("Error while generating course : " + e.getMessage(), e);
		}
		log.info("Generate course action - end");

	}
	
	private void updatePreviousJournal(long userId, String url, long groupId, String articleId, int workflowStatus, ServiceContext serviceContext) {
		
			List<Double> journalArticlesVersionsToUpdate = new ArrayList<Double>();
			
			DynamicQuery dq = JournalArticleLocalServiceUtil.dynamicQuery()
					.setProjection(ProjectionFactoryUtil.projectionList().add(ProjectionFactoryUtil.property("id"))
							.add(ProjectionFactoryUtil.property("version"))
							.add(ProjectionFactoryUtil.property("status")))
					.add(PropertyFactoryUtil.forName("groupId").eq(groupId))
					.add(PropertyFactoryUtil.forName("articleId").eq(articleId))
					.addOrder(OrderFactoryUtil.asc("version"));
			List<Object[]> result = JournalArticleLocalServiceUtil.dynamicQuery(dq);
			

				for (int i=0; i < result.size(); i++) {
					long id = (long) result.get(i)[0];
					double version = (double) result.get(i)[1];
					int status = (int) result.get(i)[2];

					if ((status == WorkflowConstants.STATUS_APPROVED) ) {
						journalArticlesVersionsToUpdate.add(version);
					}
				}
				
				for (double v : journalArticlesVersionsToUpdate) {
					try {
//						JournalArticleLocalServiceUtil.updateArticle(userId, groupId, folderId, articleId, v, content, serviceContext);
						JournalArticleLocalServiceUtil.updateStatus(userId, groupId, articleId, v, workflowStatus, url, new HashMap<String, Serializable>(), serviceContext);
					} catch (PortalException e) {
						e.printStackTrace();
					}
				}
	}
	
	private Set<String> getPopularArticles (long resourcePrimaryKey, long groupId, String ddmStructureKey, String ddmTemplateKey) {
		Set<String> relatedArticleIds = new HashSet<String>();
		List<Course> popularCourse = CourseLocalServiceUtil.getAllActiveAndPopularCourse(false, true);
		
		List<AssetCategory> existingJournalCategories = AssetCategoryLocalServiceUtil.getCategories("com.liferay.journal.model.JournalArticle", resourcePrimaryKey);
		log.info("existing resource primkey = "+resourcePrimaryKey);
		log.info("existingJournalCategories.size() = "+existingJournalCategories.size());
		AssetVocabulary topicVocabulary = AssetVocabularyLocalServiceUtil.fetchGroupVocabulary(groupId, CourseAdminWebPortletKeys.COURSE_TOPIC_VOCAB_NAME);
		List<AssetCategory> existingTopicCategories = existingJournalCategories.stream().filter(x -> x.getVocabularyId() == topicVocabulary.getVocabularyId()).collect(Collectors.toList());
		Set<Long> existingTopicCategoryIds = existingTopicCategories.stream().map(x -> x.getCategoryId()).collect(Collectors.toSet());
		for(long a : existingTopicCategoryIds ) {
			log.info("existingTopicCategoryId "+a);
		}
		for(Course course : popularCourse) {
			DynamicQuery articleQuery = JournalArticleLocalServiceUtil.dynamicQuery();
			articleQuery.add(PropertyFactoryUtil.forName("groupId").eq(groupId));
			articleQuery.add(PropertyFactoryUtil.forName("content").like("%[" + course.getCourseCode() + "]%"));
			articleQuery.add(PropertyFactoryUtil.forName("status").ne(WorkflowConstants.STATUS_IN_TRASH));
			List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.dynamicQuery(articleQuery);
			List<JournalArticle> filteredArticles = journalArticles.stream().filter(x -> x.getTemplateId().equals(ddmTemplateKey) && x.getStructureId().equals(ddmStructureKey)).collect(Collectors.toList());
			
			if (filteredArticles.size() > 0) {				
				List<AssetCategory> popularJournalCategories = AssetCategoryLocalServiceUtil.getCategories("com.liferay.journal.model.JournalArticle", filteredArticles.get(0).getResourcePrimKey());
				Set<Long> popularTopicCategoryIds = popularJournalCategories.stream().filter(x -> x.getVocabularyId() == topicVocabulary.getVocabularyId()).map(x -> x.getCategoryId()).collect(Collectors.toSet());
				for(long topicId : popularTopicCategoryIds) {
					log.info("popularTopicCategoryIds "+topicId);
					if(existingTopicCategoryIds.contains(topicId)) {
						relatedArticleIds.add(filteredArticles.get(0).getArticleId());
					}
				}
			}
		}
		return relatedArticleIds;
	}
	
	private void setRelatedAsset(long groupId, long userId, String currentArticleId, Set<String> targetArticleIds) {
		try {
			JournalArticle currentArticle = JournalArticleLocalServiceUtil.getArticle(groupId, currentArticleId);
			AssetEntry currentAssetEntry = AssetEntryLocalServiceUtil.getEntry("com.liferay.journal.model.JournalArticle", currentArticle.getResourcePrimKey());
			
			Set<Long> targetEntryIds = new HashSet<Long>();
			
			for(String targetArticleId : targetArticleIds ) {
				JournalArticle targetArticle = JournalArticleLocalServiceUtil.getArticle(groupId, targetArticleId);
				AssetEntry targetAssetEntry = AssetEntryLocalServiceUtil.getEntry("com.liferay.journal.model.JournalArticle", targetArticle.getResourcePrimKey());
				targetEntryIds.add(targetAssetEntry.getEntryId());
			}
			
			AssetLinkLocalServiceUtil.deleteLinks(currentAssetEntry.getEntryId());

			for(long targetEntryId : targetEntryIds) {
				if(targetEntryId != currentAssetEntry.getEntryId() ) {
					AssetLinkLocalServiceUtil.addLink(userId, targetEntryId,currentAssetEntry.getEntryId(), AssetLinkConstants.TYPE_RELATED, 0);
				}
			}
		} catch (PortalException e) {
			e.printStackTrace();
		}
		
	}

}

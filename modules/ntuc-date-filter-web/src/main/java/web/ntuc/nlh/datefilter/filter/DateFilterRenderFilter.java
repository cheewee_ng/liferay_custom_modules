package web.ntuc.nlh.datefilter.filter;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;
import javax.portlet.filter.RenderFilter;

import org.osgi.service.component.annotations.Component;

import api.ntuc.common.override.OverrideRenderRequestParam;
import api.ntuc.common.util.CSRFValidationUtil;
import api.ntuc.common.util.XSSValidationUtil;
import web.ntuc.nlh.datefilter.constants.DateFilterPortletKeys;

@Component(immediate = true, property = {
		"javax.portlet.name=" + DateFilterPortletKeys.DATEFILTER_PORTLET, }, service = PortletFilter.class)
public class DateFilterRenderFilter implements RenderFilter {

//	private Log log = LogFactoryUtil.getLog(SearchRenderFilter.class.getName());

	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain)
			throws IOException, PortletException {
//		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Map<String, Object> newParam = new HashMap<String, Object>();

		/*
		 * Check Permission for access page
		 */
		boolean isAuthorized = true;
		String renderName = ParamUtil.getString(request, "mvcRenderCommandName");
		newParam.put("isAuthorized", isAuthorized);

		/*
		 * CSRF Validation
		 */
		boolean validCSRF = false;
		if (Validator.isNull(renderName)) {
			// open module paramter for the 1st time no need to validate
			validCSRF = true;
		} else {
			String authToken = ParamUtil.getString(request, "authToken");
			if (CSRFValidationUtil.isValidRequest(request, authToken)) {
				validCSRF = true;
			}
		}
		newParam.put("validCSRF", validCSRF);

		/*
		 * XSS Validation
		 */
		boolean xssPass = true;
		Set<String> names = request.getRenderParameters().getNames();
		for (String name : names) {
			String param = request.getRenderParameters().getValue(name);
			if (!XSSValidationUtil.isValid(param)) {
				xssPass = false;
				break;
			}
		}
		newParam.put("xssPass", xssPass);

		OverrideRenderRequestParam over = new OverrideRenderRequestParam(request, newParam);
		chain.doFilter(over, response);
	}

}

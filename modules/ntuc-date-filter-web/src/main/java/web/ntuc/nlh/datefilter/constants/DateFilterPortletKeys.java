package web.ntuc.nlh.datefilter.constants;

/**
 * @author muhamadpangestu
 */
public class DateFilterPortletKeys {

	public static final String DATEFILTER_PORTLET = "DateFilter_Portlet";
	public static final String ASSET_VOCAB_TOPICS = "topic for search purposes";

}
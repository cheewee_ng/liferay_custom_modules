package web.ntuc.nlh.datefilter.engine;

import com.liferay.portal.kernel.theme.ThemeDisplay;

public abstract interface ThemeDisplaySupplier {

	public abstract ThemeDisplay getThemeDisplay();
}

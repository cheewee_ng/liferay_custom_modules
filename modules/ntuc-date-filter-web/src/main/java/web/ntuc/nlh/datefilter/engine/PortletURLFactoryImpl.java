package web.ntuc.nlh.datefilter.engine;

import com.liferay.portal.kernel.portlet.PortletURLUtil;

import javax.portlet.MimeResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

public class PortletURLFactoryImpl implements PortletURLFactory {
	private final MimeResponse _mimeResponse;
	private final PortletRequest _portletRequest;

	public PortletURLFactoryImpl(PortletRequest portletRequest, MimeResponse mimeResponse) {
		this._portletRequest = portletRequest;
		this._mimeResponse = mimeResponse;
	}

	public PortletURL getPortletURL() throws PortletException {
		PortletURL portletURL = PortletURLUtil.getCurrent(this._portletRequest, this._mimeResponse);

		return PortletURLUtil.clone(portletURL, this._mimeResponse);
	}
}
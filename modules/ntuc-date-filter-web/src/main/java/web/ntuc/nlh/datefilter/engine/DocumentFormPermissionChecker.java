package web.ntuc.nlh.datefilter.engine;

public abstract interface DocumentFormPermissionChecker {

	public abstract boolean hasPermission();
}

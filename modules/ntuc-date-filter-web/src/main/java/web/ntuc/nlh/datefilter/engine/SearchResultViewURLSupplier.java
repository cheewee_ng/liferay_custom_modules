package web.ntuc.nlh.datefilter.engine;

public abstract interface SearchResultViewURLSupplier {

	public abstract String getSearchResultViewURL();
	
}

package web.ntuc.nlh.datefilter.config;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "web.nlh.ntuc.datefilter.config.DateFilterConfig")
public interface DateFilterConfig {
	
	public static final String TOPICS = "topics";
	
	public static final String SUBTOPICS = "subTopics";
	
	@Meta.AD(required = false)
	public String topics();
	
	@Meta.AD(required = false)
	public String subTopics();
	
}

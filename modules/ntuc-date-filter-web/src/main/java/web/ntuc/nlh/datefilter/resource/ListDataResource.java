package web.ntuc.nlh.datefilter.resource;

import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.MimeResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import api.ntuc.nlh.content.util.ContentUtil;
import web.ntuc.nlh.datefilter.constants.DateFilterMessageKeys;
import web.ntuc.nlh.datefilter.constants.DateFilterPortletKeys;
import web.ntuc.nlh.datefilter.constants.MVCCommandNames;
import web.ntuc.nlh.datefilter.dto.TopicsDto;
import web.ntuc.nlh.datefilter.engine.PortletRequestThemeDisplaySupplier;
import web.ntuc.nlh.datefilter.engine.PortletURLFactory;
import web.ntuc.nlh.datefilter.engine.PortletURLFactoryImpl;
import web.ntuc.nlh.datefilter.engine.SearchPortletSearchResultPreferences;
import web.ntuc.nlh.datefilter.engine.SearchResultPreferences;
import web.ntuc.nlh.datefilter.engine.SearchResultSummaryDisplayBuilder;
import web.ntuc.nlh.datefilter.engine.SearchResultSummaryDisplayContext;
import web.ntuc.nlh.datefilter.engine.ThemeDisplaySupplier;

@Component(immediate = true, property = { "mvc.command.name=" + MVCCommandNames.LIST_TOPICS_RESOURCE,
		"javax.portlet.name=" + DateFilterPortletKeys.DATEFILTER_PORTLET }, service = MVCResourceCommand.class)
public class ListDataResource implements MVCResourceCommand {

	private static Log log = LogFactoryUtil.getLog(ListDataResource.class);

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException {
		log.info("List Topic Resources - Start");
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long groupId = themeDisplay.getScopeGroupId();
			log.info("groupID = " + groupId);

//			Filter Implementation and prepare resources
			boolean isAuthorized = ParamUtil.getBoolean(resourceRequest, "isAuthorized");
			boolean validCSRF = ParamUtil.getBoolean(resourceRequest, "validCSRF");
			boolean xssPass = ParamUtil.getBoolean(resourceRequest, "xssPass");

			long topics = ParamUtil.getLong(resourceRequest, "topicId");
			long subTopics = ParamUtil.getLong(resourceRequest, "subTopicId");
			
			List<TopicsDto> topicList = new ArrayList<TopicsDto>();

			log.info("topics : " + topics + " | subtopics : " + subTopics);

//			Authorization and validation check
			if (!isAuthorized || !validCSRF) {
				String msg = "isAuthorized : " + isAuthorized + " | validCSRF : " + validCSRF;
				throw new Exception(msg);
			}

			if (!xssPass) {
				PortletConfig portletConfig = (PortletConfig) resourceRequest
						.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
				String msg = LanguageUtil.format(portletConfig.getResourceBundle(themeDisplay.getLocale()),
						DateFilterMessageKeys.XSS_VALIDATION_NOT_PASS, xssPass);
				throw new Exception(msg);
			}
			
//			Getting Article Data
			HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
			
			List<Long> categoryIds = new ArrayList<Long>();
			
			if (Validator.isNotNull(subTopics) && !Validator.isBlank(String.valueOf(subTopics)) && subTopics > 0) {
				categoryIds.add(subTopics);
			} else {
				categoryIds.add(topics);
			}

//			log.info("categoryIds.size() = " + categoryIds.size());

//			Preparation Elastic Search
			SearchContext searchContext = SearchContextFactory.getInstance(request);

			searchContext.setAttribute("paginationType", "more");
//			searchContext.setStart(start);

			searchContext.getQueryConfig().setCollatedSpellCheckResultEnabled(true);
			searchContext.getQueryConfig().setCollatedSpellCheckResultScoresThreshold(200);
			searchContext.getQueryConfig().setQueryIndexingEnabled(false);
			searchContext.getQueryConfig().setQueryIndexingThreshold(50);
			searchContext.getQueryConfig().setQuerySuggestionEnabled(true);
			searchContext.getQueryConfig().setQuerySuggestionScoresThreshold(0);
			searchContext.getQueryConfig().setQuerySuggestionsMax(5);

//			Getting detail data from courses id
			List<SearchResultSummaryDisplayContext> resultCourse = this.buildResultSummary(categoryIds, searchContext,
					themeDisplay, request, resourceRequest, resourceResponse);
			
			for (SearchResultSummaryDisplayContext res : resultCourse) {
				TopicsDto topic = new TopicsDto();
				
				topic.setTitle(res.getHighlightedTitle());
				topic.setDesc(res.getContent());
				
				if (res.getClassName().equals(JournalArticle.class.getName())) {
					JournalArticle article = (JournalArticle) res.getAssetObject();
					
					try {
						String xmlContent = article.getContent();
//						log.info(xmlContent);
						String imageUrl = ContentUtil.getImageUrl(xmlContent, 1, "image", themeDisplay);
						String status = ContentUtil.getWebContentVal(xmlContent, 1, "category", themeDisplay);
						Date date = article.getCreateDate();
						String dateFormat = "dd MMM yyyy hh:mm:ss aaa";
						SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
						String createDate = sdf.format(date);
						
						topic.setDate(createDate);
						//log.info(createDate);
						topic.setStatus(status);
						topic.setUrlImage(imageUrl);
					} catch (Exception e) {
						topic.setUrlImage("");
					}
				}
				
				// add data when viewURL not null
				if (Validator.isNull(topic.getUrlMore())) {
					if (res.isAssetRendererURLDownloadVisible()) {
						topic.setUrlMore(res.getAssetRendererURLDownload());
					} else {
						if (Validator.isNotNull(res.getViewURL())) {
							topic.setUrlMore(res.getViewURL());
						}
					}
				}

				topicList.add(topic);
			}
			
//			log.info(topicList.toString());

			resourceResponse.setContentType("application/json");
			PrintWriter out = null;
			out = resourceResponse.getWriter();

			JSONObject rowData = JSONFactoryUtil.createJSONObject();
			rowData.put("topicList", topicList);
			rowData.put("topicId", topics);
			rowData.put("subTopicId", subTopics);
			rowData.put("status", HttpServletResponse.SC_OK);
			out.print(rowData.toString());
			out.flush();
		} catch (Exception e) {
			log.error("Error while searching result data : " + e.getMessage());
			return true;
		}
		log.info("search result data resources - end");
		return false;
	}

	@SuppressWarnings("unchecked")
	private List<SearchResultSummaryDisplayContext> buildResultSummary(List<Long> categoryIds, SearchContext searchContext,
			ThemeDisplay themeDisplay, HttpServletRequest request, ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws Exception {
		Indexer<JournalArticle> indexer = IndexerRegistryUtil.getIndexer(JournalArticle.class);

		BooleanQuery booleanQuery = new BooleanQueryImpl();

		if (categoryIds.size() > 0) {
			for (Long id : categoryIds) {
				booleanQuery.addExactTerm("assetCategoryIds", id);
//				log.info("id = " + id);
			}
		}

		BooleanClause<Query> booleanClause = BooleanClauseFactoryUtil.create(booleanQuery,
				BooleanClauseOccur.MUST.getName());
		searchContext.setBooleanClauses(new BooleanClause[] { booleanClause });

		Hits hits = indexer.search(searchContext);

		log.info("hits.getLength() = " + hits.getLength());
		List<SearchResultSummaryDisplayContext> results = new ArrayList<SearchResultSummaryDisplayContext>();

		List<Document> documents = hits.toList();
		for (Document document : documents) {
			SearchResultSummaryDisplayBuilder searchResultSummaryDisplayBuilder = new SearchResultSummaryDisplayBuilder();
			searchResultSummaryDisplayBuilder.setAssetEntryLocalService(AssetEntryLocalServiceUtil.getService());
			searchResultSummaryDisplayBuilder.setCurrentURL(request.getRequestURI());
			searchResultSummaryDisplayBuilder.setDocument(document);
			searchResultSummaryDisplayBuilder.setHighlightEnabled(true);
			searchResultSummaryDisplayBuilder.setLanguage(LanguageUtil.getLanguage());
			searchResultSummaryDisplayBuilder.setLocale(request.getLocale());
			MimeResponse mimeResponse = (MimeResponse) resourceResponse;
			PortletURLFactory portletURLFactory = new PortletURLFactoryImpl(resourceRequest, mimeResponse);
			searchResultSummaryDisplayBuilder.setPortletURLFactory(portletURLFactory);
			searchResultSummaryDisplayBuilder.setQueryTerms(hits.getQueryTerms());
			searchResultSummaryDisplayBuilder.setResourceRequest(resourceRequest);
			searchResultSummaryDisplayBuilder.setResourceResponse(resourceResponse);
			searchResultSummaryDisplayBuilder.setRequest(request);
			searchResultSummaryDisplayBuilder.setResourceActions(ResourceActionsUtil.getResourceActions());
			ThemeDisplaySupplier themeDisplaySupplier = new PortletRequestThemeDisplaySupplier(resourceRequest);
			SearchResultPreferences searchResultPreferences = new SearchPortletSearchResultPreferences(
					resourceRequest.getPreferences(), themeDisplaySupplier);
			searchResultSummaryDisplayBuilder.setSearchResultPreferences(searchResultPreferences);
			searchResultSummaryDisplayBuilder.setThemeDisplay(themeDisplay);

			SearchResultSummaryDisplayContext searchResultSummaryDisplayContext = searchResultSummaryDisplayBuilder
					.build();

			results.add(searchResultSummaryDisplayContext);

//			log.info("Document : " + document);
		}

		return results;
	}

	@Reference
	AssetVocabularyLocalService _assetVocabularyLocalService;

	@Reference
	AssetCategoryLocalService _assetCategoryLocalService;

	@Reference
	CompanyLocalService _companyLocalService;

}

package web.ntuc.login.constants;

/**
 * @author fandifadillah
 */
public class LoginPortletKeys {

	public static final String FAST_LOGIN = "com_liferay_login_web_portlet_FastLoginPortlet";

	public static final String LOGIN = "com_liferay_login_web_portlet_LoginPortlet";

}
<%@page import="web.ntuc.nlh.mediafilter.constants.MVCCommandNames"%>
<%@ include file="init.jsp"%>

<portlet:resourceURL id="<%=MVCCommandNames.LIST_TOPICS_RESOURCE%>"
	var="dataResourceURL">
	<portlet:param name="authToken" value="${authToken}" />
	<portlet:param name="topicId" value="${topicId}" />

</portlet:resourceURL>
<div class="container sp-main-bot sp-main-top">
	<div class="title-wrap-2 fly-box-date">
		<div class="row align-items-center">
			<div class="col-md-12 order-md-12 mb-3 text-right">
				<div class="calendar-view">
					<div class="checkbox">
						<input type="checkbox" id="calendarview" name="calendarview"
							value="1"> <label for="calendarview">Calendar
							view?</label>
					</div>
					<div class="row sp-row-1 yearmonth">
						<div class="col-5 bcol">
							<select name="filter_year" id="search-blog-year"
								onchange="filterTopics()" tabindex="-98">
								<option value="0">Year</option>
								<c:forEach items="${years}" var="year">
									<option value="${year}">${year}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-7 bcol">
							<select name="filter_month" id="search-blog-month"
								onchange="filterTopics()" tabindex="-98">
								<option value="0">All Months</option>
								<option value="1">January</option>
								<option value="2">February</option>
								<option value="3">March</option>
								<option value="4">April</option>
								<option value="5">May</option>
								<option value="6">June</option>
								<option value="7">July</option>
								<option value="8">August</option>
								<option value="9">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>
						</div>
					</div>
					<div class="date-wrap" style="display: none;">
						<input name="specifieddate" id="specifieddate"
							onblur="filterByDate()" type="text" value=""
							class="form-control nogetdate"> <i
							class="fas fa-calendar-alt"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-12">
			<div class="no-data">
				<p>No records found</p>
			</div>
			<div class="row sp-row-1" id="topic-list">
				<div class="loader"></div>
				<!-- INSERTED BY JS -->
			</div>
		</div>
	</div>
</div>


<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script type="text/javascript">
	
	var dataList = "";
	
	$(document).ready(function() {
		initData("<%=dataResourceURL.toString()%>");	
	});

	function initData(url) {
		return jQuery.ajax({
			url : url,
			type : "GET",
			dataType : "json",
			async : false,
			cache : false,
			beforeSend: function () { // Before we send the request, remove the .hidden class from the spinner and default to inline-block.
                $('.loader').removeClass('hidden');
                $('.no-data').hide();
            },
			success : function(data, status, XMLHttpRequest) {
				console.log(data.topicList);
				var htmlArticles = '';
				
				if(data.topicList.length > 0){
					data.topicList.sort(sortDate);
					data.topicList.forEach(function(topic){
						htmlArticles += generateTopicList(topic);
					});
				}
				
				$('#topic-list').html(htmlArticles);
				
				dataList = data.topicList;
			},
			complete: function () { // Set our complete callback, adding the .hidden class and hiding the spinner.
            	$('.loader').hide();
            },
			error: function(data, status, XMLHttpRequest) {
                //return null;
            }
		});
	}
	
	function filterTopics(){
		var htmlArticles = '';
		var topicFilter = dataList;
		
		var filterYear = parseInt($('#search-blog-year option:selected').val());
		var filterMonth = parseInt($('#search-blog-month option:selected').val());
		
		topicFilter = topicFilter.filter(topic => {
			var date = new Date(topic.date);
			var topicYear = date.getFullYear();
			var topicMonth = date.getMonth() + 1;
			
			if (filterYear != 0 && filterMonth != 0){
				return filterYear == topicYear && filterMonth == topicMonth;
			}else if(filterYear != 0 || filterMonth != 0){
				return filterYear == topicYear || filterMonth == topicMonth;
			}else if(filterYear == 0 && filterMonth == 0){
				return topicFilter = dataList;
			}
		});
		
		console.log(topicFilter);
		
		if (topicFilter.length > 0) {
			$('.no-data').hide();
			topicFilter.forEach(function(topic) {
				htmlArticles += generateTopicList(topic);
            });
        }else{
        	$('.no-data').show();
        }

        $('#topic-list').html(htmlArticles);
	}
	
	function filterByDate(){
		//alert("ok");
		var topicFilter = dataList;
		var htmlArticles = '';
		
		var filterDate = document.getElementById("specifieddate").value;
		console.log(filterDate);
		
		topicFilter = topicFilter.filter(topic => {
			var topicDate = getFormattedDate(topic.date);
			
			if (filterDate != ""){
				return filterDate == topicDate;
			}else{
				return topicFilter = dataList;
			}
		});
		
		console.log(topicFilter);
		
		if (topicFilter.length > 0) {
			$('.no-data').hide();
			topicFilter.forEach(function(topic) {
				htmlArticles += generateTopicList(topic);
            });
        }else{
        	$('.no-data').show();
        }

        $('#topic-list').html(htmlArticles);
	}
	
	function generateTopicList(data){
		var tempDate = getFormattedDate(data.date);
		var html = '<div class="grid-8">';
		html += '<h2>' + data.title + '</h2>';
		html += '<div class="info">';
		html += '<span class="far fa-calendar-alt">' + tempDate + '</span>';	
		html += '</div>';	
		html += '<p>' + data.desc + '</p>';	
		html += '<span class="more">Read more</span>';	
		var url = ''+data.urlMore;
		html += '<a class="fxlink" href="' + url.split('?')[0] + '">View details</a>';	
		html += '</div>';
		
		return html;
	}
	
	function getFormattedDate(convertDate){
		const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var d = new Date(convertDate);
		var month = months[d.getMonth()];
		var day = '' + d.getDate();
		if(day.length < 2){
			day = '0' + day;
		}
		var year = d.getFullYear();
		
		return day + " " + month + " " + year;
	}
	
	function sortDate(a, b){
		var dateA = new Date(a.date), dateB = new Date(b.date);
		return dateB - dateA;
	}
	

</script>
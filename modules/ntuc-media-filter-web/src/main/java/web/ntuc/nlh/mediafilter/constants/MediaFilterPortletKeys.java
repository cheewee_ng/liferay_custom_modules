package web.ntuc.nlh.mediafilter.constants;

/**
 * @author muhamadpangestu
 */
public class MediaFilterPortletKeys {

	public static final String MEDIAFILTER_PORTLET = "MediaFilter_Portlet";
	public static final String ASSET_VOCAB_TOPICS = "topic for search purposes";

}
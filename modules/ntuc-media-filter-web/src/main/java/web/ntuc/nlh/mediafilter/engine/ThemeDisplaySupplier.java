package web.ntuc.nlh.mediafilter.engine;

import com.liferay.portal.kernel.theme.ThemeDisplay;

public abstract interface ThemeDisplaySupplier {

	public abstract ThemeDisplay getThemeDisplay();
}

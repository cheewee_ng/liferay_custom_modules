package web.ntuc.nlh.mediafilter.engine;

import com.liferay.asset.kernel.model.AssetRendererFactory;

public abstract interface AssetRendererFactoryLookup {

	public abstract AssetRendererFactory<?> getAssetRendererFactoryByClassName(String paramString);

}

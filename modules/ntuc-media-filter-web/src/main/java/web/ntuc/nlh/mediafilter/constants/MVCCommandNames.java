package web.ntuc.nlh.mediafilter.constants;

public class MVCCommandNames {
	
	public static final String TOPIC_ACTION = "topicAction";
	public static final String LIST_TOPICS_RESOURCE = "listTopicsResource";
	public static final String TOPIC_DATA_RESOURCES = "topicDataResources";

}

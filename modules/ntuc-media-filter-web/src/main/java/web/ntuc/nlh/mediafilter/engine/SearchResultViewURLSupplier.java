package web.ntuc.nlh.mediafilter.engine;

public abstract interface SearchResultViewURLSupplier {

	public abstract String getSearchResultViewURL();
	
}

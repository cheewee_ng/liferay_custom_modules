package web.ntuc.nlh.mediafilter.engine;

public abstract interface DocumentFormPermissionChecker {

	public abstract boolean hasPermission();
}

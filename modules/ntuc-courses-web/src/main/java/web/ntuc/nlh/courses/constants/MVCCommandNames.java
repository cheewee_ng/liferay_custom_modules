package web.ntuc.nlh.courses.constants;

public class MVCCommandNames {
	
	public static final String COURSES_ACTION = "coursesAction";
	public static final String LIST_COURSES_RESOURCE = "listCoursesResource";
	public static final String COURSES_RESULT_RENDER = "coursesResultRender";
	public static final String TOPIC_DATA_RESOURCES = "topicDataResources";
	public static final String THEME_DATA_RESOURCES = "themeDataResources";

}

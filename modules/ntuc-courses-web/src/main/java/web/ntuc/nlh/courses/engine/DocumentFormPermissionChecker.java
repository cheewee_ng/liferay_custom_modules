package web.ntuc.nlh.courses.engine;

public abstract interface DocumentFormPermissionChecker {

	public abstract boolean hasPermission();
}

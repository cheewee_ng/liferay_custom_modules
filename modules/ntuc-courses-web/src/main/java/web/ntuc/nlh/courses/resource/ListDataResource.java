package web.ntuc.nlh.courses.resource;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.search.BooleanClause;
import com.liferay.portal.kernel.search.BooleanClauseFactoryUtil;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.Query;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.security.permission.ResourceActionsUtil;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;

import javax.portlet.MimeResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import api.ntuc.nlh.content.util.ContentUtil;
import web.ntuc.nlh.courses.constants.CoursesMessageKeys;
import web.ntuc.nlh.courses.constants.CoursesPortletKeys;
import web.ntuc.nlh.courses.constants.MVCCommandNames;
import web.ntuc.nlh.courses.dto.CoursesDto;
import web.ntuc.nlh.courses.dto.FilterDto;
import web.ntuc.nlh.courses.engine.PortletRequestThemeDisplaySupplier;
import web.ntuc.nlh.courses.engine.PortletURLFactory;
import web.ntuc.nlh.courses.engine.PortletURLFactoryImpl;
import web.ntuc.nlh.courses.engine.SearchPortletSearchResultPreferences;
import web.ntuc.nlh.courses.engine.SearchResultPreferences;
import web.ntuc.nlh.courses.engine.SearchResultSummaryDisplayBuilder;
import web.ntuc.nlh.courses.engine.SearchResultSummaryDisplayContext;
import web.ntuc.nlh.courses.engine.ThemeDisplaySupplier;

@Component(immediate = true, property = { "mvc.command.name=" + MVCCommandNames.LIST_COURSES_RESOURCE,
		"javax.portlet.name=" + CoursesPortletKeys.COURSES_PORTLET }, service = MVCResourceCommand.class)
public class ListDataResource implements MVCResourceCommand {

	private static Log log = LogFactoryUtil.getLog(ListDataResource.class);

	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException {
		log.info("List Courses Resources - Start");
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long groupId = themeDisplay.getScopeGroupId();
			log.info("groupID = " + groupId);
//			Filter Implementation and prepare resources
			boolean isAuthorized = ParamUtil.getBoolean(resourceRequest, "isAuthorized");
			boolean validCSRF = ParamUtil.getBoolean(resourceRequest, "validCSRF");
			boolean xssPass = ParamUtil.getBoolean(resourceRequest, "xssPass");

			long topic = ParamUtil.getLong(resourceRequest, "topic");
			long theme = ParamUtil.getLong(resourceRequest, "theme");
			
//			log.info("theme ID: " + theme);

			if (!isAuthorized || !validCSRF) {
				String msg = "isAuthorized : " + isAuthorized + " | validCSRF : " + validCSRF;
				throw new Exception(msg);
			}

			if (!xssPass) {
				PortletConfig portletConfig = (PortletConfig) resourceRequest
						.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
				String msg = LanguageUtil.format(portletConfig.getResourceBundle(themeDisplay.getLocale()),
						CoursesMessageKeys.XSS_VALIDATION_NOT_PASS, xssPass);
				throw new Exception(msg);
			}
			HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);

//			Adding Courses Id
			List<CoursesDto> courses = new ArrayList<CoursesDto>();
			List<Long> categoryIds = new ArrayList<Long>();

			if (Validator.isNotNull(topic) && !Validator.isBlank(String.valueOf(topic)) && topic > 0) {
				categoryIds.add(topic);
			} else {
				categoryIds.add(theme);
			}

//			log.info("categoryIds.size() = " + categoryIds.size());

//			Preparation Elastic Search
			SearchContext searchContext = SearchContextFactory.getInstance(request);

			searchContext.setAttribute("paginationType", "more");

			searchContext.getQueryConfig().setCollatedSpellCheckResultEnabled(true);
			searchContext.getQueryConfig().setCollatedSpellCheckResultScoresThreshold(200);
			searchContext.getQueryConfig().setQueryIndexingEnabled(false);
			searchContext.getQueryConfig().setQueryIndexingThreshold(50);
			searchContext.getQueryConfig().setQuerySuggestionEnabled(true);
			searchContext.getQueryConfig().setQuerySuggestionScoresThreshold(0);
			searchContext.getQueryConfig().setQuerySuggestionsMax(5);

//			Getting detail data from courses id
			List<SearchResultSummaryDisplayContext> resultCourse = this.buildResultSummary(categoryIds, searchContext,
					themeDisplay, request, resourceRequest, resourceResponse);

//			Mapping Price List to Range List
			int[] priceList = { 1, 50, 100, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500,
					7000, 7500 };
			List<FilterDto> priceRangeList = new ArrayList<FilterDto>();
			List<FilterDto> fundedOptionList = new ArrayList<FilterDto>();

			for (int i = 0; i <= priceList.length; i++) {
				if (i == priceList.length - 1) {
					break;
				} else {
					FilterDto dto = new FilterDto();
					String temp = priceList[i] + "-" + priceList[i + 1];
					Long id = new Long(i);
					dto.setFilterId(id);
					dto.setFilterName(temp);
					dto.setCategory("price");
					priceRangeList.add(dto);
				}
			}
//			log.info(priceRange.toString());

			String[] fundedList = { "Funded", "Not Funded" };
			for (int i = 0; i < fundedList.length; i++) {
				FilterDto dto = new FilterDto();
				Long id = new Long(i);
				dto.setFilterId(id);
				dto.setFilterName(fundedList[i]);
				dto.setCategory("funded");
				fundedOptionList.add(dto);
			}

			List<AssetCategory> topicList = AssetCategoryLocalServiceUtil.getCategories();
			List<FilterDto> filterTheme = new ArrayList<FilterDto>();
			List<FilterDto> filterTopic = new ArrayList<FilterDto>();
//			log.info(topicList.toString());			

			for (SearchResultSummaryDisplayContext res : resultCourse) {
//				log.info(res.toString());
//				filterTopic = new ArrayList<FilterDto>();
//				filterTheme = new ArrayList<FilterDto>();
				List<Long> topicId = new ArrayList<Long>();
				List<Long> themeId = new ArrayList<Long>();
				Long[] topicCategories = {};
				Long[] themeCategories = {};

				// Get all categories Id from summary to array

				CoursesDto v = new CoursesDto();
//				log.info(Arrays.toString(res.getCategoryIds()));

				for (long a : res.getCategoryIds()) {
//					log.info("category ids = "+a);
					AssetCategory ac = AssetCategoryLocalServiceUtil.getCategory(a);
//					log.info(ac.toString());
//					log.info("Content Group Id : " + ac.getGroupId());
					for (AssetCategory assetCat : topicList) {
						if (assetCat.getCategoryId() == ac.getCategoryId() && ac.getGroupId() == groupId) {
							if (!topicId.contains(a) && ac.getVocabularyId() != getCoursesVocabularyId(theme)) {
								topicId.add(ac.getCategoryId());
//								topicCategories = topicId.toArray(topicCategories);
							}

							if (!themeId.contains(a) && ac.getVocabularyId() == getCoursesVocabularyId(theme)) {
								themeId.add(ac.getCategoryId());
//								themeCategories = themeId.toArray(themeCategories);
							}
						}
					}
				}
				v.setTopicIds(topicId.toArray(topicCategories));
				v.setThemeIds(themeId.toArray(themeCategories));
//				log.info("Topic Id List : " + Arrays.toString(v.getTopicIds()));
//				log.info("Theme Id List : " + Arrays.toString(v.getThemeIds()));
				
//				v.setStructure("product");
				v.setTitle(res.getHighlightedTitle());
				v.setDesc(res.getContent());
				if (res.getClassName().equals(JournalArticle.class.getName())) {
					JournalArticle journalArticle = (JournalArticle) res.getAssetObject();
//					log.info(journalArticle.getDisplayDate());
					try {
						String xmlContent = journalArticle.getContent();
//						log.info(xmlContent.toString());
						String imageUrl = ContentUtil.getImageUrl(xmlContent, 1, "image", themeDisplay);
//						String detailUrl = ContentUtil.getLinkUrl(xmlContent, 1, "URLDetail", themeDisplay);
						String status = ContentUtil.getWebContentVal(xmlContent, 1, "status", themeDisplay);
						String price = ContentUtil.getWebContentVal(xmlContent, 1, "price", themeDisplay);
						String funded = ContentUtil.getWebContentVal(xmlContent, 1, "funded", themeDisplay);
						String popular = ContentUtil.getWebContentVal(xmlContent, 1, "popular", themeDisplay);

						v.setStatus(status);
						v.setUrlImage(imageUrl);
						
						if (price.equals("")) {
							v.setPrice(0.0);
						}else {
							v.setPrice(Double.valueOf(price));
						}
						
						if (funded.equals("true")) {
							v.setFunded("FUNDED");
							v.setStatus("Before Funding");
						}else {
							v.setFunded("NOT FUNDED");
						}

						if (popular.equals("true")) {
							v.setPopular("POPULAR");
						} 

//						v.setUrlMore(detailUrl);
					} catch (Exception e) {
						v.setUrlImage("");
					}
				}

				// add data when viewURL not null
				if (Validator.isNull(v.getUrlMore())) {
					if (res.isAssetRendererURLDownloadVisible()) {
						v.setUrlMore(res.getAssetRendererURLDownload());
					} else {
						if (Validator.isNotNull(res.getViewURL())) {
							v.setUrlMore(res.getViewURL());
						}
					}
				}

//				Mapping topic and theme list filter
				for (Long topicCategoryId : v.getTopicIds()) {
					FilterDto dto = new FilterDto();
					AssetCategory temp = _assetCategoryLocalService.getCategory(topicCategoryId);
					dto.setFilterId(temp.getCategoryId());
					dto.setFilterName(temp.getName());
					dto.setCategory("topicIds");
//					log.info("Dto : " + dto.toString());

					if (!filterTopic.contains(dto)) {
						filterTopic.add(dto);
//						log.info("Topic : " + filterTopic.toString());

					}
				}

				for (Long themeCategoryId : v.getThemeIds()) {
					FilterDto dto = new FilterDto();
					AssetCategory temp = _assetCategoryLocalService.getCategory(themeCategoryId);
					dto.setFilterId(temp.getCategoryId());
					dto.setFilterName(temp.getName());
					dto.setCategory("themeIds");
//					log.info("Dto : " + dto.toString());

					if (!filterTheme.contains(dto)) {
						filterTheme.add(dto);
//						log.info("Theme : " + filterTheme.toString());
					}
				}

//				log.info(topicId.toString());
//				log.info(themeId.toString());

//				log.info("Filter Topic : " + filterTopic.toString());
//				log.info("Filter Theme : " + filterTheme.toString());

//				log.info(v.toString());
				courses.add(v);
			}


//			log.info("Total Filter : " + filterTheme.size());
//			log.info("Total Filter : " + filterTopic.size());
//
//			for (FilterDto filterDto : filterTheme) {
//				log.info(filterDto.getFilterId());
//				log.info(filterDto.getFilterName());
			
			List<FilterDto> newTopicList = new ArrayList<FilterDto>();
			List<FilterDto> newThemeList = new ArrayList<FilterDto>();
			
			HashSet<Long> removeDuplicateTopicId = new HashSet<Long>();
			filterTopic.removeIf(e->!removeDuplicateTopicId.add(e.getFilterId()));
			
			HashSet<Long> removeDuplicateThemeId = new HashSet<Long>();
			filterTheme.removeIf(e->!removeDuplicateThemeId.add(e.getFilterId()));
			
			for (Long topicId : removeDuplicateTopicId) {
				FilterDto dto = new FilterDto();
				AssetCategory ac = _assetCategoryLocalService.getCategory(topicId);
				dto.setFilterId(ac.getCategoryId());
				dto.setFilterName(ac.getName());
				dto.setCategory("topicIds");
				newTopicList.add(dto);
			}
			
			for (Long themeId : removeDuplicateThemeId) {
				FilterDto dto = new FilterDto();
				AssetCategory ac = _assetCategoryLocalService.getCategory(themeId);
				dto.setFilterId(ac.getCategoryId());
				dto.setFilterName(ac.getName());
				dto.setCategory("themeIds");
				newThemeList.add(dto);
				
			}
			
//			log.info("New topic List : " + newTopicList);
//			log.info("New theme List : " + newThemeList);
			
			CoursesDto maxCourse = courses.stream().max(Comparator.comparing(CoursesDto::getPrice)).orElseThrow(NoSuchElementException::new);
			
			resourceResponse.setContentType("application/json");
			PrintWriter out = null;
			out = resourceResponse.getWriter();

			JSONObject rowData = JSONFactoryUtil.createJSONObject();
			rowData.put("filterTheme", newThemeList);
			rowData.put("filterTopic", newTopicList);
			rowData.put("priceList", priceRangeList);
			rowData.put("fundedList", fundedOptionList);
			rowData.put("courses", courses);
			rowData.put("maxPrice", maxCourse.getPrice());
			rowData.put("status", HttpServletResponse.SC_OK);
			out.print(rowData.toString());
			out.flush();
		} catch (Exception e) {
			log.error("Error while searching result data : " + e.getMessage());
			return true;
		}
		log.info("search result data resources - end");
		return false;
	}

	@SuppressWarnings("unchecked")
	private List<SearchResultSummaryDisplayContext> buildResultSummary(List<Long> categoryIds,
			SearchContext searchContext, ThemeDisplay themeDisplay, HttpServletRequest request,
			ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws Exception {
		Indexer<JournalArticle> indexer = IndexerRegistryUtil.getIndexer(JournalArticle.class);

		BooleanQuery booleanQuery = new BooleanQueryImpl();
//		DDMStructure productStructure = ContentUtil.getStructure(themeDisplay.getScopeGroupId(), structure, false);
//		if(Validator.isNotNull(theme)) {
//			booleanQuery.addExactTerm("assetCategoryIds", theme);
//		}
//		if(Validator.isNotNull(topic)) {
//			booleanQuery.addExactTerm("assetCategoryIds", topic);
//		}

		if (categoryIds.size() > 0) {
			for (Long id : categoryIds) {
				booleanQuery.addExactTerm("assetCategoryIds", id);
				//log.info("id = " + id);
			}
		}

		BooleanClause<Query> booleanClause = BooleanClauseFactoryUtil.create(booleanQuery,
				BooleanClauseOccur.MUST.getName());
		searchContext.setBooleanClauses(new BooleanClause[] { booleanClause });

//		final MultiValueFacet fieldFacet = new MultiValueFacet(searchContext);
//        fieldFacet.setFieldName("roleId");
//        FacetConfiguration config = fieldFacet.getFacetConfiguration();
//        JSONObject obj = JSONFactoryUtil.createJSONObject();
//        obj.put("maxTerms", 500);
//        config.setDataJSONObject(obj);
//        fieldFacet.setFacetConfiguration(config);
//        searchContext.addFacet(fieldFacet);

		Hits hits = indexer.search(searchContext);

//		final List<String> terms = new ArrayList<>();
//        for (final TermCollector collector : fieldFacet.getFacetCollector().getTermCollectors()) {
//            terms.add(collector.getTerm());
//            log.info("Collector : " + collector.getTerm());
//        }
//        
//        log.info("Term : " + terms.size());
//		log.info("hits.getQuery() : " + hits.getQuery());
//		log.info("hits.getLength() = " + hits.getLength());
		List<SearchResultSummaryDisplayContext> results = new ArrayList<SearchResultSummaryDisplayContext>();

		List<Document> documents = hits.toList();
		for (Document document : documents) {
//			log.info("assetCategoryIds = " + document.get(Field.ASSET_CATEGORY_IDS));
//			log.info("roleId = " + document.get("roleId"));
//			log.info("title = " + document.get("title_en_US"));
			SearchResultSummaryDisplayBuilder searchResultSummaryDisplayBuilder = new SearchResultSummaryDisplayBuilder();
			searchResultSummaryDisplayBuilder.setAssetEntryLocalService(AssetEntryLocalServiceUtil.getService());
			searchResultSummaryDisplayBuilder.setCurrentURL(request.getRequestURI());
			searchResultSummaryDisplayBuilder.setDocument(document);
			searchResultSummaryDisplayBuilder.setHighlightEnabled(true);
			searchResultSummaryDisplayBuilder.setLanguage(LanguageUtil.getLanguage());
			searchResultSummaryDisplayBuilder.setLocale(request.getLocale());
			MimeResponse mimeResponse = (MimeResponse) resourceResponse;
			PortletURLFactory portletURLFactory = new PortletURLFactoryImpl(resourceRequest, mimeResponse);
			searchResultSummaryDisplayBuilder.setPortletURLFactory(portletURLFactory);
			searchResultSummaryDisplayBuilder.setQueryTerms(hits.getQueryTerms());
			searchResultSummaryDisplayBuilder.setResourceRequest(resourceRequest);
			searchResultSummaryDisplayBuilder.setResourceResponse(resourceResponse);
			searchResultSummaryDisplayBuilder.setRequest(request);
			searchResultSummaryDisplayBuilder.setResourceActions(ResourceActionsUtil.getResourceActions());
			ThemeDisplaySupplier themeDisplaySupplier = new PortletRequestThemeDisplaySupplier(resourceRequest);
			SearchResultPreferences searchResultPreferences = new SearchPortletSearchResultPreferences(
					resourceRequest.getPreferences(), themeDisplaySupplier);
			searchResultSummaryDisplayBuilder.setSearchResultPreferences(searchResultPreferences);
			searchResultSummaryDisplayBuilder.setThemeDisplay(themeDisplay);

			SearchResultSummaryDisplayContext searchResultSummaryDisplayContext = searchResultSummaryDisplayBuilder
					.build();

			results.add(searchResultSummaryDisplayContext);

//			log.info("Document : " + document);
		}

		return results;
	}

	public Long getCoursesVocabularyId(Long categoryId) {
		AssetCategory tempCategory = null;
		Long vocabularyId = null;
		try {
			tempCategory = _assetCategoryLocalService.getCategory(categoryId);
			vocabularyId = tempCategory.getVocabularyId();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vocabularyId;
	}

	@Reference
	AssetVocabularyLocalService _assetVocabularyLocalService;

	@Reference
	AssetCategoryLocalService _assetCategoryLocalService;

	@Reference
	CompanyLocalService _companyLocalService;

}

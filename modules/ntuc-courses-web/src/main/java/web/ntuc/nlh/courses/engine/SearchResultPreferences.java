package web.ntuc.nlh.courses.engine;

public abstract interface SearchResultPreferences {

	public abstract boolean isDisplayResultsInDocumentForm();

	public abstract boolean isHighlightEnabled();

	public abstract boolean isViewInContext();

}

package web.ntuc.nlh.courses.engine;

public abstract interface SearchResultViewURLSupplier {

	public abstract String getSearchResultViewURL();
	
}

package web.ntuc.nlh.courses.portlet;

import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetVocabularyLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.persistence.GroupPersistence;
import com.liferay.portal.kernel.service.persistence.GroupUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import api.ntuc.common.util.CSRFValidationUtil;
import web.ntuc.nlh.courses.config.CoursesConfig;
import web.ntuc.nlh.courses.constants.CoursesPortletKeys;
import web.ntuc.nlh.courses.dto.FilterDto;

/**
 * @author muhamadpangestu
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=ntuc-module",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Courses", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp", "javax.portlet.name=" + CoursesPortletKeys.COURSES_PORTLET,
		"javax.portlet.resource-bundle=content.Language", "javax.portlet.version=3.0",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class CoursesPortlet extends MVCPortlet {

	private static Log log = LogFactoryUtil.getLog(CoursesPortlet.class);
	List<Long> categoryId = new ArrayList<Long>();
	List<FilterDto> filterDto = new ArrayList<FilterDto>();

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		log.info("Courses Portlet render - start");
		try {

			String authToken = CSRFValidationUtil.authToken(renderRequest);
			renderRequest.setAttribute("authToken", authToken);

			Long themeId = Long.valueOf(renderRequest.getPreferences().getValue(CoursesConfig.THEMES, "0"));
			Long topicId = Long.valueOf(renderRequest.getPreferences().getValue(CoursesConfig.TOPICS, "0"));

			String theme = String.valueOf(themeId);
			String topic = String.valueOf(topicId);

			log.info("theme = " + theme + " topic = " + topic);

			renderRequest.setAttribute("dataTheme", theme);
			renderRequest.setAttribute("dataTopic", topic);

			String datathemeName = "";
			String dataTopicName = "";
			if (!Validator.isBlank(theme) && themeId > 0 && topicId == 0) {
				AssetCategory themeAsset = _assetCategoryLocalService.getAssetCategory(Long.valueOf(theme));
				datathemeName = themeAsset.getName();
			}
			if (!Validator.isBlank(topic) && topicId > 0) {
				AssetCategory topicAsset = _assetCategoryLocalService.getAssetCategory(Long.valueOf(topic));
				datathemeName = topicAsset.getName();
			}

			log.info("dataThemeName = " + datathemeName + " dataTopicName = " + dataTopicName);
			renderRequest.setAttribute("dataThemeName", datathemeName);
			renderRequest.setAttribute("dataTopicName", dataTopicName);

		} catch (

		Exception e) {
			log.error("Failed when render Courses, error:" + e.getMessage());
		}
		log.info("Courses portlet render - end");
		super.render(renderRequest, renderResponse);
	}

	@Reference
	AssetCategoryLocalService _assetCategoryLocalService;
}
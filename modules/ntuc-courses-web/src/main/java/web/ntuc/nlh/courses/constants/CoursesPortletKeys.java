package web.ntuc.nlh.courses.constants;

/**
 * @author muhamadpangestu
 */
public class CoursesPortletKeys {

	public static final String COURSES_PORTLET = "Courses_Portlet";
	public static final String COURSES_DISPLAY = "Search Course";
	public static final String COURSES_THEME = "course_theme";
	public static final String PARAMETER_THEME = "theme";
	public static final String PARAMETER_TOPIC = "topic";
	public static final String ASSET_VOCAB_THEME = "course theme";
	public static final String ASSET_VOCAB_TOPIC = "course topic";
	public static final String TOPIC_VOCABULARY_NAME = "topic";
	public static final String COURSE_CATEGORY_NAME = "courses";
	public static final String BLOGS_CATEGORY_NAME = "blogs";
	public static final String PAGES_CATEGORY_NAME = "pages";
	public static final String PRESS_CATEGORY_NAME = "press releases";
	public static final String NEWS_CATEGORY_NAME = "news";

}
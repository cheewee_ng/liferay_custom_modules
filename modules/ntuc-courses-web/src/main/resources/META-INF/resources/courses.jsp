<%@page import="web.ntuc.nlh.courses.constants.MVCCommandNames"%>
<%@ include file="init.jsp"%>

<portlet:resourceURL id="<%=MVCCommandNames.LIST_COURSES_RESOURCE%>"
	var="coursesResourceURL">
	<portlet:param name="authToken" value="${authToken}" />
	<portlet:param name="keyword" value="${keyword}" />
	<portlet:param name="theme" value="${dataTheme}" />
	<portlet:param name="topic" value="${dataTopic}" />

</portlet:resourceURL>

<div class="container main-wrap">

	<div class="sp-main-bot sp-main-top">
		<h3 class="title-1 center-mb">All ${dataThemeName} Courses</h3>
		<div class="row filter-wrap-2">
			<div class="col-md-auto">
				<div class="show-control">
					<a href="#" class="control btn-4"><i class="fas fa-filter"></i>
						Filter</a>
					<div class="sub-content">
						<div class="row">
							<div class="col-6">Advance Filters</div>
							<div class="col-6 text-right">
								<a class="clear-btn" data-form-id="filter"
									href="javascript:void(0)">Clear All</a>
							</div>
						</div>

						<div id="filter">
						<div class="lb-1 mb-2">Topic</div>
							<!-- INSERTED BY JS -->
						</div>
					</div>
				</div>
			</div>
			<div class="col-md last">
				<!--<div class="checkbox checkfund">
                    <input type="checkbox" id="funded" value="1" />
                    <label for="funded">Funded</label>
                </div>-->
				<!--<div class="select-wrap">
                    <select id="rating" class="selectpicker">
                        <option value="desc">Rating (High to Low)</option>
                        <option value="asc">Rating (Low to High)</option>
                    </select>
                </div>-->
				<div class="select-wrap">
					<select id="price" class="selectpicker">
						<option value="asc">Price (Low to High)</option>
						<option value="desc">Price (High to Low)</option>
					</select>
				</div>
			</div>
		</div>
		<div class="sly-wrap sp-bot-2">
			<div class="multisly" data-slide="1">
				<div class="grid-4 clearfix sly-content" id="course-search-result">
					<div id="courses-result">
						<!-- INSERTED BY JS -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function loadResult(url) {
		jQuery.ajax({
			url : url,
			type : "POST",
			dataType : "json",
			async : "false",
			cache : "false",
			success : function(data,textStatus,XMLHttpRequest){
				console.log(data);
				console.log("success");
				var htmlCourses = '';
				var htmlFilterList = '';
				var htmlFinal = '';
				
				if(data.filterList.length > 0){
					data.filterList.forEach(function(filter){
						htmlFilterList += generateFilterList(filter);
					});
				}
				
				$('#filter-topic').html(htmlFilterList);
				
				if(data.courses.length > 0){
					data.courses.forEach(function(course){
						htmlCourses += generateCourseResult(course) ;
						
					});	
				}

				$('#courses-result').html(htmlCourses);
			},
			error : function(data,textStatus,XMLHttpRequest){
				console.log("failed");
			}
		});
	}
	$(document).ready(function() {
		loadResult("<%=coursesResourceURL.toString()%>");
	});

	function generateCourseResult(data) {
		var html = '<div class="item col-md-3">';
		html += '<div class="inner imgeffect hoverpp">';
		html += '<figure class="imgwrap">';
		html += '<img src="'+ data.urlImage +'" style="height: 200px;"/>';
		html += '<div class="status">'+ data.status +'</div>';
		html += '</figure>';
		html += '<div class="content">';
		html += '<p class="title">'+ data.title +'</p>';
		html += '<div class="info">';
		html += '<p class="price-container">';
		html += '<strong class="price">#</strong></p></div></div>';
		html += '<a href="'+ data.urlMore +'" class="fxlink">View detail</a></div>';
		html += '</div>'
		return html;
	}
	
	function generateFilterList(data){
		var html = '<div class="row sp-row-1 break-425">';
		html += '<div class="col-lg-4 col-md-4 col-6 bcol">';
		html +=	'<div class="checkbox checktype">';
		html +=	'<input type="checkbox" name="' + data.filterName + '" id="' + data.filterName + '" value="' + data.filterId + '" /> <label for="' + data.filterName + '">' + data.filterName + '</label>';
		html += '</div>'
		html += '</div>'
		html += '</div>'
		return html;
	}

</script>
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package svc.ntuc.nlh.course.admin.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import org.osgi.annotation.versioning.ProviderType;

import svc.ntuc.nlh.course.admin.exception.NoSuchCourseException;
import svc.ntuc.nlh.course.admin.model.Course;

/**
 * The persistence interface for the course service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourseUtil
 * @generated
 */
@ProviderType
public interface CoursePersistence extends BasePersistence<Course> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CourseUtil} to access the course persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the courses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching courses
	 */
	public java.util.List<Course> findByUuid(String uuid);

	/**
	 * Returns a range of all the courses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @return the range of matching courses
	 */
	public java.util.List<Course> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the courses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first course in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the first course in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the last course in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the last course in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the courses before and after the current course in the ordered set where uuid = &#63;.
	 *
	 * @param courseId the primary key of the current course
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course[] findByUuid_PrevAndNext(
			long courseId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Removes all the courses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of courses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching courses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the course where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchCourseException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByUUID_G(String uuid, long groupId)
		throws NoSuchCourseException;

	/**
	 * Returns the course where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the course where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the course where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the course that was removed
	 */
	public Course removeByUUID_G(String uuid, long groupId)
		throws NoSuchCourseException;

	/**
	 * Returns the number of courses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching courses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the courses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching courses
	 */
	public java.util.List<Course> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the courses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @return the range of matching courses
	 */
	public java.util.List<Course> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the courses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first course in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the first course in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the last course in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the last course in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the courses before and after the current course in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param courseId the primary key of the current course
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course[] findByUuid_C_PrevAndNext(
			long courseId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Removes all the courses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of courses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching courses
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the courses where courseCode = &#63; and deleted = &#63;.
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @return the matching courses
	 */
	public java.util.List<Course> findByCourseCode(
		String courseCode, boolean deleted);

	/**
	 * Returns a range of all the courses where courseCode = &#63; and deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @return the range of matching courses
	 */
	public java.util.List<Course> findByCourseCode(
		String courseCode, boolean deleted, int start, int end);

	/**
	 * Returns an ordered range of all the courses where courseCode = &#63; and deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByCourseCode(
		String courseCode, boolean deleted, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courses where courseCode = &#63; and deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByCourseCode(
		String courseCode, boolean deleted, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first course in the ordered set where courseCode = &#63; and deleted = &#63;.
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByCourseCode_First(
			String courseCode, boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the first course in the ordered set where courseCode = &#63; and deleted = &#63;.
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByCourseCode_First(
		String courseCode, boolean deleted,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the last course in the ordered set where courseCode = &#63; and deleted = &#63;.
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByCourseCode_Last(
			String courseCode, boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the last course in the ordered set where courseCode = &#63; and deleted = &#63;.
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByCourseCode_Last(
		String courseCode, boolean deleted,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the courses before and after the current course in the ordered set where courseCode = &#63; and deleted = &#63;.
	 *
	 * @param courseId the primary key of the current course
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course[] findByCourseCode_PrevAndNext(
			long courseId, String courseCode, boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Removes all the courses where courseCode = &#63; and deleted = &#63; from the database.
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 */
	public void removeByCourseCode(String courseCode, boolean deleted);

	/**
	 * Returns the number of courses where courseCode = &#63; and deleted = &#63;.
	 *
	 * @param courseCode the course code
	 * @param deleted the deleted
	 * @return the number of matching courses
	 */
	public int countByCourseCode(String courseCode, boolean deleted);

	/**
	 * Returns all the courses where courseTitle = &#63; and deleted = &#63;.
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @return the matching courses
	 */
	public java.util.List<Course> findByCourseTitle(
		String courseTitle, boolean deleted);

	/**
	 * Returns a range of all the courses where courseTitle = &#63; and deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @return the range of matching courses
	 */
	public java.util.List<Course> findByCourseTitle(
		String courseTitle, boolean deleted, int start, int end);

	/**
	 * Returns an ordered range of all the courses where courseTitle = &#63; and deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByCourseTitle(
		String courseTitle, boolean deleted, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courses where courseTitle = &#63; and deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByCourseTitle(
		String courseTitle, boolean deleted, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first course in the ordered set where courseTitle = &#63; and deleted = &#63;.
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByCourseTitle_First(
			String courseTitle, boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the first course in the ordered set where courseTitle = &#63; and deleted = &#63;.
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByCourseTitle_First(
		String courseTitle, boolean deleted,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the last course in the ordered set where courseTitle = &#63; and deleted = &#63;.
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByCourseTitle_Last(
			String courseTitle, boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the last course in the ordered set where courseTitle = &#63; and deleted = &#63;.
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByCourseTitle_Last(
		String courseTitle, boolean deleted,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the courses before and after the current course in the ordered set where courseTitle = &#63; and deleted = &#63;.
	 *
	 * @param courseId the primary key of the current course
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course[] findByCourseTitle_PrevAndNext(
			long courseId, String courseTitle, boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Removes all the courses where courseTitle = &#63; and deleted = &#63; from the database.
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 */
	public void removeByCourseTitle(String courseTitle, boolean deleted);

	/**
	 * Returns the number of courses where courseTitle = &#63; and deleted = &#63;.
	 *
	 * @param courseTitle the course title
	 * @param deleted the deleted
	 * @return the number of matching courses
	 */
	public int countByCourseTitle(String courseTitle, boolean deleted);

	/**
	 * Returns the course where courseCode = &#63; and batchId = &#63; and deleted = &#63; or throws a <code>NoSuchCourseException</code> if it could not be found.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @param deleted the deleted
	 * @return the matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByCourseCodeBatchIdActive(
			String courseCode, String batchId, boolean deleted)
		throws NoSuchCourseException;

	/**
	 * Returns the course where courseCode = &#63; and batchId = &#63; and deleted = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @param deleted the deleted
	 * @return the matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByCourseCodeBatchIdActive(
		String courseCode, String batchId, boolean deleted);

	/**
	 * Returns the course where courseCode = &#63; and batchId = &#63; and deleted = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @param deleted the deleted
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByCourseCodeBatchIdActive(
		String courseCode, String batchId, boolean deleted,
		boolean useFinderCache);

	/**
	 * Removes the course where courseCode = &#63; and batchId = &#63; and deleted = &#63; from the database.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @param deleted the deleted
	 * @return the course that was removed
	 */
	public Course removeByCourseCodeBatchIdActive(
			String courseCode, String batchId, boolean deleted)
		throws NoSuchCourseException;

	/**
	 * Returns the number of courses where courseCode = &#63; and batchId = &#63; and deleted = &#63;.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @param deleted the deleted
	 * @return the number of matching courses
	 */
	public int countByCourseCodeBatchIdActive(
		String courseCode, String batchId, boolean deleted);

	/**
	 * Returns the course where courseCode = &#63; and batchId = &#63; or throws a <code>NoSuchCourseException</code> if it could not be found.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @return the matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByCourseCodeBatchId(String courseCode, String batchId)
		throws NoSuchCourseException;

	/**
	 * Returns the course where courseCode = &#63; and batchId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @return the matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByCourseCodeBatchId(String courseCode, String batchId);

	/**
	 * Returns the course where courseCode = &#63; and batchId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByCourseCodeBatchId(
		String courseCode, String batchId, boolean useFinderCache);

	/**
	 * Removes the course where courseCode = &#63; and batchId = &#63; from the database.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @return the course that was removed
	 */
	public Course removeByCourseCodeBatchId(String courseCode, String batchId)
		throws NoSuchCourseException;

	/**
	 * Returns the number of courses where courseCode = &#63; and batchId = &#63;.
	 *
	 * @param courseCode the course code
	 * @param batchId the batch ID
	 * @return the number of matching courses
	 */
	public int countByCourseCodeBatchId(String courseCode, String batchId);

	/**
	 * Returns all the courses where deleted = &#63;.
	 *
	 * @param deleted the deleted
	 * @return the matching courses
	 */
	public java.util.List<Course> findByActiveCourse(boolean deleted);

	/**
	 * Returns a range of all the courses where deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @return the range of matching courses
	 */
	public java.util.List<Course> findByActiveCourse(
		boolean deleted, int start, int end);

	/**
	 * Returns an ordered range of all the courses where deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByActiveCourse(
		boolean deleted, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courses where deleted = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByActiveCourse(
		boolean deleted, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first course in the ordered set where deleted = &#63;.
	 *
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByActiveCourse_First(
			boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the first course in the ordered set where deleted = &#63;.
	 *
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByActiveCourse_First(
		boolean deleted,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the last course in the ordered set where deleted = &#63;.
	 *
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByActiveCourse_Last(
			boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the last course in the ordered set where deleted = &#63;.
	 *
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByActiveCourse_Last(
		boolean deleted,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the courses before and after the current course in the ordered set where deleted = &#63;.
	 *
	 * @param courseId the primary key of the current course
	 * @param deleted the deleted
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course[] findByActiveCourse_PrevAndNext(
			long courseId, boolean deleted,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Removes all the courses where deleted = &#63; from the database.
	 *
	 * @param deleted the deleted
	 */
	public void removeByActiveCourse(boolean deleted);

	/**
	 * Returns the number of courses where deleted = &#63;.
	 *
	 * @param deleted the deleted
	 * @return the number of matching courses
	 */
	public int countByActiveCourse(boolean deleted);

	/**
	 * Returns all the courses where deleted = &#63; and updated = &#63;.
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @return the matching courses
	 */
	public java.util.List<Course> findByActiveCourseUpdated(
		boolean deleted, boolean updated);

	/**
	 * Returns a range of all the courses where deleted = &#63; and updated = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @return the range of matching courses
	 */
	public java.util.List<Course> findByActiveCourseUpdated(
		boolean deleted, boolean updated, int start, int end);

	/**
	 * Returns an ordered range of all the courses where deleted = &#63; and updated = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByActiveCourseUpdated(
		boolean deleted, boolean updated, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courses where deleted = &#63; and updated = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByActiveCourseUpdated(
		boolean deleted, boolean updated, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first course in the ordered set where deleted = &#63; and updated = &#63;.
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByActiveCourseUpdated_First(
			boolean deleted, boolean updated,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the first course in the ordered set where deleted = &#63; and updated = &#63;.
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByActiveCourseUpdated_First(
		boolean deleted, boolean updated,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the last course in the ordered set where deleted = &#63; and updated = &#63;.
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByActiveCourseUpdated_Last(
			boolean deleted, boolean updated,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the last course in the ordered set where deleted = &#63; and updated = &#63;.
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByActiveCourseUpdated_Last(
		boolean deleted, boolean updated,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the courses before and after the current course in the ordered set where deleted = &#63; and updated = &#63;.
	 *
	 * @param courseId the primary key of the current course
	 * @param deleted the deleted
	 * @param updated the updated
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course[] findByActiveCourseUpdated_PrevAndNext(
			long courseId, boolean deleted, boolean updated,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Removes all the courses where deleted = &#63; and updated = &#63; from the database.
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 */
	public void removeByActiveCourseUpdated(boolean deleted, boolean updated);

	/**
	 * Returns the number of courses where deleted = &#63; and updated = &#63;.
	 *
	 * @param deleted the deleted
	 * @param updated the updated
	 * @return the number of matching courses
	 */
	public int countByActiveCourseUpdated(boolean deleted, boolean updated);

	/**
	 * Returns all the courses where deleted = &#63; and popular = &#63;.
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @return the matching courses
	 */
	public java.util.List<Course> findByActivePopularCourse(
		boolean deleted, boolean popular);

	/**
	 * Returns a range of all the courses where deleted = &#63; and popular = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @return the range of matching courses
	 */
	public java.util.List<Course> findByActivePopularCourse(
		boolean deleted, boolean popular, int start, int end);

	/**
	 * Returns an ordered range of all the courses where deleted = &#63; and popular = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByActivePopularCourse(
		boolean deleted, boolean popular, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courses where deleted = &#63; and popular = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching courses
	 */
	public java.util.List<Course> findByActivePopularCourse(
		boolean deleted, boolean popular, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first course in the ordered set where deleted = &#63; and popular = &#63;.
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByActivePopularCourse_First(
			boolean deleted, boolean popular,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the first course in the ordered set where deleted = &#63; and popular = &#63;.
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByActivePopularCourse_First(
		boolean deleted, boolean popular,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the last course in the ordered set where deleted = &#63; and popular = &#63;.
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course
	 * @throws NoSuchCourseException if a matching course could not be found
	 */
	public Course findByActivePopularCourse_Last(
			boolean deleted, boolean popular,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Returns the last course in the ordered set where deleted = &#63; and popular = &#63;.
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching course, or <code>null</code> if a matching course could not be found
	 */
	public Course fetchByActivePopularCourse_Last(
		boolean deleted, boolean popular,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns the courses before and after the current course in the ordered set where deleted = &#63; and popular = &#63;.
	 *
	 * @param courseId the primary key of the current course
	 * @param deleted the deleted
	 * @param popular the popular
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next course
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course[] findByActivePopularCourse_PrevAndNext(
			long courseId, boolean deleted, boolean popular,
			com.liferay.portal.kernel.util.OrderByComparator<Course>
				orderByComparator)
		throws NoSuchCourseException;

	/**
	 * Removes all the courses where deleted = &#63; and popular = &#63; from the database.
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 */
	public void removeByActivePopularCourse(boolean deleted, boolean popular);

	/**
	 * Returns the number of courses where deleted = &#63; and popular = &#63;.
	 *
	 * @param deleted the deleted
	 * @param popular the popular
	 * @return the number of matching courses
	 */
	public int countByActivePopularCourse(boolean deleted, boolean popular);

	/**
	 * Caches the course in the entity cache if it is enabled.
	 *
	 * @param course the course
	 */
	public void cacheResult(Course course);

	/**
	 * Caches the courses in the entity cache if it is enabled.
	 *
	 * @param courses the courses
	 */
	public void cacheResult(java.util.List<Course> courses);

	/**
	 * Creates a new course with the primary key. Does not add the course to the database.
	 *
	 * @param courseId the primary key for the new course
	 * @return the new course
	 */
	public Course create(long courseId);

	/**
	 * Removes the course with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param courseId the primary key of the course
	 * @return the course that was removed
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course remove(long courseId) throws NoSuchCourseException;

	public Course updateImpl(Course course);

	/**
	 * Returns the course with the primary key or throws a <code>NoSuchCourseException</code> if it could not be found.
	 *
	 * @param courseId the primary key of the course
	 * @return the course
	 * @throws NoSuchCourseException if a course with the primary key could not be found
	 */
	public Course findByPrimaryKey(long courseId) throws NoSuchCourseException;

	/**
	 * Returns the course with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param courseId the primary key of the course
	 * @return the course, or <code>null</code> if a course with the primary key could not be found
	 */
	public Course fetchByPrimaryKey(long courseId);

	/**
	 * Returns all the courses.
	 *
	 * @return the courses
	 */
	public java.util.List<Course> findAll();

	/**
	 * Returns a range of all the courses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @return the range of courses
	 */
	public java.util.List<Course> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the courses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of courses
	 */
	public java.util.List<Course> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator);

	/**
	 * Returns an ordered range of all the courses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>CourseModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of courses
	 * @param end the upper bound of the range of courses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of courses
	 */
	public java.util.List<Course> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Course>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the courses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of courses.
	 *
	 * @return the number of courses
	 */
	public int countAll();

}
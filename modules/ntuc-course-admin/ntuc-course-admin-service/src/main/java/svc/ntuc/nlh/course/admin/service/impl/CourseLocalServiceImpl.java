/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package svc.ntuc.nlh.course.admin.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.dao.orm.Conjunction;
import com.liferay.portal.kernel.dao.orm.Disjunction;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.annotations.Component;

import svc.ntuc.nlh.course.admin.exception.NoSuchCourseException;
import svc.ntuc.nlh.course.admin.model.Course;
import svc.ntuc.nlh.course.admin.service.base.CourseLocalServiceBaseImpl;

/**
 * The implementation of the course local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * <code>svc.ntuc.nlh.course.admin.service.CourseLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CourseLocalServiceBaseImpl
 */
@Component(property = "model.class.name=svc.ntuc.nlh.course.admin.model.Course", service = AopService.class)
public class CourseLocalServiceImpl extends CourseLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Use
	 * <code>svc.ntuc.nlh.course.admin.service.CourseLocalService</code> via
	 * injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use
	 * <code>svc.ntuc.nlh.course.admin.service.CourseLocalServiceUtil</code>.
	 */
	private static Log log = LogFactoryUtil.getLog(CourseLocalServiceImpl.class);
	
	public Course addCourse(long groupId, Date endDate, String venue, boolean allowOnlinePayment,
			String courseTitle, boolean allowWebRegistration, String description,
			int availability, String batchId, Date webExpiry, boolean fundedCourseFlag, String courseCode,
			double courseDuration, Date startDate, double courseFee, String courseType, boolean deleted, boolean updated, boolean popular) throws PortalException {

		Group group = groupLocalService.getGroup(groupId);
//		long userId = serviceContext.getUserId();
//		User user = userLocalService.getUser(userId);

		long courseId = counterLocalService.increment(Course.class.getName());
		Course course = createCourse(courseId);
		course.setCompanyId(group.getCompanyId());
		course.setGroupId(groupId);
//		course.setUserId(userId);
//		course.setUserName(user.getScreenName());
		course.setCreateDate(new Date());
		course.setEndDate(endDate);
		course.setVenue(venue);
		course.setAllowOnlinePayment(allowOnlinePayment);
		course.setCourseTitle(courseTitle);
		course.setAllowWebRegistration(allowWebRegistration);
		course.setDescription(description);
		course.setAvailability(availability);
		course.setBatchId(batchId);
		course.setWebExpiry(webExpiry);
		course.setFundedCourseFlag(fundedCourseFlag);
		course.setCourseCode(courseCode);
		course.setCourseDuration(courseDuration);
		course.setStartDate(startDate);
		course.setCourseFee(courseFee);
		course.setCourseType(courseType);
		course.setDeleted(deleted);
		course.setUpdated(updated);
		course.setPopular(popular);
		return super.addCourse(course);

	}

	public Course updateCourse(long courseId, Date endDate, String venue, boolean allowOnlinePayment,
			String courseTitle, boolean allowWebRegistration, String description,
			int availability, String batchId, Date webExpiry, boolean fundedCourseFlag, String courseCode,
			double courseDuration, Date startDate, double courseFee, String courseType, boolean deleted, boolean updated, boolean popular) throws PortalException {
		Course course = getCourse(courseId);
		course.setModifiedDate(new Date());
		course.setEndDate(endDate);
		course.setVenue(venue);
		course.setAllowOnlinePayment(allowOnlinePayment);
		course.setCourseTitle(courseTitle);
		course.setAllowWebRegistration(allowWebRegistration);
		course.setDescription(description);
		course.setAvailability(availability);
		course.setBatchId(batchId);
		course.setWebExpiry(webExpiry);
		course.setFundedCourseFlag(fundedCourseFlag);
		course.setCourseCode(courseCode);
		course.setCourseDuration(courseDuration);
		course.setStartDate(startDate);
		course.setCourseFee(courseFee);
		course.setCourseType(courseType);
		course.setDeleted(deleted);
		course.setUpdated(updated);
		course.setPopular(popular);
		return super.updateCourse(course);
	}

//	finder
	public List<Course> getCourseByCourseCode(String courseCode, boolean deleted) {
		return coursePersistence.findByCourseCode(courseCode, deleted);
	}

	public List<Course> getCourseByCourseCode(String courseCode, boolean deleted, int start, int end) {
		return coursePersistence.findByCourseCode(courseCode, deleted, start, end);
	}

	public List<Course> getCourseByCourseCode(String courseCode, boolean deleted, int start, int end,
			OrderByComparator<Course> orderByComparator) {
		return coursePersistence.findByCourseCode(courseCode,deleted, start, end, orderByComparator);
	}

	public List<Course> getCourseByCourseTitle(String courseTitle,boolean deleted) {
		return coursePersistence.findByCourseTitle(courseTitle,deleted);
	}

	public List<Course> getCourseByCourseTitle(String courseTitle, boolean deleted, int start, int end) {
		return coursePersistence.findByCourseTitle(courseTitle,deleted, start, end);
	}

	public List<Course> getCourseByCourseTitle(String courseTitle, boolean deleted, int start, int end,
			OrderByComparator<Course> orderByComparator) {
		return coursePersistence.findByCourseTitle(courseTitle, deleted, start, end, orderByComparator);
	}
	
	
	public Course getCourseByCourseCodeAndBatchIdActive(String courseCode, String batchId, boolean deleted ) throws NoSuchCourseException {
		return coursePersistence.findByCourseCodeBatchIdActive(courseCode, batchId, deleted);
	}
	
	public Course getCourseByCourseCodeAndBatchId(String courseCode, String batchId) throws NoSuchCourseException {
		return coursePersistence.findByCourseCodeBatchId(courseCode, batchId);
	}
	
	public List<Course> getAllActiveCourse() {
		return coursePersistence.findByActiveCourse(false);
	}
	
	public List<Course> getAllActiveAndUpdatedCourse(boolean deleted, boolean updated) {
		return coursePersistence.findByActiveCourseUpdated(deleted, updated);
	}
	
	public List<Course> getAllActiveAndPopularCourse(boolean deleted, boolean popular) {
		return coursePersistence.findByActivePopularCourse(deleted, popular);
	}

	public List<Course> getCoursesByKeywords(long groupId, String keywords, int start, int end,
			OrderByComparator<Course> orderByComparator) {
		return courseLocalService.dynamicQuery(getKeywordSearchDynamicQuery(groupId, keywords), start, end,
				orderByComparator);
//		return courseLocalService.dynamicQuery(getKeywordSearchDynamicQuery(groupId2, keywords));
	}

	public long getCoursesCountByKeywords(long groupId, String keywords) {
		return courseLocalService.dynamicQueryCount(getKeywordSearchDynamicQuery(groupId, keywords));
	}

	private DynamicQuery getKeywordSearchDynamicQuery(long groupId, String keywords) {
//		log.info("group id = "+groupId);
//		log.info("keywords = "+keywords);
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Course.class, getClassLoader()); 
		Conjunction conjunctionQuery = RestrictionsFactoryUtil.conjunction();
		conjunctionQuery.add(RestrictionsFactoryUtil.eq("groupId", groupId));
		conjunctionQuery.add(RestrictionsFactoryUtil.eq("deleted", false));
		dynamicQuery.add(conjunctionQuery);
		if (!Validator.isBlank(keywords)) {
			Disjunction disjunctionQuery = RestrictionsFactoryUtil.disjunction();

			disjunctionQuery.add(RestrictionsFactoryUtil.like("courseTitle", "%" + keywords + "%"));
			disjunctionQuery.add(RestrictionsFactoryUtil.like("description", "%" + keywords + "%"));
			disjunctionQuery.add(RestrictionsFactoryUtil.like("courseCode", "%" + keywords + "%"));
			disjunctionQuery.add(RestrictionsFactoryUtil.like("batchId", "%" + keywords + "%"));
			disjunctionQuery.add(RestrictionsFactoryUtil.like("venue", "%" + keywords + "%"));
			disjunctionQuery.add(RestrictionsFactoryUtil.like("courseType", "%" + keywords + "%"));
			dynamicQuery.add(disjunctionQuery);
		}
//		log.info("dynamicQuery = "+dynamicQuery.toString());
		return dynamicQuery;
	}

	@Override
	public Course addCourse(Course course) {
		throw new UnsupportedOperationException("Not supported");
	}

	@Override
	public Course updateCourse(Course course) {
		throw new UnsupportedOperationException("Not supported");
	}
}
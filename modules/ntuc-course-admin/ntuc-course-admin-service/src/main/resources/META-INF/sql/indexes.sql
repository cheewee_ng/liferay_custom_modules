create index IX_647EF60C on ntuc_course (courseCode[$COLUMN_LENGTH:75$], batchId[$COLUMN_LENGTH:75$], deleted);
create index IX_244C44C1 on ntuc_course (courseCode[$COLUMN_LENGTH:75$], deleted);
create index IX_FB293C98 on ntuc_course (courseCode[$COLUMN_LENGTH:75$], startDate);
create index IX_A41874E8 on ntuc_course (courseTitle[$COLUMN_LENGTH:2000$], deleted);
create index IX_F602A00C on ntuc_course (deleted, popular);
create index IX_A6C09D6E on ntuc_course (deleted, updated);
create index IX_484E7520 on ntuc_course (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_8731C3A2 on ntuc_course (uuid_[$COLUMN_LENGTH:75$], groupId);